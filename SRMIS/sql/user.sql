CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `securitypassword` varchar(255) DEFAULT NULL COMMENT '安全密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `telephone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `type` varchar(255) DEFAULT NULL COMMENT '用户类型 0-普通教师 1-专家评审 2-科研管理人员 3-系统管理员',
  `dep_id` int(10) DEFAULT NULL COMMENT '科室id',
  `dname` varchar(255) DEFAULT NULL COMMENT '科室名',
  `duty` varchar(255) DEFAULT NULL COMMENT '职务',
  PRIMARY KEY (`id`),
  KEY `dep_id` (`dep_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`dep_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户'
