CREATE TABLE `projectreview` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) DEFAULT NULL COMMENT '对应项目id',
  `classify` varchar(255) DEFAULT NULL COMMENT '评审类型    1-项目申请 2-结题申请',
  `pssj` date DEFAULT NULL COMMENT '评审时间',
  `opinion` varchar(255) DEFAULT NULL COMMENT '评审意见    （0-待评审） 1-评审通过 2-评审不通过 3-退回',
  `comment` varchar(4095) DEFAULT NULL COMMENT '评语',
  `user_id` int(10) DEFAULT NULL COMMENT '评审专家id',
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `projectreview_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `projectreview_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目评审'
