CREATE TABLE `department` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '科室名称',
  `telephone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `subject` varchar(255) DEFAULT NULL COMMENT '对应学科',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='科室表'


insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('1','计算机科室',NULL,NULL,'计算机');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('2','物理科室',NULL,NULL,'物理');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('3','生物科室',NULL,NULL,'生物');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('4','外语科室',NULL,NULL,'外语');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('5','数学科室',NULL,NULL,'数学');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('6','化学科室',NULL,NULL,'化学');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('7','中文科室',NULL,NULL,'中文');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('8','经济科室',NULL,NULL,'经济');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('9','教育科室',NULL,NULL,'教育');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('10','体育科室',NULL,NULL,'体育');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('11','中药科室',NULL,NULL,'中药');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('12','临床医学科室',NULL,NULL,'临床医学');
insert into `department` (`id`, `name`, `telephone`, `email`, `subject`) values('13','生物制药科室',NULL,NULL,'生物制药');