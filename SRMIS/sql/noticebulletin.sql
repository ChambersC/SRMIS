CREATE TABLE `noticebulletin` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`bh`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '编号' ,
`btmc`  varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '标题名称' ,
`zynr`  varchar(10000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '主要内容' ,
`zgr`  varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '撰稿人' ,
`fbrxm`  varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '发布人姓名' ,
`fbsj`  date NULL DEFAULT NULL COMMENT '发布时间' ,
`fbzt`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '发布状态 0-未发布  1-已发布  2-置顶  3-下架' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin
AUTO_INCREMENT=24
ROW_FORMAT=COMPACT
COMMENT='通知信息'
;



INSERT INTO `noticebulletin` VALUES (1, 'te', 'st', NULL, NULL, 'uncompleted', '2018-3-9', '1');
INSERT INTO `noticebulletin` VALUES (2, 'd', 'e', 'l', 'e', 'uncompleted', '2018-3-9', '1');
INSERT INTO `noticebulletin` VALUES (3, '3', 'inner222', 'fsagafa', 'sql', 'uncompleted', '2018-3-9', '1');
INSERT INTO `noticebulletin` VALUES (12, '12', 'a', NULL, NULL, NULL, NULL, '3');
INSERT INTO `noticebulletin` VALUES (14, '14', 'a', NULL, NULL, NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (15, '15', 'a', NULL, '1', NULL, NULL, '3');
INSERT INTO `noticebulletin` VALUES (17, '17', 'last', '<p>name <span style=\"font-size: 24px;\"><strong>test</strong></span></p>', 'uncompleted', 'uncompleted', '2018-3-1', '2');
INSERT INTO `noticebulletin` VALUES (18, '18', 'add', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\"/></p>', 'uncompleted', 'uncompleted', '2018-3-12', '1');
INSERT INTO `noticebulletin` VALUES (19, '11', '22', '<p>222</p>', 'uncompleted', NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (23, '13', '131', '<p><em><strong>鐧�/strong></em><span style=\"text-decoration: underline;\"><strong>搴�/strong></span><strong><span style=\"border: 1px solid rgb(0, 0, 0);\">鍔�/span></strong><span style=\"text-decoration: line-through;\"><strong>绮�/strong></span><strong><img title=\"1520838545592003178.jpg\" src=\"/ueditor/jsp/upload/image/20180312/1520838545592003178.jpg\"/></strong></p><hr/><p>&nbsp;</p><table><tbody><tr class=\"firstRow\"><td valign=\"top\" width=\"398\">1</td><td valign=\"top\" width=\"398\"></td></tr><tr><td style=\"word-break: break-all;\" valign=\"top\" width=\"398\"></td><td style=\"word-break: break-all;\" valign=\"top\" width=\"398\">2</td></tr></tbody></table><p>&nbsp;</p>', 'uncompleted', NULL, NULL, '3');
INSERT INTO `noticebulletin` VALUES (24, '1', '1111111', '<p>2</p>', 'uncompleted', NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (25, '222', '222', '', 'uncompleted', NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (26, '111', '111', '', 'uncompleted', NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (27, '1121111', '11', '', 'uncompleted', NULL, NULL, '0');
INSERT INTO `noticebulletin` VALUES (28, '1111', '1111', '', 'uncompleted', 'uncompleted', '2018-3-12', '2');

