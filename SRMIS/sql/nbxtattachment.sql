CREATE TABLE `nbxtattachment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `stid` int(10) DEFAULT NULL COMMENT '实体id',
  `stmc` varchar(255) DEFAULT NULL COMMENT '实体名称（类名）',
  `fjmc` varchar(255) DEFAULT NULL COMMENT '附件名称',
  `fjUrl` varchar(255) DEFAULT NULL COMMENT '附件下载地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='附件'