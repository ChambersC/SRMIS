
CREATE TABLE `awardwinningnews` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bh` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '编号',
  `btmc` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '标题名称',
  `zynr` varchar(10000) COLLATE utf8_bin DEFAULT NULL COMMENT '主要内容',
  `zgr` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '撰稿人',
  `fbrxm` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '发布人姓名',
  `fbsj` date DEFAULT NULL COMMENT '发布时间',
  `fbzt` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '发布状态 0-未发布  1-已发布  2-置顶  3-下架',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='获奖喜讯'
