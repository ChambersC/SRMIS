CREATE TABLE `batchsetting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pcmc` varchar(255) DEFAULT NULL COMMENT '批次名称',
  `kssj` date DEFAULT NULL COMMENT '申请开放开始时间',
  `jssj` date DEFAULT NULL COMMENT '申请开放结束时间',
  `bz` varchar(511) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='批次设置'