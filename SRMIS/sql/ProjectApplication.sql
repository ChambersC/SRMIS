CREATE TABLE `projectapplication` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) DEFAULT NULL COMMENT '对应项目id',
  `classify` varchar(255) DEFAULT NULL COMMENT '申请类型    1-项目申请 2-结题申请',
  `sqsj` date DEFAULT NULL COMMENT '申请时间',
  `shsj` date DEFAULT NULL COMMENT '申请审核时间',
  `shr` varchar(255) DEFAULT NULL COMMENT '审核人',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `projectapplication_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目申请'
