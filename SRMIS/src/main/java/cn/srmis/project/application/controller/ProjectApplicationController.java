package cn.srmis.project.application.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.common.service.UpdateLoadService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Project application management - project application
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/apply/")
@Slf4j
public class ProjectApplicationController extends BaseContorller {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private UpdateLoadService updateLoadService;
	
	@Autowired
	private UserService userService;

/* project application */

	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
        vo.setUserId(user.getId());
		// 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        vo.setClassify("1");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有  
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "1");
        		project.setApplication(application);
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        	}
        }
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/apply/apply-list");
		return modelAndView;
    }
	
	/**
	 * 验证当前时间是否在申请开放时间内
	 * @param id
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "timeverification")
    public void timeverification(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result = new JSONObject();
		List<BatchSetting> batchList = new ArrayList<BatchSetting>();
		String flag = "Y";
        try{
        	if(id==null){
        		batchList = batchSettingService.getEntityByCondition(new Date(), null);
        	}else{
        		Project project = projectService.findById(id);
        		batchList = batchSettingService.getEntityByCondition(new Date(), project.getBatch_id());
        	}
        	if(batchList.size()==0){
        		flag = "N";
        	}
        	result.put("flag", flag);
        }catch (Exception e){
            e.printStackTrace();
        }finally{
        	this.write(result, response);
        }
    }

	@RequestMapping(value = "edit")
    public ModelAndView edit(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		if(id!=null){
			Project project = projectService.findById(id);
			ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "1");
			project.setApplication(application);
			modelAndView.addObject("project", project);
			modelAndView.addObject("stid", application.getId());
			modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		}
		List<BatchSetting> batchList = batchSettingService.getEntityByCondition(new Date(), null);
        modelAndView.addObject("batchList", batchList);
		List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        modelAndView.setViewName("content/apply/apply-edit");
		return modelAndView;
    }
	
	@RequestMapping(value = "save")
    public void save(Project project, HttpServletResponse response,HttpServletRequest request, 
    		String multiFileIds) throws IOException{
		JSONObject result = new JSONObject();
		try{
			ProjectApplication application = new ProjectApplication();
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			project.setUser_id(user.getId());
			Department department = departmentService.findById(project.getDep_id());
			project.setSubject(department.getSubject());
			
	    	if(project.getId()!=null){
	    		application = projectService.getProApplByProIdLx(project.getId(), "1");
	    		if(project.getStatus().equals("7")){	//立项退回修改特殊处理
	    			project.setStatus("4");
	    			application.setBz("立项退回后已修改");
	    			List<ProjectReview> reviewList = projectService.getProRevByProIdLx(project.getId(), "1");
	    			if(reviewList!=null&&reviewList.size()>0){
	    				for(ProjectReview review:reviewList){
	    					review.setOpinion(null);
	    					review.setComment(null);
	    					review.setPssj(null);
	    					projectService.updateReview(review);
	    				}
	    			}
	    		}else{
	    			project.setStatus("0");
	    			application.setSqsj(new Date());
		    		application.setShr(null);
		    		application.setShsj(null);
		    		application.setBz(null);
	    		}
	    		projectService.update(project);
	    		projectService.updateApplication(application);
			} else{
				project.setStatus("0");
				projectService.save(project);
				application.setSqsj(new Date());
	    		application.setClassify("1");
	    		application.setProject_id(project.getId());
	    		projectService.saveApplication(application);
			}
	    	updateLoadService.update(multiFileIds, ProjectApplication.class.getSimpleName(), application.getId());
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "1");
		project.setApplication(application);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		modelAndView.setViewName("content/apply/apply-view");
		return modelAndView;
    }
	
	@RequestMapping(value = "delete")
    public void delete(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
			updateLoadService.deleteByStidmc(ProjectApplication.class.getSimpleName(), application.getId());
			projectService.deleteApplication(application.getId());
			projectService.delete(id);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	

}
