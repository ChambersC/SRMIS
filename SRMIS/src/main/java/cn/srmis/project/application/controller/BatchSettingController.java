package cn.srmis.project.application.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.BatchVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.util.DateUtil;

/**
 * Project application management - Batch Setting
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/apply/batch/")
@Slf4j
public class BatchSettingController extends BaseContorller {
	
	@Autowired
	private BatchSettingService batchSettingServiceService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, BatchVo vo){
		
		Map<String, Object> model = new HashMap<String, Object>();
		// 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        //总记录数
        int resultSize = batchSettingServiceService.getCountByCondition(vo.getMc(), vo.getKssj(), vo.getJssj());  
        //显示所有  
        List<BatchSetting> pageList = batchSettingServiceService.getPageList(vo.getMc(), vo.getKssj(), vo.getJssj(), pageIndex*pageSize, pageSize);  
        model.put("vo", vo);
        model.put("pageList", pageList);
        model.put("resultSize", resultSize);
        model.put("pageSize", pageSize);
		
    	return new ModelAndView("content/apply/batch-list", model);
    }

	@RequestMapping(value = "edit")
    public ModelAndView edit(Integer id){
		Map<String, Object> model = new HashMap<String, Object>();
		if(id!=null){
			BatchSetting batchSetting = batchSettingServiceService.findById(id);
			model.put("batchSetting", batchSetting);
		}
		return new ModelAndView("content/apply/batch-edit",model);
    }
	
	@RequestMapping(value = "save")
    public void save(BatchSetting batchSetting, HttpServletResponse response) throws IOException{
		JSONObject result = new JSONObject();
		try{
	    	if(batchSetting.getId()!=null){
				batchSettingServiceService.update(batchSetting);
			} else{
				batchSettingServiceService.save(batchSetting);
			}
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "delete")
    public void delete(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			batchSettingServiceService.delete(id);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
}
