package cn.srmis.project.application.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Project application management - organization expert review
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/apply/organize/")
@Slf4j
public class OrganizationExpertReviewController extends BaseContorller {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
/* organization expert review */
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
        // 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        vo.setPage("1");
        vo.setClassify("1");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有  
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "1");
        		project.setApplication(application);
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        	}
        }
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/apply/organize-list");
		return modelAndView;
    }
	
	@RequestMapping(value = "organize")
    public ModelAndView organize(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
		project.setApplication(application);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		List<User> userList = userService.findExpert(project.getDep_id(), project.getUser_id());
		modelAndView.addObject("userList", userList);
		modelAndView.setViewName("content/apply/organize-organize");
        return modelAndView;
    }
	
	@RequestMapping(value = "save")
    public void save(Integer id, HttpServletResponse response, String userIds) throws IOException{
		JSONObject result = new JSONObject();
		try{
			String ids[] = userIds.split(",");
			for(int i=0;i<ids.length;i++){
				if(ids[i].equals("")){
					continue;
				}
				ProjectReview review = new ProjectReview();
				review.setProject_id(id);
				review.setClassify("1");
				review.setUser_id(Integer.parseInt(ids[i]));
		    	projectService.saveReview(review);
			}
			ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
			projectService.updateApplication(application);
			Project project = projectService.findById(id);
	    	project.setStatus("4");
	    	projectService.update(project);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
		project.setApplication(application);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		List<ProjectReview> reviewList = projectService.getProRevByProIdLx(id, "1");
		List<User> userList = new ArrayList<User>();
		if(reviewList!=null&&reviewList.size()>0){
			for(ProjectReview review:reviewList){
				User u = userService.findById(review.getUser_id());
				userList.add(u);
			}
		}
		modelAndView.addObject("userList", userList);		
		modelAndView.setViewName("content/apply/organize-view");
		return modelAndView;
    }
}
