package cn.srmis.project.controller;

import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;


import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Chart;
import cn.srmis.project.po.ExcelVo;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;

/**
 * Management of science research achievement
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/achieve/")
@Slf4j
public class AchievementController extends BaseContorller {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
		List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        
		ProjectVo chartVo = new ProjectVo();
		chartVo.setStatus("12");
    	User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		modelAndView.addObject("Type", user.getType());
		if(!user.getType().equals("2")){
			vo.setUserId(user.getId());
			chartVo.setUserId(user.getId());
		}else {											//科管员有全部或某一批次 的学科-项目数柱状图
			if(depList!=null&&depList.size()>0){
				ProjectVo chartVo2 = new ProjectVo();
				chartVo2.setStatus("12");
				if(vo.getBatchId()==null)
					modelAndView.addObject("name", "全部批次");
        		else{
    				chartVo2.setBatchId(vo.getBatchId());
    				BatchSetting batch = batchSettingService.findById(vo.getBatchId());
        			modelAndView.addObject("name", batch.getPcmc());
        		}
				List<Chart> barList = new ArrayList<Chart>();
	        	for(Department dep:depList){
	        		Chart bar = new Chart();
	        		bar.setName(dep.getSubject());
	        		chartVo2.setDepId(dep.getId());
	        		int number = projectService.getCountByCondition(chartVo2);
	        		bar.setNumber(number);
	        		barList.add(bar);
	        	}
	        	String strBarContext =JSON.toJSONString(barList); 
	        	modelAndView.addObject("strBarContext", strBarContext);
	        }
		}
        
        List<Chart> lineList = new ArrayList<Chart>();
        if(batchList!=null&&batchList.size()>0){
        	for(BatchSetting batch:batchList){
        		Chart line = new Chart();
        		line.setName(batch.getPcmc());
        		chartVo.setBatchId(batch.getId());
        		int number = projectService.getCountByCondition(chartVo);
        		line.setNumber(number);
        		lineList.add(line);
        	}
        	String strLineContext =JSON.toJSONString(lineList); 
        	modelAndView.addObject("strLineContext", strLineContext);
        }
        // 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        vo.setStatus("12");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有  
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        	}
        }
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/project/achieve-list");
		return modelAndView;
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id, HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		
		ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
		project.setApplication(application);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		if(user.getType().equals("2")){
			User u = userService.findById(project.getUser_id());
			project.setUser(u);
		}
		modelAndView.addObject("project", project);
		modelAndView.setViewName("content/project/achieve-view");
		return modelAndView;
    }
	
	/**
	 * 导出Excel
	 */
	@RequestMapping(value = "exportExcel")
	public void exportExcel(HttpServletResponse response, String cid) throws Exception{
		OutputStream os = null;
		try{
			List<ExcelVo> dataList = new ArrayList<ExcelVo>();
			String ids[] = cid.split(",");
			for(int i=0;i<ids.length;i++){
				if(ids[i].equals("")){
					continue;
				}
				ExcelVo vo = new ExcelVo();
				Project project = projectService.findById(Integer.parseInt(ids[i]));
				vo.setJtsj(project.getJtsj());
				vo.setLxsj(project.getLxsj());
				vo.setName(project.getName());
				vo.setSubject(project.getSubject());
				BatchSetting batch = batchSettingService.findById(project.getBatch_id());
        		vo.setBatch(batch.getPcmc());
        		User user = userService.findById(project.getUser_id());
        		vo.setPerson(user.getName());
        		dataList.add(vo);
			}
			os = response.getOutputStream();	// 取得输出流 
            String outPutFileName = "项目信息表.xls";
            response.reset();	// 清空输出流
            response.setHeader("content-disposition",	// 设定输出文件头 
                    "attachment;filename=" + new String(outPutFileName.getBytes("gb2312"), "ISO-8859-1"));
            response.setContentType("APPLICATION/msexcel");		// 定义输出类型
			projectService.exportExcel(dataList,os);
            response.flushBuffer();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (os != null){
                os.close();
            }
		}
	}
	
}
