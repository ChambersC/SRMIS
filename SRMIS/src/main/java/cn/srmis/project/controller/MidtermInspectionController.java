package cn.srmis.project.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.common.service.UpdateLoadService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Midterm Inspection
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/inspect/")
@Slf4j
public class MidtermInspectionController extends BaseContorller {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UpdateLoadService updateLoadService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		modelAndView.addObject("Type", user.getType());
		if(!user.getType().equals("2")){
			vo.setUserId(user.getId());
		}
        // 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
		vo.setPage("3");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "1");
        		project.setApplication(application);
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        		
        		List<NbxtAttachment> nbxtList = updateLoadService.getList("Project", project.getId());
        		if(nbxtList!=null && nbxtList.size()>0){
        			project.setAttachment(nbxtList.get(0));
        		}
        	}
        }
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/project/inspect-list");
		return modelAndView;
    }
	
	@RequestMapping(value = "submit")
    public ModelAndView submit(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid2", project.getId());
		modelAndView.addObject("stmc2", Project.class.getSimpleName());
		modelAndView.setViewName("content/project/inspect-submit");
        return modelAndView;
    }
	
	@RequestMapping(value = "refer")
    public ModelAndView refer(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		
		ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		List<NbxtAttachment> nbxtList = updateLoadService.getList("Project", id);
		if(nbxtList!=null && nbxtList.size()>0){
			modelAndView.addObject("nbxtList", nbxtList);
		}
		modelAndView.setViewName("content/project/inspect-refer");
        return modelAndView;
    }
	
	@RequestMapping(value = "save")
    public void save(Integer id, HttpServletResponse response, HttpServletRequest request, 
    		String multiFileIds) throws IOException{
		JSONObject result = new JSONObject();
		try{
			if(multiFileIds != null && multiFileIds != ""){
				updateLoadService.update(multiFileIds, Project.class.getSimpleName(), id);
			}else{
				Project project = projectService.findById(id);
				project.setStatus("8");
				projectService.update(project);
			}
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id, HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		if(user.getType().equals("2")){
			User u = userService.findById(project.getUser_id());
			project.setUser(u);
		}
		modelAndView.addObject("project", project);
		
		ProjectApplication application = projectService.getProApplByProIdLx(id, "1");
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		List<NbxtAttachment> nbxtList = updateLoadService.getList("Project", id);
		if(nbxtList!=null && nbxtList.size()>0){
			modelAndView.addObject("nbxtList", nbxtList);
		}
		modelAndView.setViewName("content/project/inspect-view");
		return modelAndView;
    }
	
}
