package cn.srmis.project.service;

import java.util.Date;
import java.util.List;

import cn.srmis.project.po.BatchSetting;

/**
 * 批次设置业务类
 * @author Chambers
 * 
 */
public interface BatchSettingService {

	List<BatchSetting> getPageList(String mc, Date kssj, Date jssj, 
			int pageIndex, int pageSize);

	int getAllCount();

	int getCountByCondition(String mc, Date kssj, Date jssj);

	BatchSetting findById(Integer id);

	void save(BatchSetting batchSetting);

	void update(BatchSetting batchSetting);

	void delete(Integer id);

	void updateByPrimaryKeySelective(BatchSetting batchSetting);

	List<BatchSetting> getEntityByCondition(Date time, Integer id);
	
	List<BatchSetting> findAll();
}
