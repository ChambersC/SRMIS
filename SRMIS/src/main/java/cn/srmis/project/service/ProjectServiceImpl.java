package cn.srmis.project.service;

import java.io.OutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.project.dao.ProjectApplicationMapper;
import cn.srmis.project.dao.ProjectMapper;
import cn.srmis.project.dao.ProjectReviewMapper;
import cn.srmis.project.po.ExcelVo;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.util.DateUtil;

/**
 * 项目业务类实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectMapper projectMapper;
    
    @Autowired
    private ProjectApplicationMapper projectApplicationMapper;
    
    @Autowired
    private ProjectReviewMapper projectReviewMapper;
    
	@Override
	public List<Project> getPageList(ProjectVo vo) {
		// TODO Auto-generated method stub
		return projectMapper.getPageList(vo);
	}

	@Override
	public int getCountByCondition(ProjectVo vo) {
		// TODO Auto-generated method stub
		return projectMapper.getCountByCondition(vo);
	}
	
	@Override
	public int getAllCount() {
		// TODO Auto-generated method stub
		return projectMapper.getAllCount();
	}

	@Override
	public Project findById(Integer id) {
		// TODO Auto-generated method stub
		return projectMapper.selectByPrimaryKey(id);
	}

	@Override
	public void save(Project project) {
		// TODO Auto-generated method stub
		projectMapper.insert(project);
	}

	@Override
	public void update(Project project) {
		// TODO Auto-generated method stub
		projectMapper.updateByPrimaryKey(project);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		projectMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(Project project) {
		// TODO Auto-generated method stub
		projectMapper.updateByPrimaryKeySelective(project);
	}

	/*@Override
	public Project getEntityByCondition(String bh, Integer id) {
		// TODO Auto-generated method stub
		return projectMapper.getEntityByCondition(bh, id);
	}*/

	@Override
	public ProjectApplication getProApplByProIdLx(Integer project_id,
			String classify) {
		// TODO Auto-generated method stub
		return projectApplicationMapper.getProApplByProIdLx(project_id, classify);
	}

	@Override
	public void updateApplication(ProjectApplication projectApplication) {
		// TODO Auto-generated method stub
		projectApplicationMapper.updateByPrimaryKey(projectApplication);
	}

	@Override
	public void saveApplication(ProjectApplication application) {
		// TODO Auto-generated method stub
		projectApplicationMapper.insert(application);
	}

	@Override
	public void deleteApplication(Integer id) {
		// TODO Auto-generated method stub
		projectApplicationMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void saveReview(ProjectReview review) {
		// TODO Auto-generated method stub
		projectReviewMapper.insert(review);
	}

	@Override
	public List<ProjectReview> getProRevByProIdLx(Integer project_id, String classify) {
		// TODO Auto-generated method stub
		return projectReviewMapper.getProRevByProIdLx(project_id, classify);
	}

	@Override
	public ProjectReview getReviewByProIdLxUserId(Integer project_id,
			String classify, Integer user_id) {
		// TODO Auto-generated method stub
		return projectReviewMapper.getReviewByProIdLxUserId(project_id, classify, user_id);
	}

	@Override
	public void updateReview(ProjectReview review) {
		// TODO Auto-generated method stub
		projectReviewMapper.updateByPrimaryKey(review);
	}

	@Override
	public void exportExcel(List<ExcelVo> dataList, OutputStream os) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] title = {"所属批次名称","所属学科","项目名称","负责人","立项时间","结题时间"};
			
			HSSFWorkbook workbook = new HSSFWorkbook();		//创建一个excel工作簿
	        HSSFSheet sheet = workbook.createSheet();		//创建一个sheet工作表
	        
	        //设置样式
	        HSSFCellStyle cellStyle = workbook.createCellStyle();
	        setBorderStyle(cellStyle, BorderStyle.THIN);
	        cellStyle.setFont(setFontStyle(workbook, "黑体", (short) 18));
	        cellStyle.setAlignment(HorizontalAlignment.CENTER);		//对齐方式-水平对齐
	        
	        HSSFCellStyle cellStyle2 = workbook.createCellStyle();
	        setBorderStyle(cellStyle2, BorderStyle.THIN);
	        cellStyle2.setFont(setFontStyle(workbook, "宋体", (short) 16));
	        cellStyle2.setAlignment(HorizontalAlignment.CENTER);
	        //创建第0行表头，再在这行里在创建单元格，并赋值
	        HSSFRow row = sheet.createRow(0);
	        HSSFCell cell = null;
	        //插入第一行数据 
	        for (int i = 0; i < title.length; i++) {
	            cell = row.createCell(i);
	            cell.setCellValue(title[i]);
	            cell.setCellStyle(cellStyle);
	        }
	        //将主体数据填入Excel中
	        for (int i = 0; i < dataList.size(); i++) {
	            row = sheet.createRow(i + 1);
	            String[] context = {dataList.get(i).getBatch(),dataList.get(i).getSubject(),
	            		dataList.get(i).getName(),dataList.get(i).getPerson(),
	            		DateUtil.formatDate(dataList.get(i).getLxsj(), "yyyy-MM-dd"),
	            		DateUtil.formatDate(dataList.get(i).getJtsj(), "yyyy-MM-dd")};
	            for(int j = 0;j < context.length; j++){
	            	cell = row.createCell(j);
	            	cell.setCellValue(context[j]);
	                cell.setCellStyle(cellStyle2);
	            } 
	        }
	        //自动调整列宽
	        for (int i = 0; i < title.length; i++) {
	            sheet.autoSizeColumn(i); 
	        }
	        workbook.write(os);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if (os != null){
                os.close();
            }
		}
	}

	/**
	 * 设置单元格样式
	 * @param cellStyle
	 * @param border
	 */
	private void setBorderStyle(HSSFCellStyle cellStyle, BorderStyle border) {
		// TODO Auto-generated method stub
		cellStyle.setBorderBottom(border); // 下边框
        cellStyle.setBorderLeft(border);// 左边框
        cellStyle.setBorderTop(border);// 上边框
        cellStyle.setBorderRight(border);// 右边框
	}

	/**
	 * 设置字体样式
	 * @param workbook 工作簿
	 * @param name 字体类型
	 * @param height 字体大小
	 * @return
	 */
	private HSSFFont setFontStyle(HSSFWorkbook workbook, String name, short height) {
		// TODO Auto-generated method stub
		HSSFFont font = workbook.createFont();
		font.setFontHeightInPoints(height);
        font.setFontName(name);
        return font;
	}
	
}
