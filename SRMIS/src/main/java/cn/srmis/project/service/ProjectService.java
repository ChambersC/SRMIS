package cn.srmis.project.service;

import java.io.OutputStream;
import java.util.List;

import cn.srmis.project.po.ExcelVo;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;

/**
 * 项目业务类
 * @author Chambers
 * 
 */
public interface ProjectService {

	List<Project> getPageList(ProjectVo vo);

	int getAllCount();

	int getCountByCondition(ProjectVo vo);

	Project findById(Integer id);

	void save(Project project);

	void update(Project project);

	void delete(Integer id);

	void updateByPrimaryKeySelective(Project project);

	//Project getEntityByCondition(String bh, Integer id);
	
	/**
	 * 通过项目id和类型找到对应申请表
	 * @author Chambers
	 * @param project_id
	 * @param classify	//申请类型	1-项目申请 2-结题申请
	 * @return
	 */
	ProjectApplication getProApplByProIdLx(Integer project_id, String classify);

	void updateApplication(ProjectApplication projectApplication);

	void saveApplication(ProjectApplication application);

	void deleteApplication(Integer id);

	/**
	 * 通过项目id和类型找到对应评审表列
	 * @author Chambers
	 * @param project_id
	 * @param classify	//申请类型	1-项目申请 2-结题申请
	 * @return
	 */
	List<ProjectReview> getProRevByProIdLx(Integer project_id, String classify);
	
	void saveReview(ProjectReview review);
	
	/**
	 * 通过项目id、类型和专家id找到对应评审表
	 * @author Chambers
	 * @param project_id
	 * @param classify
	 * @param user_id
	 * @return
	 */
	ProjectReview getReviewByProIdLxUserId(Integer project_id, String classify, Integer user_id);

	void updateReview(ProjectReview review);

	/**
	 * 导出Excel
	 * @author Chambers
	 * @param dataList
	 * @param os
	 */
	void exportExcel(List<ExcelVo> dataList, OutputStream os) throws Exception;
	
}
