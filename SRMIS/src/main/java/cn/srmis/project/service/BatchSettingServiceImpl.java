package cn.srmis.project.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.project.dao.BatchSettingMapper;
import cn.srmis.project.po.BatchSetting;

/**
 * 批次设置业务类实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class BatchSettingServiceImpl implements BatchSettingService {

    @Autowired
    private BatchSettingMapper batchSettingMapper;
    
	@Override
	public List<BatchSetting> getPageList(String mc, Date kssj, Date jssj, 
			int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return batchSettingMapper.getPageList(mc, kssj, jssj, pageIndex, pageSize);
	}

	@Override
	public int getCountByCondition(String mc, Date kssj, Date jssj) {
		// TODO Auto-generated method stub
		if(StringUtils.isNotEmpty(mc)||kssj!=null||jssj!=null)
			return batchSettingMapper.getCountByCondition(mc, kssj, jssj);
		else
			return this.getAllCount();
	}
	
	@Override
	public int getAllCount() {
		// TODO Auto-generated method stub
		return batchSettingMapper.getAllCount();
	}

	@Override
	public BatchSetting findById(Integer id) {
		// TODO Auto-generated method stub
		return batchSettingMapper.selectByPrimaryKey(id);
	}

	@Override
	public void save(BatchSetting batchSetting) {
		// TODO Auto-generated method stub
		batchSettingMapper.insert(batchSetting);
	}

	@Override
	public void update(BatchSetting batchSetting) {
		// TODO Auto-generated method stub
		batchSettingMapper.updateByPrimaryKey(batchSetting);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		batchSettingMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(BatchSetting batchSetting) {
		// TODO Auto-generated method stub
		batchSettingMapper.updateByPrimaryKeySelective(batchSetting);
	}

	@Override
	public List<BatchSetting> getEntityByCondition(Date time, Integer id) {
		// TODO Auto-generated method stub
		return batchSettingMapper.getEntityByCondition(time, id);
	}

	@Override
	public List<BatchSetting> findAll() {
		// TODO Auto-generated method stub
		return batchSettingMapper.findAll();
	}

}
