package cn.srmis.project.dao;

import java.util.List;

import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectVo;

public interface ProjectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKey(Project record);
    
    
    List<Project> getPageList(ProjectVo vo);

	int getCountByCondition(ProjectVo vo);

	int getAllCount();

	//Project getEntityByCondition(String bh, Integer id);
}