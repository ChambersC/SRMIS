package cn.srmis.project.dao;

import cn.srmis.project.po.ProjectApplication;

public interface ProjectApplicationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProjectApplication record);

    int insertSelective(ProjectApplication record);

    ProjectApplication selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProjectApplication record);

    int updateByPrimaryKey(ProjectApplication record);

	
    
    ProjectApplication getProApplByProIdLx(Integer project_id, String classify);
}