package cn.srmis.project.dao;

import java.util.List;

import cn.srmis.project.po.ProjectReview;

public interface ProjectReviewMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProjectReview record);

    int insertSelective(ProjectReview record);

    ProjectReview selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProjectReview record);

    int updateByPrimaryKey(ProjectReview record);

	
    
    List<ProjectReview> getProRevByProIdLx(Integer project_id, String classify);
    
    ProjectReview getReviewByProIdLxUserId(Integer project_id, String classify, Integer user_id);
}