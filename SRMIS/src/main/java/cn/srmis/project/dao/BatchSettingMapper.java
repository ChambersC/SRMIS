package cn.srmis.project.dao;

import java.util.Date;
import java.util.List;

import cn.srmis.project.po.BatchSetting;

public interface BatchSettingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BatchSetting record);

    int insertSelective(BatchSetting record);

    BatchSetting selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BatchSetting record);

    int updateByPrimaryKey(BatchSetting record);
    
    
    
    List<BatchSetting> getPageList(String mc, Date kssj, Date jssj, 
			int pageIndex, int pageSize);

	int getCountByCondition(String mc, Date kssj, Date jssj);

	int getAllCount();

	List<BatchSetting> getEntityByCondition(Date time, Integer id);
	
	List<BatchSetting> findAll();
}