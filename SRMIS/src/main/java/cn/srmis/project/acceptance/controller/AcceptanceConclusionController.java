package cn.srmis.project.acceptance.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Project acceptance management - acceptance conclusion
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/accept/conclude/")
@Slf4j
public class AcceptanceConclusionController extends BaseContorller {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
        // 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        vo.setPage("5");
        vo.setClassify("2");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有  
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "2");
        		project.setApplication(application);
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        	}
        }
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/accept/conclude-list");
		return modelAndView;
    }
	
	@RequestMapping(value = "edit")
    public ModelAndView edit(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
		project.setApplication(application);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		List<ProjectReview> reviewList = projectService.getProRevByProIdLx(id, "2");
		if(reviewList!=null&&reviewList.size()>0){
			for(ProjectReview review:reviewList){
				User u = userService.findById(review.getUser_id());
				review.setUser(u);
			}
		}
		modelAndView.addObject("reviewList", reviewList);
		modelAndView.setViewName("content/accept/conclude-edit");
        return modelAndView;
    }
	
	@RequestMapping(value = "save")
    public void save(Integer id, HttpServletResponse response, HttpServletRequest request, 
    		String status, String bz) throws IOException{
		JSONObject result = new JSONObject();
		try{
			ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
			application.setBz(bz);
			projectService.updateApplication(application);
			Project project = projectService.findById(id);
			project.setStatus(status);
			project.setJtsj(new Date());
			projectService.update(project);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
		project.setApplication(application);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		List<ProjectReview> reviewList = projectService.getProRevByProIdLx(id, "2");
		if(reviewList!=null&&reviewList.size()>0){
			for(ProjectReview review:reviewList){
				User u = userService.findById(review.getUser_id());
				review.setUser(u);
			}
		}
		modelAndView.addObject("reviewList", reviewList);		
		modelAndView.setViewName("content/accept/conclude-view");
		return modelAndView;
    }
	
}
