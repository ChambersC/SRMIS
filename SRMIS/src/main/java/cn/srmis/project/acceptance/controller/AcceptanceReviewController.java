package cn.srmis.project.acceptance.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.project.po.BatchSetting;
import cn.srmis.project.po.Project;
import cn.srmis.project.po.ProjectApplication;
import cn.srmis.project.po.ProjectReview;
import cn.srmis.project.po.ProjectVo;
import cn.srmis.project.service.BatchSettingService;
import cn.srmis.project.service.ProjectService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Project acceptance management - acceptance review
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/accept/review/")
@Slf4j
public class AcceptanceReviewController extends BaseContorller {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BatchSettingService batchSettingService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletRequest request, ProjectVo vo){
		ModelAndView modelAndView = new ModelAndView();
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
        vo.setExpertId(user.getId());
        vo.setClassify2("2");
        // 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        vo.setPage("5");
        //总记录数
        int resultSize = projectService.getCountByCondition(vo);  
        //显示所有  
        List<Project> pageList = projectService.getPageList(vo);  
        if(pageList!=null&&pageList.size()>0){
        	for(Project project:pageList){
        		ProjectApplication application = projectService.getProApplByProIdLx(project.getId(), "2");
        		project.setApplication(application);
        		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
        		project.setBatchSetting(batchSetting);
        		ProjectReview review = projectService.getReviewByProIdLxUserId(project.getId(), "2", user.getId());
        		project.setReview(review);
        	}
        }
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        List<BatchSetting> batchList = batchSettingService.findAll();
        modelAndView.addObject("batchList", batchList);
        modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        modelAndView.setViewName("content/accept/review-list");
		return modelAndView;
    }
	
	@RequestMapping(value = "review")
    public ModelAndView review(Integer id){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
		project.setApplication(application);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		modelAndView.setViewName("content/accept/review-review");
        return modelAndView;
    }
	
	@RequestMapping(value = "save")
    public void save(Integer id, HttpServletResponse response, HttpServletRequest request, 
    		String opinion, String comment) throws IOException{
		JSONObject result = new JSONObject();
		try{
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			ProjectReview review = projectService.getReviewByProIdLxUserId(id, "2", user.getId());
			review.setOpinion(opinion);
			review.setComment(comment);
			review.setPssj(new Date());
			projectService.updateReview(review);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "view")
    public ModelAndView view(Integer id, HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		Project project = projectService.findById(id);
		ProjectApplication application = projectService.getProApplByProIdLx(id, "2");
		project.setApplication(application);
		BatchSetting batchSetting = batchSettingService.findById(project.getBatch_id());
		project.setBatchSetting(batchSetting);
		User user = userService.findById(project.getUser_id());
		project.setUser(user);
		modelAndView.addObject("project", project);
		modelAndView.addObject("stid", application.getId());
		modelAndView.addObject("stmc", ProjectApplication.class.getSimpleName());
		
		User u = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		ProjectReview review = projectService.getReviewByProIdLxUserId(id, "2", u.getId());
		modelAndView.addObject("review", review);		
		modelAndView.setViewName("content/accept/review-view");
		return modelAndView;
    }
}
