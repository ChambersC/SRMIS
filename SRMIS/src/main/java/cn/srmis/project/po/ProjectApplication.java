package cn.srmis.project.po;

import java.util.Date;

/**
 * 项目 ~ 申请
 * @author Chambers
 * 2018
 */
public class ProjectApplication {
	
	private Integer id;
	
	private Integer project_id;	//对应项目id
	
	private String classify;	//申请类型	1-项目申请 2-结题申请
	
	private Date sqsj;	//申请时间
	
	private Date shsj;	//申请审核时间
	
	private String shr;	//审核人
	
	private String bz;	//备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProject_id() {
		return project_id;
	}

	public void setProject_id(Integer project_id) {
		this.project_id = project_id;
	}

	public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify == null ? null : classify.trim();
    }

    public Date getSqsj() {
        return sqsj;
    }

    public void setSqsj(Date sqsj) {
        this.sqsj = sqsj;
    }

    public Date getShsj() {
        return shsj;
    }

    public void setShsj(Date shsj) {
        this.shsj = shsj;
    }

    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr == null ? null : shr.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }
}