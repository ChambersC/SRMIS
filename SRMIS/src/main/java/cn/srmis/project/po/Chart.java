package cn.srmis.project.po;

/**
 * 科研成果管理 - 线性图、柱状图
 * @author Chambers
 * 2018
 */
public class Chart {

	private String name;	//批次或学科名称
	
	private int number;		//项目数量

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
}
