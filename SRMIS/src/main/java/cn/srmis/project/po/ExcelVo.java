package cn.srmis.project.po;

import java.util.Date;

/**
 * 导出项目信息ExcelVo
 * @author Chambers
 * 2018
 */
public class ExcelVo {
    
	private String batch;
	
	private String subject;
	
	private String name;
	
	private String person;
	
	private Date lxsj;	//项目立项时间
	
	private Date jtsj;	//验收结题时间

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public Date getLxsj() {
		return lxsj;
	}

	public void setLxsj(Date lxsj) {
		this.lxsj = lxsj;
	}

	public Date getJtsj() {
		return jtsj;
	}

	public void setJtsj(Date jtsj) {
		this.jtsj = jtsj;
	}

}