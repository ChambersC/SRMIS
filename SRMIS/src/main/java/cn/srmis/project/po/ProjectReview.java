package cn.srmis.project.po;

import java.util.Date;

import cn.srmis.user.po.User;

/**
 * 项目 ~ 评审
 * @author Chambers
 * 2018
 */
public class ProjectReview {
	
	private Integer id;
	
	private Integer project_id;	//对应项目id
	
	private String classify;	//评审类型	1-项目申请 2-结题申请
	
	private Date pssj;	//评审时间
	
	private String opinion;	//评审意见	（0-待评审） 1-评审通过 2-评审不通过 3-退回
	
	private String comment;	//评语
	
	private Integer user_id;	//评审专家id
	
	private User user;	//对应评审专家

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProject_id() {
		return project_id;
	}

	public void setProject_id(Integer project_id) {
		this.project_id = project_id;
	}

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify == null ? null : classify.trim();
    }

    public Date getPssj() {
        return pssj;
    }

    public void setPssj(Date pssj) {
        this.pssj = pssj;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion == null ? null : opinion.trim();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}