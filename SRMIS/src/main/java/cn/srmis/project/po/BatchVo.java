package cn.srmis.project.po;

import java.util.Date;

/**
 * 批次设置 查询条件Vo
 * @author Chambers
 * 2018
 */
public class BatchVo {
    
	private String mc;	//批次名称

    private Date kssj;	//申请开放开始时间

    private Date jssj;	//申请开放结束时间

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public Date getKssj() {
		return kssj;
	}

	public void setKssj(Date kssj) {
		this.kssj = kssj;
	}

	public Date getJssj() {
		return jssj;
	}

	public void setJssj(Date jssj) {
		this.jssj = jssj;
	}

}