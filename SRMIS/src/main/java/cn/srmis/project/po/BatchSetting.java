package cn.srmis.project.po;

import java.util.Date;

/**
 * 批次设置
 * @author Chambers
 * 2018
 */
public class BatchSetting {
	
	private Integer id;
	
	private String pcmc;	//批次名称
	
	private Date kssj;	//申请开放开始时间
	
	private Date jssj;	//申请开放结束时间
	
	private String bz;	//备注

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPcmc() {
		return pcmc;
	}

	public void setPcmc(String pcmc) {
        this.pcmc = pcmc == null ? null : pcmc.trim();
    }

    public Date getKssj() {
        return kssj;
    }

    public void setKssj(Date kssj) {
        this.kssj = kssj;
    }

    public Date getJssj() {
        return jssj;
    }

    public void setJssj(Date jssj) {
        this.jssj = jssj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

}
