package cn.srmis.project.po;

import java.util.Date;

/**
 * 项目 查询条件Vo
 * @author Chambers
 * 2018
 */
public class ProjectVo {
    
	private Integer index;
	
	private Integer number;
	
	private String mc;	//项目名称

    private Date kssj;	//开始时间（对申请）

    private Date jssj;	//结束时间
    
    private Date shkssj;	//开始时间（对审核）

    private Date shjssj;	//结束时间
    
    private Date pskssj;	//开始时间（对评审）

    private Date psjssj;	//结束时间
    
    private Date jtkssj;	//开始时间（对结题）

    private Date jtjssj;	//结束时间
    
    private Integer depId;	//科室id
    
    private Integer batchId;	//批次id
    
    private Integer userId;	//发起人id
    
    private String classify;	//类型	1-项目申请 2-结题申请
    
    private String classify2;	//评审类型	1-项目申请 2-结题申请
    
    private String status;	//状态	0-待审核 1-审核通过 2-审核失败 3-退回 4-评审中 5-已立项 6-评审不通过 7-立项退回
								//8-中期检查查阅 9-验收申请待审核 10-审核通过(结题评审中) 11-不通过退回 12-验收结题 13-评审不通过退回 
    private String page;	//0页面() 1组织专家页面(1-审核通过 >=4-评审中) 2专家评审/立项页面(>=4-评审中 5-已立项 6-评审不通过  7-立项退回)
    						//3中期检查/验收申请页面(5-已立项 >=8) 4验收审核页面(>=9-验收申请待审核) 5评审/结题页面(10-评审中 >=12)
    private Integer expertId;	//专家id
    
    public Integer getExpertId() {
		return expertId;
	}

	public void setExpertId(Integer expertId) {
		this.expertId = expertId;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public Date getKssj() {
		return kssj;
	}

	public void setKssj(Date kssj) {
		this.kssj = kssj;
	}

	public Date getJssj() {
		return jssj;
	}

	public void setJssj(Date jssj) {
		this.jssj = jssj;
	}

	public Date getShkssj() {
		return shkssj;
	}

	public void setShkssj(Date shkssj) {
		this.shkssj = shkssj;
	}

	public Date getShjssj() {
		return shjssj;
	}

	public void setShjssj(Date shjssj) {
		this.shjssj = shjssj;
	}

	public Date getPskssj() {
		return pskssj;
	}

	public void setPskssj(Date pskssj) {
		this.pskssj = pskssj;
	}

	public Date getPsjssj() {
		return psjssj;
	}

	public void setPsjssj(Date psjssj) {
		this.psjssj = psjssj;
	}

	public Date getJtkssj() {
		return jtkssj;
	}

	public void setJtkssj(Date jtkssj) {
		this.jtkssj = jtkssj;
	}

	public Date getJtjssj() {
		return jtjssj;
	}

	public void setJtjssj(Date jtjssj) {
		this.jtjssj = jtjssj;
	}

	public Integer getDepId() {
		return depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getClassify2() {
		return classify2;
	}

	public void setClassify2(String classify2) {
		this.classify2 = classify2;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

}