package cn.srmis.project.po;

import java.util.Date;
import java.util.List;

import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.user.po.User;

/**
 * 项目
 * @author Chambers
 * 2018
 */
public class Project {
	
	private Integer id;
	
	private Integer batch_id;	//对应批次id
	
	private BatchSetting batchSetting;	//对应批次内容
	
	private String name;	//项目名称
	
	private String content;	//项目内容介绍
	
	private String status;	//项目状态	0-待审核 1-审核通过 2-审核失败 3-退回 4-评审中 5-已立项 6-评审不通过 7-立项退回
								//8-中期检查查阅 9-验收申请待审核 10-审核通过(结题评审中) 11-不通过退回 12-验收结题 13-评审不通过退回 
	private double funds;	//申请经费
	
	private Date wcsj;	//预计完成时间
	
	private String subject;	//所属学科
	
	private Integer dep_id;	//所属学科对应科室id
	
	private Integer user_id;	//项目发起人id
	
	private User user;	//对应项目发起人
	
	private Date lxsj;	//项目立项处理时间
	
	private Date jtsj;	//项目结题处理时间

	private ProjectApplication application;	//对应申请内容
	
	private ProjectReview review;	//对应专家个人评审内容
	
	private List<ProjectReview> reviewList;	//对应评审内容
	
	private NbxtAttachment attachment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(Integer batch_id) {
		this.batch_id = batch_id;
	}

	public BatchSetting getBatchSetting() {
		return batchSetting;
	}

	public void setBatchSetting(BatchSetting batchSetting) {
		this.batchSetting = batchSetting;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getFunds() {
        return funds;
    }

    public void setFunds(Double funds) {
        this.funds = funds;
    }

    public Date getWcsj() {
        return wcsj;
    }

    public void setWcsj(Date wcsj) {
        this.wcsj = wcsj;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    public Date getLxsj() {
        return lxsj;
    }

    public void setLxsj(Date lxsj) {
        this.lxsj = lxsj;
    }

    public Date getJtsj() {
        return jtsj;
    }

    public void setJtsj(Date jtsj) {
        this.jtsj = jtsj;
    }

	public Integer getDep_id() {
		return dep_id;
	}

	public void setDep_id(Integer dep_id) {
		this.dep_id = dep_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ProjectApplication getApplication() {
		return application;
	}

	public void setApplication(ProjectApplication application) {
		this.application = application;
	}

	public List<ProjectReview> getReviewList() {
		return reviewList;
	}

	public void setReviewList(List<ProjectReview> reviewList) {
		this.reviewList = reviewList;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

	public ProjectReview getReview() {
		return review;
	}

	public void setReview(ProjectReview review) {
		this.review = review;
	}

	public NbxtAttachment getAttachment() {
		return attachment;
	}

	public void setAttachment(NbxtAttachment attachment) {
		this.attachment = attachment;
	}
}