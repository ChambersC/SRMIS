package cn.srmis.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class CommonMultipartFileToFile {
	/** 
     * MultipartFile 转换成File 
     *  
     * @param multfile 原文件类型 
     * @return File 
     * @throws IOException 
*/  

	public static File commonsMultipartToFile(CommonsMultipartFile commonsMultipartFile){  
        
        DiskFileItem fi = (DiskFileItem) commonsMultipartFile.getFileItem();  
        File file = fi.getStoreLocation();  
        //手动创建临时文件  
//        if(file.length() < CommonConstants.MIN_FILE_SIZE){  
//            File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") +   
//                    file.getName());  
//            commonsMultipartFile.transferTo(tmpFile);  
//            return tmpFile;  
//        }  
        return file;  
    }  

}
