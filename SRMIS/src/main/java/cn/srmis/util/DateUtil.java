p a c k a g e   c n . s r m i s . u t i l ;  
  
 i m p o r t   j a v a . t e x t . D a t e F o r m a t ;  
 i m p o r t   j a v a . t e x t . P a r s e E x c e p t i o n ;  
 i m p o r t   j a v a . t e x t . S i m p l e D a t e F o r m a t ;  
 i m p o r t   j a v a . u t i l . A r r a y L i s t ;  
 i m p o r t   j a v a . u t i l . C a l e n d a r ;  
 i m p o r t   j a v a . u t i l . D a t e ;  
 i m p o r t   j a v a . u t i l . L i s t ;  
  
 p u b l i c   c l a s s   D a t e U t i l   {  
 	 p r i v a t e   s t a t i c   D a t e F o r m a t   d a t e f m t   =   n e w   S i m p l e D a t e F o r m a t ( " y y y y - M M - d d   H H : m m : s s " ) ;  
  
         / * *  
           *   	cgqc�[<h_ԏ�V�egW[&{2N 
           *    
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *                      ���<h_S�v�eg 
           *   @ p a r a m   f o r m a t  
           *                     <h_SW[&{2N�:NzzRǑ(u؞��    y y y y - M M - d d   H H : m m : s s 0�Y�g<h_SW[&{2NNT�lO�b�Q_8^ 
           *   @ r e t u r n  
           *                     <h_ST�v�egW[&{2N0 
           * /  
         p u b l i c   s t a t i c   S t r i n g   f o r m a t D a t e ( D a t e   d a t e   ,   S t r i n g   f o r m a t ) {  
                 i f ( d a t e   = =   n u l l )  
                         r e t u r n   n u l l ;  
                 S t r i n g   s t r _ d a t e   =   n u l l ;  
                 i f ( f o r m a t   ! =   n u l l ) {  
                         D a t e F o r m a t   f o r m a t e r   =   n e w   S i m p l e D a t e F o r m a t ( f o r m a t ) ;  
                         s t r _ d a t e   =   f o r m a t e r . f o r m a t ( d a t e ) ;  
                 } e l s e {  
                         s t r _ d a t e   = d a t e f m t . f o r m a t ( d a t e ) ;  
                 }  
  
                 r e t u r n   s t r _ d a t e ;  
         }  
  
         p u b l i c   s t a t i c   D a t e   p a r s e T o D a t e ( S t r i n g   d a t e   ,   S t r i n g   f o r m a t )   t h r o w s   P a r s e E x c e p t i o n {  
                 i f ( d a t e   = =   n u l l )  
                         r e t u r n   n u l l ;  
                 D a t e   d D a t e   =   n u l l ;  
                 i f ( f o r m a t   ! =   n u l l ) {  
                         D a t e F o r m a t   f o r m a t e r   =   n e w   S i m p l e D a t e F o r m a t ( f o r m a t ) ;  
                         d D a t e   =   f o r m a t e r . p a r s e ( d a t e ) ;  
                 } e l s e {  
                         D a t e F o r m a t   f o r m a t e r   =   n e w   S i m p l e D a t e F o r m a t ( " y y y y - M M - d d " ) ;  
                         d D a t e   = f o r m a t e r . p a r s e ( d a t e ) ;  
                 }  
  
                 r e t u r n   d D a t e ;  
         }  
  
         / * *  
           *   �k��$N*N�e��/f&T�vI{0 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d 1      
           *                     �e��1  
           *   @ p a r a m   d 2  
           *                     �e��2    
           *   @ r e t u r n  
           *                     �vI{Rt r u e 0�V:Npenc�^-N���Q�vpenc:NT i m e s t a m p {|�W( D a t e �vP[{|) � 
           *   S_�[ND a t e {|�WۏL��k���e, ;`/f:Nf a l s e , sSO/fT N*N�e��. �Vdk�Q�Nُ*N�e�l, (u�N|Q�[ُ$N�y{|�W�v�e���k��.  
           * /  
         p u b l i c   s t a t i c   b o o l e a n   e q u a l s D a t e ( D a t e   d 1   ,   D a t e   d 2 ) {  
                 i f ( d 1   ! = n u l l   & &   d 2 ! =   n u l l ) {  
                         r e t u r n   d 1 . g e t T i m e ( )   = =   d 2 . g e t T i m e ( ) ;  
                 }  
                 r e t u r n   f a l s e ;  
         }  
  
         / * *  
           *   $R�eTb��v N)Y/f&T/fMRb� N)Y�vN N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a y        
           *                     �W�Q�eg 
           *   @ p a r a m   n e x t D a y  
           *                     �k���eg 
           *   @ r e t u r n  
           *                     �Y�g�k���eg/f�W�Q�eg�vN N)YRԏ�Vt r u e �&TR:Nf a l s e  
           * /  
         p u b l i c   s t a t i c   b o o l e a n   i s N e x t D a y ( D a t e   d a y , D a t e   n e x t D a y ) {  
                 r e t u r n   (   g e t B e t w e e n D a y s ( d a y   , n e x t D a y )   = =   - 1   ) ;  
         }  
  
         / * *  
           *   $R�e$N*N�eg/f&T/fT N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a y  
           *   @ p a r a m   o t h e r D a y  
           *   @ r e t u r n  
           * /  
         p u b l i c   s t a t i c   b o o l e a n   i s S a m e D a y ( D a t e   d a y , D a t e   o t h e r D a y ) {  
                 r e t u r n   (   g e t B e t w e e n D a y s ( d a y   , o t h e r D a y )   = =   0   ) ;  
         }  
  
         / * *  
           *   ���{$N*N�eg�v�]�v)Ype. N�n2 4 \�eN�{ZP N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   f D a t e           �eg1  
           *   @ p a r a m   o D a t e           �eg2  
           *   @ r e t u r n  
           *             �eg1   -   �eg2   �v�] 
           * /  
         p u b l i c   s t a t i c   i n t   g e t B e t w e e n D a y s ( D a t e   f D a t e ,   D a t e   s D a t e )   {  
                 i n t   d a y = ( i n t ) ( ( f D a t e . g e t T i m e ( ) - s D a t e . g e t T i m e ( ) ) / 8 6 4 0 0 0 0 0 L ) ; / / ( 2 4 \�e  *   6 0 R  *   6 0 �y  *   1 0 0 0 �k�y  =   1 )Y�k�ype)    
                 r e t u r n   d a y ;  
         }  
  
         / * *  
           *   �eg�v�Rc�[t^ 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ p a r a m   a d d Y e a r s  
           *             ���m�R�vt^pe 
           *   @ r e t u r n  
           *             �v�RT�v�eg 
           * /  
         p u b l i c   s t a t i c   D a t e   a d d Y e a r s ( D a t e   d a t e   ,   i n t   a d d Y e a r s ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 c a l e n d e r . a d d ( C a l e n d a r . Y E A R ,   a d d Y e a r s ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
         / * *  
           *   �Rc�[g 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ p a r a m   a d d M o n t h s  
           *             gpe 
           *   @ r e t u r n  
           *             �v�RT�v�eg 
           * /  
         p u b l i c   s t a t i c   D a t e   a d d M o n t h ( D a t e   d a t e   ,   i n t   a d d M o n t h s ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 c a l e n d e r . a d d ( C a l e n d a r . M O N T H ,   a d d M o n t h s ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   �Rc�[)Ype 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ p a r a m   a d d D a y s  
           *             )Ype 
           *   @ r e t u r n  
           *             �v�RT�v�eg 
           * /  
         p u b l i c   s t a t i c   D a t e   a d d D a y ( D a t e   d a t e   ,   i n t   a d d D a y s ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 c a l e n d e r . a d d ( C a l e n d a r . D A Y _ O F _ Y E A R ,   a d d D a y s ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   �_0R Nt^�v,{ N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   y e a r  
           *             t^ 
           *   @ r e t u r n  
           *              Nt^�v,{ N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t F i r s t D a t e O f Y e a r ( i n t   y e a r ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . Y E A R , y e a r ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ Y E A R ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . D A Y _ O F _ Y E A R ) ) ;  
                 s e t S t a r t T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   �_0R Nt^�v gT N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   y e a r  
           *             t^ 
           *   @ r e t u r n  
           *              Nt^�v gT N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t L a s t D a t e O f Y e a r ( i n t   y e a r ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . Y E A R , y e a r ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ Y E A R ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . D A Y _ O F _ Y E A R ) ) ;  
                 s e t E n d T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   $R�eS_MR�eg/f&T/f@b(Wg�N�v gT N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ r e t u r n  
           *             /f gT N)Y:N  t r u e  
           * /  
         p u b l i c   s t a t i c   b o o l e a n   i s L a s t D a y O f M o n t h ( D a t e   d a t e )   {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 i n t   d a y   =   c a l e n d e r . g e t ( C a l e n d a r . D A Y _ O F _ M O N T H ) ;  
                 i n t   l a s t D a y   =   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . D A Y _ O F _ M O N T H ) ;  
                 r e t u r n   d a y   = =   l a s t D a y   ;  
         }  
  
         / * *  
           *   �_0Rc�[g�v gT N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   y e a r  
           *             t^ 
           *   @ p a r a m   m o n t h  
           *             g 
           *   @ r e t u r n  
           *              gT N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t L a s t D a y O f M o n t h ( i n t   y e a r   ,   i n t   m o n t h ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t ( y e a r ,   m o n t h - 1 ,   1 ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ M O N T H ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . D A Y _ O F _ M O N T H ) ) ;  
                 s e t E n d T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   �_0R�eg@b(Wg�v gT N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ r e t u r n  
           *             @b(Wg�v gT N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t L a s t D a y O f M o n t h ( D a t e   d a t e ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ M O N T H ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . D A Y _ O F _ M O N T H ) ) ;  
                 s e t E n d T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   ��n0RS_MRg�v gT�e;R 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   c a l e n d e r  
           * /  
         p r i v a t e   s t a t i c   v o i d   s e t E n d T i m e O f D a y ( C a l e n d a r   c a l e n d e r ) {  
                 c a l e n d e r . s e t ( C a l e n d a r . H O U R _ O F _ D A Y ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . H O U R _ O F _ D A Y ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . M I N U T E ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . M I N U T E ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . S E C O N D ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . S E C O N D ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . M I L L I S E C O N D ,   c a l e n d e r . g e t A c t u a l M a x i m u m ( C a l e n d a r . M I L L I S E C O N D ) ) ;  
         }  
  
         / * *  
           *   �_0Rc�[g�v,{ N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   y e a r  
           *             t^ 
           *   @ p a r a m   m o n t h  
           *             g 
           *   @ r e t u r n  
           *             ,{ N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t F i r s t D a y O f M o n t h ( i n t   y e a r   ,   i n t   m o n t h ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t ( y e a r ,   m o n t h - 1 ,   1 ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ M O N T H ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . D A Y _ O F _ M O N T H ) ) ;  
                 s e t S t a r t T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   �_0Rc�[�eg@b(Wg�v,{ N)Y 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e  
           *             �eg 
           *   @ r e t u r n  
           *             ,{ N)Y 
           * /  
         p u b l i c   s t a t i c   D a t e   g e t F i r s t D a y O f M o n t h ( D a t e   d a t e ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . D A Y _ O F _ M O N T H ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . D A Y _ O F _ M O N T H ) ) ;  
                 s e t S t a r t T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         / * *  
           *   ��n0Rg�N _�Y�v�e;R 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   c a l e n d e r  
           * /  
         p r i v a t e   s t a t i c   v o i d   s e t S t a r t T i m e O f D a y ( C a l e n d a r   c a l e n d e r ) {  
                 c a l e n d e r . s e t ( C a l e n d a r . H O U R _ O F _ D A Y ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . H O U R _ O F _ D A Y ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . M I N U T E ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . M I N U T E ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . S E C O N D ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . S E C O N D ) ) ;  
                 c a l e n d e r . s e t ( C a l e n d a r . M I L L I S E C O N D ,   c a l e n d e r . g e t A c t u a l M i n i m u m ( C a l e n d a r . M I L L I S E C O N D ) ) ;  
         }  
  
         p u b l i c   s t a t i c   D a t e   g e t S t a r t T i m e O f D a y ( D a t e   d a t e ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 s e t S t a r t T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
         }  
  
         p u b l i c   s t a t i c   D a t e   g e t E n d T i m e O f D a y ( D a t e   d a t e ) {  
                 C a l e n d a r   c a l e n d e r   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 c a l e n d e r . s e t T i m e ( d a t e ) ;  
                 s e t E n d T i m e O f D a y ( c a l e n d e r ) ;  
                 r e t u r n   c a l e n d e r . g e t T i m e ( ) ;  
  
         }  
  
         / * *  
           *   �_0RS_MRt^g 
           *    
           *   @ r e t u r n   <h_�2 0 0 8 - 1 1  
           *   @ t h r o w s   P a r s e E x c e p t i o n  
           * /  
         p u b l i c   s t a t i c     S t r i n g   g e t T h i s Y e a r M o n t h ( )   t h r o w s   P a r s e E x c e p t i o n   {  
                 r e t u r n   g e t Y e a r M o n t h ( n e w   D a t e ( ) ) ;  
         }  
  
         / * *  
           *   �_0Rt^g 
           *    
           *   @ r e t u r n   <h_�2 0 0 8 - 1 1  
           *   @ t h r o w s   P a r s e E x c e p t i o n  
           * /  
         p u b l i c   s t a t i c     S t r i n g     g e t Y e a r M o n t h ( D a t e   d a t e ) {  
                 C a l e n d a r   t o d a y   =   C a l e n d a r . g e t I n s t a n c e ( ) ;  
                 t o d a y . s e t T i m e ( d a t e ) ;  
                 r e t u r n   ( t o d a y . g e t ( C a l e n d a r . Y E A R ) )   +   " - "   +   ( ( t o d a y . g e t ( C a l e n d a r . M O N T H ) + 1 ) > = 1 0 ? ( t o d a y . g e t ( C a l e n d a r . M O N T H ) + 1 ) : ( " 0 " + ( t o d a y . g e t ( C a l e n d a r . M O N T H )   +   1 ) ) ) ;  
         }  
  
         / * *  
           *   ���{$N*N�egKN���v�]�vg�Npe 
           *   < b r >   �egz��^NRHQTNOԏ�V�pe 
           *   < b r >   N�� N*NgN�{ZP N*Ng 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e 1  
           *             �eg1  
           *   @ p a r a m   d a t e 2  
           *             �eg2  
           *   @ r e t u r n  
           *             gpe 
           * /  
         p u b l i c   s t a t i c   i n t   g e t B e t w e e n M o n t h s ( D a t e   d a t e 1 ,   D a t e   d a t e 2 ) {              
                 i n t   i M o n t h   =   0 ;              
                 i n t   f l a g   =   0 ;              
                 C a l e n d a r   o b j C a l e n d a r D a t e 1   =   C a l e n d a r . g e t I n s t a n c e ( ) ;              
                 o b j C a l e n d a r D a t e 1 . s e t T i m e ( d a t e 1 ) ;              
  
                 C a l e n d a r   o b j C a l e n d a r D a t e 2   =   C a l e n d a r . g e t I n s t a n c e ( ) ;              
                 o b j C a l e n d a r D a t e 2 . s e t T i m e ( d a t e 2 ) ;              
  
                 i f   ( o b j C a l e n d a r D a t e 2 . e q u a l s ( o b j C a l e n d a r D a t e 1 ) )              
                         r e t u r n   0 ;              
                 i f   ( o b j C a l e n d a r D a t e 1 . a f t e r ( o b j C a l e n d a r D a t e 2 ) ) {              
                         C a l e n d a r   t e m p   =   o b j C a l e n d a r D a t e 1 ;              
                         o b j C a l e n d a r D a t e 1   =   o b j C a l e n d a r D a t e 2 ;              
                         o b j C a l e n d a r D a t e 2   =   t e m p ;              
                 }              
                 i f   ( o b j C a l e n d a r D a t e 2 . g e t ( C a l e n d a r . D A Y _ O F _ M O N T H )   <   o b j C a l e n d a r D a t e 1 . g e t ( C a l e n d a r . D A Y _ O F _ M O N T H ) )              
                         f l a g   =   1 ;              
  
                 i f   ( o b j C a l e n d a r D a t e 2 . g e t ( C a l e n d a r . Y E A R )   >   o b j C a l e n d a r D a t e 1 . g e t ( C a l e n d a r . Y E A R ) )              
                         i M o n t h   =   ( ( o b j C a l e n d a r D a t e 2 . g e t ( C a l e n d a r . Y E A R )   -   o b j C a l e n d a r D a t e 1 . g e t ( C a l e n d a r . Y E A R ) )              
                                         *   1 2   +   o b j C a l e n d a r D a t e 2 . g e t ( C a l e n d a r . M O N T H )   -   f l a g )              
                                         -   o b j C a l e n d a r D a t e 1 . g e t ( C a l e n d a r . M O N T H ) ;              
                 e l s e            
                         i M o n t h   =   o b j C a l e n d a r D a t e 2 . g e t ( C a l e n d a r . M O N T H )              
                                         -   o b j C a l e n d a r D a t e 1 . g e t ( C a l e n d a r . M O N T H )   -   f l a g ;              
  
                 r e t u r n   i M o n t h ;              
         }  
  
         / * *  
           *   ���{$N*N�egKN���v�]�vt^�Npe 
           *   < b r >   �egz��^NRHQTNOԏ�V�pe 
           *   < b r >   N�� N*Nt^N�{ZP N*Nt^ 
           *   @ m o d i f y N o t e  
           *   @ p a r a m   d a t e 1  
           *             �eg1  
           *   @ p a r a m   d a t e 2  
           *             �eg2  
           *   @ r e t u r n  
           *             t^pe 
           * /  
         p u b l i c   s t a t i c   i n t   g e t B e t w e e n Y e a r s ( D a t e   d a t e 1 ,   D a t e   d a t e 2 ) {      
                 r e t u r n   g e t B e t w e e n M o n t h s ( d a t e 1   , d a t e 2 )   /   1 2 ;  
         }  
          
         / * *  
 	   *   l�eQ$N*N�e��{|�W�v�eP��Tԏ�V�v�v�]{|+R 
 	   *   @ a u t h o r   �h�eOe 
 	   *   @ p a r a m   t y p e ( n d = )Ype�n h = \�e�n m = R���n s = �y��, m s = �k�y)  
 	   *   @ t h r o w s   P a r s e E x c e p t i o n    
 	   *   * * /  
 	 p u b l i c   s t a t i c   l o n g   d a t e D i f f ( D a t e   s t a r t T i m e ,   D a t e   e n d T i m e ,   S t r i n g   t y p e )   t h r o w s   P a r s e E x c e p t i o n   {  
 	 	 l o n g   v a l u e   =   0 ;  
 	 	 l o n g   n d   =   1 0 0 0 * 2 4 * 6 0 * 6 0 ; / /  N)Y�v�k�ype       
 	 	 l o n g   n h   =   1 0 0 0 * 6 0 * 6 0 ; / /  N\�e�v�k�ype       
 	 	 l o n g   n m   =   1 0 0 0 * 6 0 ; / /  NR���v�k�ype       
 	 	 l o n g   n s   =   1 0 0 0 ; / /  N�y���v�k�ype       
 	 	 l o n g   d i f f   =   e n d T i m e . g e t T i m e ( )   -   s t a r t T i m e . g e t T i m e ( ) ;       / / ���_$N*N�e���v�k�y�e���]_ 
 	 	 i f ( t y p e . e q u a l s ( " n d " ) )  
 	 	 	 v a l u e   =   d i f f / n d ; / / ���{�]Y\)Y       
 	 	 e l s e   i f ( t y p e . e q u a l s ( " n h " ) )  
 	 	 	 v a l u e   =   d i f f / n h ; / / ���{�]Y\\�e       
 	 	 e l s e   i f ( t y p e . e q u a l s ( " n m " ) )  
 	 	 	 v a l u e   =   d i f f / n m ; / / ���{�]Y\R��       
 	 	 e l s e   i f ( t y p e . e q u a l s ( " n s " ) )  
 	 	 	 v a l u e   =   d i f f / n s ; / / ���{�]Y\�y 
 	 	 e l s e   i f ( t y p e . e q u a l s ( " m s " ) )  
 	 	 	 v a l u e   =   d i f f ; / / ���{�]Y\�k�y 
 	 	 r e t u r n   v a l u e ;  
 	 }  
  
         p u b l i c   s t a t i c   v o i d   m a i n ( S t r i n g [ ]   a r g s )   t h r o w s   E x c e p t i o n   {  
 / /             D a t e   d 1   =   p a r s e T o D a t e ( " 2 0 0 9 - 1 1 - 2 9 " ,   n u l l ) ;  
 / /             D a t e   d 2   =   p a r s e T o D a t e ( " 2 0 0 7 - 1 2 - 2 9 " ,   n u l l ) ;  
                 S y s t e m . o u t . p r i n t l n ( f o r m a t D a t e ( g e t F i r s t D a y O f M o n t h ( 2 0 1 0 , 1 0 ) , " y y y y - M M - d d   H H : m m : s s . S S S " ) ) ;  
  
                 S y s t e m . o u t . p r i n t l n ( f o r m a t D a t e ( g e t L a s t D a t e O f Y e a r ( 2 0 0 9 ) , " y y y y - M M - d d   H H : m m : s s . S S S " ) ) ;  
                 S y s t e m . o u t . p r i n t l n ( f o r m a t D a t e ( g e t F i r s t D a t e O f Y e a r ( 2 0 0 9 ) , " y y y y - M M - d d   H H : m m : s s . S S S " ) ) ;  
                 S y s t e m . o u t . p r i n t l n ( f o r m a t D a t e ( g e t E n d T i m e O f D a y ( n e w   D a t e ( ) ) , " y y y y - M M - d d   H H : m m : s s . S S S " ) ) ;  
         }  
          
          
         / * *  
           *   9hnc�eg���_ NhT�v�eg   
           *   @ p a r a m   m d a t e  
           *   @ r e t u r n  
           *   @ a u t h o r   4T	V�� 
           * /  
         @ S u p p r e s s W a r n i n g s ( " d e p r e c a t i o n " )  
         p u b l i c   s t a t i c   L i s t < D a t e >   d a t e T o W e e k ( D a t e   m d a t e )   {  
                 i n t   b   =   m d a t e . g e t D a y ( ) ;  
                 D a t e   f d a t e ;  
                 L i s t < D a t e >   l i s t   =   n e w   A r r a y L i s t < D a t e > ( ) ;  
                 L o n g   f T i m e   =   m d a t e . g e t T i m e ( )   -   b   *   2 4   *   3 6 0 0 0 0 0 ;  
                 f o r   ( i n t   a   =   1 ;   a   < =   7 ;   a + + )   {  
                         f d a t e   =   n e w   D a t e ( ) ;  
                         f d a t e . s e t T i m e ( f T i m e   +   ( a   *   2 4   *   3 6 0 0 0 0 0 ) ) ;  
                         l i s t . a d d ( a - 1 ,   f d a t e ) ;  
                 }  
                 r e t u r n   l i s t ;  
         }  
  
 }  
 