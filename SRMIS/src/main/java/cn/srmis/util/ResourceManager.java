package cn.srmis.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;


/**
 * 上传的文件类资源文件的管理。
 *
 */
public class ResourceManager {
	
	/**
	 * 下载指定的服务器资源文件。
	 * @param filepath
	 */
	public static void download(String filepath, String name,  HttpServletResponse response) {
		download(filepath, name, null, response);
	}
	
	/**
	 * 下载指定的服务器资源文件，并且指定下载类型
	 * @param filepath
	 */
	public static void download(String filepath, String name, String contentType,  HttpServletResponse resp) {
		OutputStream os = null;
		InputStream is = null;
		try {
			ResourceVO vo = getResource(filepath);
			if (vo == null) return;
			//HttpServletResponse resp  = ServletActionContext.getResponse();
			resp.reset();
			String filename = new String(name.getBytes("GBK"), "ISO8859-1");
			resp.setHeader("Content-Disposition", "attachment;filename=" + filename);
			resp.setHeader("Content-Length", vo.getFile().length() + "");
			resp.setContentType(contentType == null ? "application/octet-stream" : contentType);
			os = new BufferedOutputStream(resp.getOutputStream());
			is = new BufferedInputStream(new FileInputStream(vo.getFile()));
			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
			}
			os.flush();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				if (is != null) is.close();
			} catch (Exception ex) {}
			try {
				if (os != null) os.close();
			} catch (Exception ex) {}
		}
	}
	
	/**
	 * 下载指定的服务器cvs文件，并且指定下载类型
	 * @param filepath
	 */
	public static void downloads(String filepath, String name, String contentType) {
		OutputStream os = null;
		InputStream is = null;
		try {
			ResourceVO vo = getResource(filepath);
			if (vo == null) return;
			HttpServletResponse resp = null;//= ServletActionContext.getResponse();
			String filename = new String(name.getBytes("GBK"), "ISO8859-1");
			resp.setHeader("Content-Disposition", "attachment;filename=" + filename);
			resp.setHeader("Content-Length", vo.getFile().length() + "");
			resp.setContentType(contentType == null ? "application/octet-stream" : contentType);
			os = new BufferedOutputStream(resp.getOutputStream());
			is = new BufferedInputStream(new FileInputStream(vo.getFile()));
			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
			}
			os.flush();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				if (is != null) is.close();
			} catch (Exception ex) {}
			try {
				if (os != null) os.close();
			} catch (Exception ex) {}
		}
	}
	
	/**
	 * 添加文件到指定的根目录下
	 * @param path  相对web根目录的路径
	 * @param file
	 * @return
	 */
	public static ResourceVO addResource(String path, File file, String filename) {
		String destFolder = path + "/" + generatePath();
		return copy(file, filename, destFolder);
	}
	
	/**
	 * 根据给定的路径获取该文件的信息，不存在文件则返回Null
	 * @param filepath 相对web服务器根路径
	 * @return
	 */
	public static ResourceVO getResource(String filepath) {
		//String realpath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/");
    	String realpath = ContextLoader
                .getCurrentWebApplicationContext().getServletContext().getRealPath("/");
    	File file = new File(realpath + filepath);
		if (!file.isFile()) return null;
		ResourceVO vo = new ResourceVO();
		vo.setFile(file);
		vo.setFilepath(filepath);
		vo.setSuffix(getSuffix(file.getName()));
		return vo;
	}
	
	/**
	 * 生成路径信息,根据年月日.
	 * @return
	 */
	private static String generatePath() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1);
	}
	
	/**
	 * 复制给定的文件到指定的目录下
	 * @param file
	 * @param destFolder
	 */
	private static ResourceVO copy(File file, String filename, String destFolder) {
		FileChannel input = null, output = null;
		FileOutputStream fos=null;
		try {
			String[] imageFileExt = {"jpg","jpeg","png","bmp","gif"};
			String suffix = getSuffix(filename).toLowerCase().intern();
			/*临时注释图片自动压缩功能，2017-03-06
			 * if(isHave(imageFileExt,suffix)){
				Image img = ImageIO.read(file);
				int[] newSize=getImageNewSize(img.getWidth(null), img.getHeight(null));
				BufferedImage buffImg = new BufferedImage((int) newSize[0], (int) newSize[1], BufferedImage.TYPE_INT_RGB);
				Graphics g = buffImg.getGraphics();	//得到画笔对象
				g.drawImage(img.getScaledInstance(newSize[0], newSize[1], Image.SCALE_SMOOTH), 0, 0, null);	//压缩图片
				g.dispose();
				fos = new FileOutputStream(file);
				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(fos);
				encoder.encode(buffImg);
				buffImg = null;
			}*/
			input = new FileInputStream(file).getChannel();
			//String realpath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/");
	    	String realpath = ContextLoader
	                .getCurrentWebApplicationContext().getServletContext().getRealPath("/");
	    	
	    	String destFile = destFolder + "/" + filename;
//	    	String destFile = destFolder + "/" + UUID.randomUUID().toString();
//	    	if (suffix != "") destFile += "." + suffix;
			
	    	File dest = new File(realpath + "/" + destFile);
			if (!dest.getParentFile().isDirectory()) dest.getParentFile().mkdirs();
			if (!dest.isFile()) dest.createNewFile();
			output = new FileOutputStream(dest).getChannel();
			output.transferFrom(input, 0, input.size());
			ResourceVO vo = new ResourceVO();
			vo.setFilepath(destFile);
			vo.setSuffix(suffix);
			vo.setFile(dest);
			
			if(suffix.equals("pdf")){
				int pdfNumberOfPages=PdfBoxUtil.pdfToImg(dest.getPath(), dest.getPath().replace(".pdf", ".jpg"));
				vo.setPdfNumberOfPages(pdfNumberOfPages);
			}
			
			return vo;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				if (input != null) input.close();
			} catch (Exception ex) {}
			try {
				if (output != null) output.close();
			} catch (Exception ex) {}
			if(fos!=null)
				try {
					fos.close();
				} catch (IOException e) {}
			fos = null;
		}
	}
	
	private static boolean isHave(String[] strs, String s) {
		//循环查找字符串数组中的每个字符串中是否包含所有查找的内容
		for (int i = 0; i < strs.length; i++) {
			if (strs[i].indexOf(s) != -1) {
				return true;//查找到了就返回真，不在继续查询
			}
		}
		return false;//没找到返回false
	}
	
    
	private static int[] getImageNewSize(double oldWidth,double oldHeight) throws Exception{
		int width = 1024; //默认输出图片宽   
	    int height = 768; //默认输出图片高
		// 为等比缩放计算输出的图片宽度及高度
		double rate1 = oldWidth/(double)width;
        double rate2 = oldHeight/(double)height;
        // 根据缩放比率大的进行缩放控制
        double rate = rate1 > rate2 ? rate1 : rate2;    
        int newWidth = (int) (((double)oldWidth)/rate);
        int newHeight = (int) (((double)oldHeight)/rate);
        return new int[]{newWidth,newHeight};
	}
	
	
	/**
	 * 获取文件名的后缀
	 * @param filename
	 * @return
	 */
	private static String getSuffix(String filename) {
		int index = filename.lastIndexOf(".");
		if (index == -1) return "";
		return filename.substring(index + 1);
	}
	
    /**   
     * 删文件   
     * @param deleteFilePath   
     *          删除文件路径   
     */   
    public static void deleteFile(final String deleteFilePath){ 
    	//String realpath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/");
    	String realpath = ContextLoader
                .getCurrentWebApplicationContext().getServletContext().getRealPath("/");
    	File file=new File(realpath+deleteFilePath);    
        deleteFile(file);    
        file.delete();    
    }    
        
    /**   
     * 实施删除文件   
     * @param file   
     */   
    public static void deleteFile(File file){    
        if(file.isDirectory()){    
            File[] fl = file.listFiles();    
            int indexSize=fl.length;    
            for(int i=0; i<indexSize; i++){    
                deleteFile(fl[i]);    
            }    
            /*此处删除目录*/   
            file.delete();    
        }else{    
            /*此处删除文件*/   
            file.delete();
        }    
    } 
    /**   
     * 上传任意文件（重要文档）  移植到web-inf下，防止他人通过非法获取   
     * @param file   
     */ 
	public static ResourceVO addResource(File file, String filename) {
		String destFolder = WebCommon.getInstance().getString("upload.root") + "/" + generatePath();
		return copy(file, filename, destFolder);
	}
	
    /**   
     * 上传任意文件  上传图片目录  
     * @param file   
     */ 
	public static ResourceVO addResourceByImage(File file, String filename) {
		String destFolder = WebCommon.getInstance().getString("upload.image") + "/" + generatePath();
		return copy(file, filename, destFolder);
	}
	   /**   
     * 上传
     * @param pdf文件   
     */ 
	public static ResourceVO addResourceForPdf(File file, String filename) {
		String destFolder = WebCommon.getInstance().getString("upload.pdf") + "/" + generatePath();
		return copy(file, filename, destFolder);
	}
	   /**   
     * 上传swf文件   
     * @param file   
     */ 
	public static ResourceVO addResourceForSwf(File file, String filename) {
		String destFolder = WebCommon.getInstance().getString("upload.swf") + "/" + generatePath();
		return copy(file, filename, destFolder);
	}
}
