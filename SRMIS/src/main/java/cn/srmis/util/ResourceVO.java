package cn.srmis.util;

import java.io.File;
import java.io.Serializable;

/**
 * 表示一个服务器上的资源文件信息。
 *
 */
public class ResourceVO implements Serializable {
	private static final long serialVersionUID = 8487393907198135318L;
	private String filepath;
	private String suffix;
	private File file;
	private int pdfNumberOfPages;

	/**
	 * 获取该文件的相对路径，相对web服务器的根路径而言
	 * @return
	 */
	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	/**
	 * 获取该文件的后缀，如果没有后缀则返回空串
	 * @return
	 */
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	/**
	 * 表示该文件的文件对象
	 * @return
	 */
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public int getPdfNumberOfPages() {
		return pdfNumberOfPages;
	}

	public void setPdfNumberOfPages(int pdfNumberOfPages) {
		this.pdfNumberOfPages = pdfNumberOfPages;
	}
}
