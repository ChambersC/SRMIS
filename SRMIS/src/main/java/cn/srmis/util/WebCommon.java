package cn.srmis.util;



import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;


public class WebCommon {
	private static final WebCommon instance = new WebCommon();
	private Map<String,Object> config = new HashMap<String,Object>();
	
	private WebCommon() {
		try {
			Properties prop = new Properties();
			prop.load(WebCommon.class.getResourceAsStream("/web-common.properties"));
			Iterator<Object> it = prop.keySet().iterator();
			while (it.hasNext()) {
				String key = (String)it.next();
				config.put(key, prop.get(key));
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * 获取配置的唯一实例
	 * @return
	 */
	public static WebCommon getInstance() {
		return instance;
	}
	
	/**
	 * 获取配置的值。
	 * @param key
	 * @return
	 */
	public Object getValue(String key) {
		return config.get(key);
	}
	
	/**
	 * 获取字符串值
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		return (String)getValue(key);
	}
	
	/**
	 * 获取数字
	 * @param key
	 * @return
	 */
	public int getInt(String key) {
		return Integer.parseInt(getString(key).trim());
	}
}