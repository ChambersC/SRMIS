package cn.srmis.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.core.convert.converter.Converter;

/**
 * ʱ��ת����
 * @author Chambers
 * 
 */

public class DateConverter implements Converter<String, Date> {
	
    public Date convert(String source) {
        if(source.length() == 10) {
            try {
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                return sdf.parse(source);
            } catch (ParseException e) {
                e.printStackTrace();  

            }
        } else if(source.length() == 19) {
            try {
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                return sdf.parse(source);
            } catch (ParseException e) {
                e.printStackTrace();  
            }
        }
        return null;
    }
}
