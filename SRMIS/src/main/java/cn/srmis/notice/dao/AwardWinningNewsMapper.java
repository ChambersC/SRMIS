package cn.srmis.notice.dao;

import java.util.Date;
import java.util.List;

import cn.srmis.notice.po.AwardWinningNews;

/**
 * ��ϲѶDAO��
 * @author Chambers
 * 
 */
public interface AwardWinningNewsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AwardWinningNews record);

    int insertSelective(AwardWinningNews record);

    AwardWinningNews selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AwardWinningNews record);

    int updateByPrimaryKey(AwardWinningNews record);
    
    
    List<AwardWinningNews> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize);

	int getCountByCondition(String btmc, Date fbkssj, Date fbjssj);

	int getAllCount();

	List<AwardWinningNews> findFbAll();

	AwardWinningNews getNewsByCondition(String bh, Integer id);
}