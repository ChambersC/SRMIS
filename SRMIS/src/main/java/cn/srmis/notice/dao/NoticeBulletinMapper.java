package cn.srmis.notice.dao;

import java.util.Date;
import java.util.List;

import cn.srmis.notice.po.NoticeBulletin;

/**
 * 通知公告DAO类
 * @author Chambers
 * 
 */
public interface NoticeBulletinMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(NoticeBulletin record);

    int insertSelective(NoticeBulletin record);

    NoticeBulletin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NoticeBulletin record);

    int updateByPrimaryKey(NoticeBulletin record);

    
	List<NoticeBulletin> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize);

	int getCountByCondition(String btmc, Date fbkssj, Date fbjssj);

	int getAllCount();

	List<NoticeBulletin> findFbAll();

	NoticeBulletin getNoticeByCondition(String bh, Integer id);

}