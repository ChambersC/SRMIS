package cn.srmis.notice.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.notice.dao.AwardWinningNewsMapper;
import cn.srmis.notice.po.AwardWinningNews;

/**
 * 获奖喜讯业务类实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class AwardWinningNewsServiceImpl implements AwardWinningNewsService {

    @Autowired
    private AwardWinningNewsMapper awardWinningNewsMapper;
    
    @Override
	public List<AwardWinningNews> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return awardWinningNewsMapper.getPageList(btmc, fbkssj, fbjssj, pageIndex, pageSize);
	}

	@Override
	public int getCountByCondition(String btmc, Date fbkssj, Date fbjssj) {
		// TODO Auto-generated method stub
		if(StringUtils.isNotEmpty(btmc)||fbkssj!=null||fbjssj!=null)
			return awardWinningNewsMapper.getCountByCondition(btmc, fbkssj, fbjssj);
		else
			return this.getAllCount();
	}
	
	@Override
	public int getAllCount() {
		// TODO Auto-generated method stub
		return awardWinningNewsMapper.getAllCount();
	}

	@Override
	public AwardWinningNews findById(Integer id) {
		// TODO Auto-generated method stub
		return awardWinningNewsMapper.selectByPrimaryKey(id);
	}

	@Override
	public void save(AwardWinningNews awardWinningNews) {
		// TODO Auto-generated method stub
		awardWinningNewsMapper.insert(awardWinningNews);
	}

	@Override
	public void update(AwardWinningNews awardWinningNews) {
		// TODO Auto-generated method stub
		awardWinningNewsMapper.updateByPrimaryKey(awardWinningNews);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		awardWinningNewsMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(AwardWinningNews awardWinningNews) {
		// TODO Auto-generated method stub
		awardWinningNewsMapper.updateByPrimaryKeySelective(awardWinningNews);
	}

	@Override
	public List<AwardWinningNews> findFbAll() {
		// TODO Auto-generated method stub
		return awardWinningNewsMapper.findFbAll();
	}

	@Override
	public AwardWinningNews getNewsByCondition(String bh, Integer id) {
		// TODO Auto-generated method stub
		return awardWinningNewsMapper.getNewsByCondition(bh, id);
	}

}
