package cn.srmis.notice.service;

import java.util.Date;
import java.util.List;

import cn.srmis.notice.po.AwardWinningNews;

/**
 * 获奖喜讯业务类
 * @author Chambers
 * 
 */
public interface AwardWinningNewsService {

	List<AwardWinningNews> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize);

	int getAllCount();

	int getCountByCondition(String btmc, Date fbkssj, Date fbjssj);

	AwardWinningNews findById(Integer id);

	void save(AwardWinningNews awardWinningNews);

	void update(AwardWinningNews awardWinningNews);

	void delete(Integer id);

	void updateByPrimaryKeySelective(AwardWinningNews awardWinningNews);

	List<AwardWinningNews> findFbAll();

	AwardWinningNews getNewsByCondition(String bh, Integer id);
	
}
