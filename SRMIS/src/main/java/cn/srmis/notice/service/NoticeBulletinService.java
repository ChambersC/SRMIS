package cn.srmis.notice.service;

import java.util.Date;
import java.util.List;

import cn.srmis.notice.po.NoticeBulletin;

/**
 * 通知公告业务类
 * @author Chambers
 * 
 */
public interface NoticeBulletinService {

	List<NoticeBulletin> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize);

	int getAllCount();

	int getCountByCondition(String btmc, Date fbkssj, Date fbjssj);

	NoticeBulletin findById(Integer id);

	void save(NoticeBulletin noticeBulletin);

	void update(NoticeBulletin noticeBulletin);

	void delete(Integer id);

	void updateByPrimaryKeySelective(NoticeBulletin noticeBulletin);

	List<NoticeBulletin> findFbAll();

	NoticeBulletin getNoticeByCondition(String bh, Integer id);
	
}
