package cn.srmis.notice.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.notice.dao.NoticeBulletinMapper;
import cn.srmis.notice.po.NoticeBulletin;

/**
 * 通知公告业务类实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class NoticeBulletinServiceImpl implements NoticeBulletinService {

    @Autowired
    private NoticeBulletinMapper noticeBulletinMapper;
    
    @Override
	public List<NoticeBulletin> getPageList(String btmc, Date fbkssj, Date fbjssj, 
			int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return noticeBulletinMapper.getPageList(btmc, fbkssj, fbjssj, pageIndex, pageSize);
	}

	@Override
	public int getCountByCondition(String btmc, Date fbkssj, Date fbjssj) {
		// TODO Auto-generated method stub
		if(StringUtils.isNotEmpty(btmc)||fbkssj!=null||fbjssj!=null)
			return noticeBulletinMapper.getCountByCondition(btmc, fbkssj, fbjssj);
		else
			return this.getAllCount();
	}
	
	@Override
	public int getAllCount() {
		// TODO Auto-generated method stub
		return noticeBulletinMapper.getAllCount();
	}

	@Override
	public NoticeBulletin findById(Integer id) {
		// TODO Auto-generated method stub
		return noticeBulletinMapper.selectByPrimaryKey(id);
	}

	@Override
	public void save(NoticeBulletin noticeBulletin) {
		// TODO Auto-generated method stub
		noticeBulletinMapper.insert(noticeBulletin);
	}

	@Override
	public void update(NoticeBulletin noticeBulletin) {
		// TODO Auto-generated method stub
		noticeBulletinMapper.updateByPrimaryKey(noticeBulletin);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		noticeBulletinMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(NoticeBulletin noticeBulletin) {
		// TODO Auto-generated method stub
		noticeBulletinMapper.updateByPrimaryKeySelective(noticeBulletin);
	}

	@Override
	public List<NoticeBulletin> findFbAll() {
		// TODO Auto-generated method stub
		return noticeBulletinMapper.findFbAll();
	}

	@Override
	public NoticeBulletin getNoticeByCondition(String bh, Integer id) {
		// TODO Auto-generated method stub
		return noticeBulletinMapper.getNoticeByCondition(bh, id);
	}

}
