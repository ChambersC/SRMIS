package cn.srmis.notice.po;

import java.util.Date;

/**
 * 获奖喜讯
 * @author Chambers
 * 2018
 */
public class AwardWinningNews {
    private Integer id;

    private String bh;		//编号

    private String btmc;	//标题名称

    private String zynr;	//主要内容

    private String zgr;		//撰稿人

    private String fbrxm;	//发布人姓名

    private Date fbsj;		//发布时间

    private String fbzt;	//发布状态 0-未发布  1-已发布  2-置顶  3-下架

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh == null ? null : bh.trim();
    }

    public String getBtmc() {
        return btmc;
    }

    public void setBtmc(String btmc) {
        this.btmc = btmc == null ? null : btmc.trim();
    }

    public String getZynr() {
        return zynr;
    }

    public void setZynr(String zynr) {
        this.zynr = zynr == null ? null : zynr.trim();
    }

    public String getZgr() {
        return zgr;
    }

    public void setZgr(String zgr) {
        this.zgr = zgr == null ? null : zgr.trim();
    }

    public String getFbrxm() {
        return fbrxm;
    }

    public void setFbrxm(String fbrxm) {
        this.fbrxm = fbrxm == null ? null : fbrxm.trim();
    }

    public Date getFbsj() {
        return fbsj;
    }

    public void setFbsj(Date fbsj) {
        this.fbsj = fbsj;
    }

    public String getFbzt() {
        return fbzt;
    }

    public void setFbzt(String fbzt) {
        this.fbzt = fbzt == null ? null : fbzt.trim();
    }
}