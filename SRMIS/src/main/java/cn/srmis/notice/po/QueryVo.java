package cn.srmis.notice.po;

import java.util.Date;

/**
 * 通知公告/获奖喜讯 查询条件Vo
 * @author Chambers
 * 2018
 */
public class QueryVo {
    
	private String btmc;	//标题名称

    private Date fbkssj;	//发布�?��时间

    private Date fbjssj;	//发布结束时间

	public String getBtmc() {
		return btmc;
	}

	public void setBtmc(String btmc) {
		this.btmc = btmc;
	}

	public Date getFbkssj() {
		return fbkssj;
	}

	public void setFbkssj(Date fbkssj) {
		this.fbkssj = fbkssj;
	}

	public Date getFbjssj() {
		return fbjssj;
	}

	public void setFbjssj(Date fbjssj) {
		this.fbjssj = fbjssj;
	}
	
}