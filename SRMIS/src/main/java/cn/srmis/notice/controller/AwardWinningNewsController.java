package cn.srmis.notice.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.common.service.UpdateLoadService;
import cn.srmis.notice.po.AwardWinningNews;
import cn.srmis.notice.po.QueryVo;
import cn.srmis.notice.service.AwardWinningNewsService;
import cn.srmis.user.po.User;
import cn.srmis.util.DateUtil;

/**
 * Scientific research information
 * @author Chambers
 */

@RestController
@RequestMapping(value = "/notice/")
@Slf4j
public class AwardWinningNewsController extends BaseContorller {

	@Autowired
	private AwardWinningNewsService awardWinningNewsService;
	
	@Autowired
	private UpdateLoadService updateLoadService;
	
/* award-winning news */
	
	@RequestMapping(value = "awnlist")
    public ModelAndView awnlist(AwardWinningNews notice){
		
		ModelAndView modelAndView = new ModelAndView();
		
		List<AwardWinningNews> hjxxList = awardWinningNewsService.findFbAll();	
		
		modelAndView.addObject("list", hjxxList);
		
		modelAndView.setViewName("content/kyzx/hjxx-list");	
    	
		return modelAndView;
    }

	@RequestMapping(value = "awnview")
    public ModelAndView awnview(Integer id){

		ModelAndView modelAndView = new ModelAndView();
		
		AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);	
		
		modelAndView.addObject("awardWinningNews", awardWinningNews);
		
		List<NbxtAttachment> nbxtList = updateLoadService.getList("AwardWinningNews",id);
		modelAndView.addObject("fileList", nbxtList);
		
		modelAndView.setViewName("content/kyzx/hjxx-view");	
		
    	return modelAndView;
    }
	

/* award-winning news release */
	
	@RequestMapping(value = "awnrlist")
    public ModelAndView awnrlist(HttpServletRequest request, QueryVo vo){
		Map<String, Object> model = new HashMap<String, Object>();
		// 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        //总记录数
        int resultSize = awardWinningNewsService.getCountByCondition(vo.getBtmc(), vo.getFbkssj(), vo.getFbjssj());  
        //显示所有  
        List<AwardWinningNews> pageList = awardWinningNewsService.getPageList(vo.getBtmc(), vo.getFbkssj(), vo.getFbjssj(), pageIndex*pageSize, pageSize);  
        model.put("vo", vo);
        model.put("pageList", pageList);
        model.put("resultSize", resultSize);
        model.put("pageSize", pageSize);
		return new ModelAndView("content/kyzx/hjxxfb-list",model);
    }

	@RequestMapping(value = "awnredit")
    public ModelAndView awnredit(Integer id){
		Map<String, Object> model = new HashMap<String, Object>();
		if(id!=null){
			AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
			model.put("awardWinningNews", awardWinningNews);
			model.put("stid", id);
			model.put("stmc", AwardWinningNews.class.getSimpleName());
		}
		return new ModelAndView("content/kyzx/hjxxfb-edit",model);
    }
	
	@RequestMapping(value = "awnrsave")
    public void awnrsave(HttpServletRequest request, AwardWinningNews awardWinningNews, HttpServletResponse response,
   		 String multiFileIds) throws IOException{
		JSONObject result = new JSONObject();
		try{
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			awardWinningNews.setZgr(user.getName());	//添加当前撰稿人姓名
	    	awardWinningNews.setFbzt("0");
	    	if(awardWinningNews.getId()!=null){
				awardWinningNewsService.update(awardWinningNews);
			}
			else{
				awardWinningNewsService.save(awardWinningNews);
			}
	    	updateLoadService.update(multiFileIds, AwardWinningNews.class.getSimpleName(), awardWinningNews.getId());
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	/**
	 * 编号是否存在
	 * @throws IOException 
	 */
	@RequestMapping(value = "bhsfcz2")
    public void bhsfcz2(Integer id, String bh, HttpServletResponse response) throws IOException{
		JSONObject result = new JSONObject();
		try {
			AwardWinningNews awardWinningNews = awardWinningNewsService.getNewsByCondition(bh,id);
			if(awardWinningNews!=null){
				result.put("flag", "1");
				result.put("msg", "编号("+bh+")已存在!");
			}
			else{
				result.put("flag", "0");
				result.put("msg", "编号("+bh+")不存在!");
			}
		} catch (Exception e) {
			String exceptionMsg = new StringBuffer("程序出错(")
					.append(DateUtil
							.formatDate(new Date(), "yyyyMMddHHmmsssss"))
					.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
	}
	
	@RequestMapping(value = "awnrview")
    public ModelAndView awnrview(Integer id){
		Map<String, Object> model = new HashMap<String, Object>();
		
		AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
		model.put("awardWinningNews", awardWinningNews);
		
		model.put("stid", id);
		model.put("stmc", AwardWinningNews.class.getSimpleName());

    	return new ModelAndView("content/kyzx/hjxxfb-view", model);
    }
	
	@RequestMapping(value = "awnrdelete")
    public void awnrdelete(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			awardWinningNewsService.delete(id);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "awnrfb")
    public void awnrfb(HttpServletRequest request, Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
			awardWinningNews.setFbsj(new Date());
			awardWinningNews.setFbzt("1");
			
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			awardWinningNews.setFbrxm(user.getName());	//添加当前发布人姓名
			
	    	awardWinningNewsService.updateByPrimaryKeySelective(awardWinningNews);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "awnrxj")
    public void awnrxj(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
			awardWinningNews.setFbsj(null);
			awardWinningNews.setFbrxm(null);
			awardWinningNews.setFbzt("3");
	    	awardWinningNewsService.update(awardWinningNews);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "awnrzd")
    public void awnrzd(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
			awardWinningNews.setFbzt("2");
	    	awardWinningNewsService.updateByPrimaryKeySelective(awardWinningNews);
	    	result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "awnrqxzd")
    public void awnrqxzd(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			AwardWinningNews awardWinningNews = awardWinningNewsService.findById(id);
			awardWinningNews.setFbzt("1");
	    	awardWinningNewsService.updateByPrimaryKeySelective(awardWinningNews);
			result.put("flag", "1");
			result.put("msg", "操作成功");
			
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
}
