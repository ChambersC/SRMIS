package cn.srmis.notice.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.common.service.UpdateLoadService;
import cn.srmis.notice.po.NoticeBulletin;
import cn.srmis.notice.po.QueryVo;
import cn.srmis.notice.service.NoticeBulletinService;
import cn.srmis.user.po.User;
import cn.srmis.util.DateUtil;

/**
 * Scientific research information
 * @author Chambers
 */

@RestController
@RequestMapping(value = "/notice/")
@Slf4j
public class NoticeBulletinController extends BaseContorller {

	@Autowired
	private NoticeBulletinService noticeBulletinService;
	
	@Autowired
	private UpdateLoadService updateLoadService;
	
/* notice bulletin */
	
	@RequestMapping(value = "nblist")
    public ModelAndView nblist(NoticeBulletin notice){
		
		ModelAndView modelAndView = new ModelAndView();
		
		List<NoticeBulletin> tzggList = noticeBulletinService.findFbAll();	
		
		modelAndView.addObject("list", tzggList);
		
		modelAndView.setViewName("content/kyzx/tzgg-list");	
    	
		return modelAndView;
    }

	@RequestMapping(value = "nbview")
    public ModelAndView nbview(Integer id){
		
		ModelAndView modelAndView = new ModelAndView();
		
		NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);	
		
		modelAndView.addObject("noticeBulletin", noticeBulletin);
		
		List<NbxtAttachment> nbxtList = updateLoadService.getList("NoticeBulletin",id);
		modelAndView.addObject("fileList", nbxtList);
		
		modelAndView.setViewName("content/kyzx/tzgg-view");	
		
    	return modelAndView;
    }
	
	
/* notice bulletin release/publish */
	
	@RequestMapping(value = "nbrlist")
    public ModelAndView nbrlist(HttpServletRequest request, QueryVo vo){
		Map<String, Object> model = new HashMap<String, Object>();
		// 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        //总记录数
        int resultSize = noticeBulletinService.getCountByCondition(vo.getBtmc(), vo.getFbkssj(), vo.getFbjssj());  
        //显示所有  
        List<NoticeBulletin> pageList = noticeBulletinService.getPageList(vo.getBtmc(), vo.getFbkssj(), vo.getFbjssj(), pageIndex*pageSize, pageSize);  
        model.put("vo", vo);
        model.put("pageList", pageList);
        model.put("resultSize", resultSize);
        model.put("pageSize", pageSize);
		return new ModelAndView("content/kyzx/tzggfb-list",model);
    }

	@RequestMapping(value = "nbredit")
    public ModelAndView nbredit(Integer id){
		Map<String, Object> model = new HashMap<String, Object>();
		if(id!=null){
			NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
			model.put("noticeBulletin", noticeBulletin);
			model.put("stid", id);
			model.put("stmc", NoticeBulletin.class.getSimpleName());
		}
		return new ModelAndView("content/kyzx/tzggfb-edit",model);
    }
	
	@RequestMapping(value = "nbrsave")
    public void nbrsave(HttpServletRequest request, NoticeBulletin noticeBulletin, HttpServletResponse response,
    		 String multiFileIds) throws IOException{
		JSONObject result = new JSONObject();
		try{
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			noticeBulletin.setZgr(user.getName());	//添加当前撰稿人姓名
	    	noticeBulletin.setFbzt("0");
	    	if(noticeBulletin.getId()!=null){
				noticeBulletinService.update(noticeBulletin);
			}
			else{
				noticeBulletinService.save(noticeBulletin);
			}
	    	updateLoadService.update(multiFileIds, NoticeBulletin.class.getSimpleName(), noticeBulletin.getId());
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	/**
	 * 编号是否存在
	 * @throws IOException 
	 */
	@RequestMapping(value = "bhsfcz")
    public void bhsfcz(Integer id, String bh, HttpServletResponse response) throws IOException{
		JSONObject result = new JSONObject();
		try {
			NoticeBulletin noticeBulletin = noticeBulletinService.getNoticeByCondition(bh,id);
			if(noticeBulletin!=null){
				result.put("flag", "1");
				result.put("msg", "编号("+bh+")已存在!");
			}
			else{
				result.put("flag", "0");
				result.put("msg", "编号("+bh+")不存在!");
			}
		} catch (Exception e) {
			String exceptionMsg = new StringBuffer("程序出错(")
					.append(DateUtil
							.formatDate(new Date(), "yyyyMMddHHmmsssss"))
					.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
	}
	
	@RequestMapping(value = "nbrview")
    public ModelAndView nbrview(Integer id){
		Map<String, Object> model = new HashMap<String, Object>();
		NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
		model.put("noticeBulletin", noticeBulletin);
		model.put("stid", id);
		model.put("stmc", NoticeBulletin.class.getSimpleName());
		return new ModelAndView("content/kyzx/tzggfb-view", model);
    }
	
	@RequestMapping(value = "nbrdelete")
    public void nbrdelete(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			noticeBulletinService.delete(id);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "nbrfb")
    public void nbrfb(HttpServletRequest request, Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
			noticeBulletin.setFbsj(new Date());
			noticeBulletin.setFbzt("1");
			
			User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
			noticeBulletin.setFbrxm(user.getName());	//添加当前发布人姓名
			
			noticeBulletinService.updateByPrimaryKeySelective(noticeBulletin);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "nbrxj")
    public void nbrxj(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
			noticeBulletin.setFbsj(null);
			noticeBulletin.setFbrxm(null);
			noticeBulletin.setFbzt("3");
	    	noticeBulletinService.update(noticeBulletin);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "nbrzd")
    public void nbrzd(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
			noticeBulletin.setFbzt("2");
	    	noticeBulletinService.updateByPrimaryKeySelective(noticeBulletin);
	    	result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "nbrqxzd")
    public void nbrqxzd(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			NoticeBulletin noticeBulletin = noticeBulletinService.findById(id);
			noticeBulletin.setFbzt("1");
	    	noticeBulletinService.updateByPrimaryKeySelective(noticeBulletin);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
}
