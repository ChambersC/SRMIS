package cn.srmis.common.po;

/**
 * 附件类
 */
public class NbxtAttachment implements java.io.Serializable{

	private static final long serialVersionUID = -4828709733792645362L;
	private Integer id;
	
	private Integer stid;//实体Id
	
	private String stmc;//实体名称(类名)
	
	
	private String fjmc;//附件名称
	
	private String fjUrl;//附件下载地址
	
	
	private String classify;	//分类（单个实体有不同类型的附件，如一个项目 申请、中期检查和结题时不同的时期对应不同类型 附件）
	
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStid() {
		return stid;
	}
	public void setStid(Integer stid) {
		this.stid = stid;
	}
	public String getStmc() {
		return stmc;
	}
	public void setStmc(String stmc) {
		this.stmc = stmc;
	}
	
	public String getFjmc() {
		return fjmc;
	}
	public void setFjmc(String fjmc) {
		this.fjmc = fjmc;
	}
	public String getFjUrl() {
		return fjUrl;
	}
	public void setFjUrl(String fjUrl) {
		this.fjUrl = fjUrl;
	}
	
}
