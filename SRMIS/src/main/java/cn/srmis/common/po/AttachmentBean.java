package cn.srmis.common.po;


/**
 * ����bean
 */
public class AttachmentBean {
	private Integer id;
	private String name;
	private String saveName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSaveName() {
		return saveName;
	}
	public void setSaveName(String saveName) {
		this.saveName = saveName;
	}
	
}
