package cn.srmis.common.dao;

import java.util.List;

import cn.srmis.common.po.NbxtAttachment;

public interface UpdateLoadDao {
	int deleteByPrimaryKey(Integer id);

    int insert(NbxtAttachment record);

    int insertSelective(NbxtAttachment record);

    NbxtAttachment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NbxtAttachment record);

    int updateByPrimaryKey(NbxtAttachment record);

	
    List<NbxtAttachment> getListByStmcid(String stmc, Integer stid);
    
    List<NbxtAttachment> getListByStmcidLX(String stmc, Integer stid, String classify);

}
