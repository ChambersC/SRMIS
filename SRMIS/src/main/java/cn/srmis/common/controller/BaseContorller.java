package cn.srmis.common.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import com.alibaba.fastjson.JSONObject;

/**
 * Controller基础类
 * @author Chambers
 *
 */
@ControllerAdvice
@Controller
public class BaseContorller {

	//页数的参数名  
    protected String pageIndexName = new ParamEncoder("bean").encodeParameterName(TableTagParameters.PARAMETER_PAGE);  
    // 每页显示的条数  
	protected int pageSize = 10; 
	
	public void write(JSONObject object, HttpServletResponse response)throws IOException {
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		try {
			out.write(object.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			out.flush();
			out.close();
		}
	}
	
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public String handleException(MaxUploadSizeExceededException exception) throws Exception {
		exception.getStackTrace();
		return "common/maxUploadSizeException";
	}   
}
