package cn.srmis.common.controller;

import java.io.File;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import cn.srmis.common.po.AttachmentBean;
import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.common.service.UpdateLoadService;
import cn.srmis.util.CommonMultipartFileToFile;
import cn.srmis.util.ResourceManager;
import cn.srmis.util.ResourceVO;

/**
 * Attachment (uploadify plugin)
 *
 */

@RestController
@RequestMapping(value = "/attachment/")
@Slf4j
public class AttachmentController {

	@Resource
	private UpdateLoadService updateLoadService;
	
	@RequestMapping(value = "list")
    public ModelAndView list(HttpServletResponse response, String stid, String stmc){
		List<AttachmentBean> result = new ArrayList<AttachmentBean>();
		//System.out.println("=========================="+stmc+","+stid);
		if(stmc != null && !stmc.equals("") && stid != null && !stid.equals("")){
			List<NbxtAttachment> attList = updateLoadService.getList(stmc,Integer.valueOf(stid));
			if(attList != null){
				for(NbxtAttachment temp: attList){
					AttachmentBean bean= new AttachmentBean();
					bean.setId(temp.getId());
					bean.setName(temp.getFjmc());
					bean.setSaveName(temp.getFjUrl());
					result.add(bean);
				}
				String js = JSONArray.fromObject(result.toArray()).toString();
				updateLoadService.writeText(response, js);
			}else{
				updateLoadService.writeText(response, "");
			}
		}else{
			updateLoadService.writeText(response, "");
		}
		return null;
    }
	
	@RequestMapping(value = "upLoad")
    public ModelAndView upLoad(HttpServletResponse response, @RequestParam("file") CommonsMultipartFile file, NbxtAttachment attach, String stmc, Integer stid){
		if(file != null){//如果附件不为空
			ResourceVO resourceVO = ResourceManager.addResource(CommonMultipartFileToFile.commonsMultipartToFile(file),file.getOriginalFilename());
//			String filePath = ServletActionContext.getServletContext()
//					.getRealPath(resourceVO.getFilepath());
			String filePath = ContextLoader.getCurrentWebApplicationContext()
					.getServletContext().getRealPath(resourceVO.getFilepath());
			if(attach == null){
				attach = new NbxtAttachment();
			}
			attach.setStid(stid);
			attach.setStmc(stmc);
			attach.setFjmc(file.getOriginalFilename());
			attach.setFjUrl(resourceVO.getFilepath());
			attach = updateLoadService.add(attach);
			if(attach != null && attach.getId() != null){
				AttachmentBean attachBean = new AttachmentBean();
				attachBean.setId(attach.getId());
				attachBean.setName(attach.getFjmc());
				attachBean.setSaveName(attach.getFjUrl());
				System.out.println(JSONObject.fromObject(attachBean).toString());
				updateLoadService.writeText(response, JSONObject.fromObject(attachBean).toString());
			}else{
				updateLoadService.writeText(response, "");
			}
		}
		return null;
    }
	
	@RequestMapping(value = "delete")
    public String delete(Integer id){
		if(id != null){
			NbxtAttachment attach = updateLoadService.getOneById(id);
		//file = new File(ServletActionContext.getServletContext().getRealPath(attach.getFjUrl()));
			File file = new File(ContextLoader.getCurrentWebApplicationContext()
					.getServletContext().getRealPath(attach.getFjUrl()));
			ResourceManager.deleteFile(file); 		// 删除文件
			updateLoadService.deleteById(id);// 删除记录
		}
		return null;
    }
	
	@RequestMapping(value = "download")
    public void download(HttpServletResponse response, Integer id){
		try{ 
			if(id != null){
				NbxtAttachment attach = updateLoadService.getOneById(id);
				if(attach != null){
					response.reset();
					response.setContentType("application/octet-stream;");
					response.setHeader("Content-Disposition", "attachment; filename=" + 
									new String(attach.getFjmc().getBytes("gb2312"),"ISO8859-1"));
					//ZipFiles.download(response, attach.getFjUrl());
					ResourceManager.download(attach.getFjUrl(),attach.getFjmc(), response);
				}
			}
		}catch (Exception e) {
		//this.addActionMessage("系统异常!");
			e.printStackTrace();
		}
    }

}
