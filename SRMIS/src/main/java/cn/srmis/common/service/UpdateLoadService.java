package cn.srmis.common.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.srmis.common.po.NbxtAttachment;



public interface UpdateLoadService {
	/**
	 * 文件上传方法等方法----------------------------------------------------------------------------
	 * 
	 * */	
	/**
	 * @param attach  添加相关附件信息
	 * @return
	 */
	NbxtAttachment add(NbxtAttachment attach);
	/**
	 * @param id   删除附件通过id
	 */
	void deleteById(Integer id);
	/**
	 * @param stmc
	 * @param stid   通过实体名称和实体id将附件列表查出来
	 * @return
	 */
	List<NbxtAttachment> getList(String stmc, Integer stid);
	/**
	 * @param id
	 * @return
	 */
	NbxtAttachment getOneById(Integer id);
	/**
	 * @param multiFileIds
	 * @param stmc
	 * @param stid
	 * 保存该实体名称的该实体Id的附件
	 */
	void update(String multiFileIds, String stmc, Integer stid);
	/**
	 * @param response 
	 * @param content
	 */
	void writeText(HttpServletResponse response, String content);
	
	
	
//	public  PageList<?> getPageList(Class<?> classType,Pagination p,String ljFetch,String condition,List params);
	public Object getBeanById(Class<?> classType, Integer id, String ljFetch);
	public Object addAndReturn(Object obj);
	
	
	/**
	 * 附件保存(随form表单一起提交)
	 * @author Chambers
	 * @param response
	 * @param file
	 * @param stmc
	 * @param stid
	 */
	void upLoad(HttpServletResponse response, CommonsMultipartFile file, String stmc, Integer stid);
	
	/**
	 * 通过实体名称（即类名）和实体id 将其所属的附件全删除
	 * @author ChenBochao
	 * @param stmc
	 * @param stid
	 */
	void deleteByStidmc(String stmc, Integer stid);

}
