package cn.srmis.common.service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.srmis.common.dao.UpdateLoadDao;
import cn.srmis.common.po.AttachmentBean;
import cn.srmis.common.po.NbxtAttachment;
import cn.srmis.util.CommonMultipartFileToFile;
import cn.srmis.util.ResourceManager;
import cn.srmis.util.ResourceVO;

@Service
@Transactional
public class UpdateLoadServiceImpl implements UpdateLoadService, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UpdateLoadDao updateLoadDao;
	
	/**
	 * 文件上传方法等方法----------------------------------------------------------------------------
	 * 
	 * */
		@Override
		public NbxtAttachment add(NbxtAttachment attach) {
			
			updateLoadDao.insert(attach);
			
			return updateLoadDao.selectByPrimaryKey(attach.getId());
		}


		@Override
		public void deleteById(Integer id) {
			updateLoadDao.deleteByPrimaryKey(id);
		}


		@Override
		public List<NbxtAttachment> getList(String stmc, Integer stid) {
			
			return updateLoadDao.getListByStmcid(stmc, stid);
		}


		@Override
		public NbxtAttachment getOneById(Integer id) {
			
			return updateLoadDao.selectByPrimaryKey(id);
		}


		@Override
		public void update(String multiFileIds, String stmc, Integer stid) {
			// TODO Auto-generated method stub
			if(multiFileIds != null && !multiFileIds.equals("")){
				String ids[] = multiFileIds.split(",");
				for(int i=0;i<ids.length;i++){
					if(ids[i].equals("")){
						continue;
					}
					NbxtAttachment po = this.getOneById(Integer.valueOf(ids[i]));
					po.setStid(stid);
					po.setStmc(stmc);
					updateLoadDao.updateByPrimaryKeySelective(po);
				}
			}
			
		}


		@Override
		public void writeText(HttpServletResponse response, String content) {
			
			response.setContentType("text/plain;charset=utf-8");
			PrintWriter out;
			try {
				out = response.getWriter();
				out.print(content);
				out.flush();
				out.close();
			} catch (IOException e) {
				System.out.println("JSON返回值异常");
				e.printStackTrace();
			}
			
		}


		
		
		
		
		/* (non-Javadoc)
		 * @see com.jx.sso.service.webservice.UpdateLoadService#addAndReturn(java.lang.Object)
		 */
		@Override
		public Object addAndReturn(Object obj) {
			// TODO Auto-generated method stub
			return null;//commonToolDao.addAndReturn( obj);
		}


		/* (non-Javadoc)
		 * @see com.jx.sso.service.webservice.UpdateLoadService#getBeanById(java.lang.Class, java.lang.Integer, java.lang.String)
		 */
		@Override
		public Object getBeanById(Class<?> classType, Integer id, String ljFetch) {
			// TODO Auto-generated method stub
			return null;//commonToolDao.getBeanById(classType, id, ljFetch);
		}

		/**
		 * 附件保存(随form表单一起提交)
		 * @author Chambers
		 * @param response
		 * @param file
		 * @param stmc
		 * @param stid
		 */
		@Override
		public void upLoad(HttpServletResponse response,
				CommonsMultipartFile file, String stmc, Integer stid) {
			if(file != null && file.getSize()!=0){//如果附件不为空
				ResourceVO resourceVO = ResourceManager.addResource(CommonMultipartFileToFile.commonsMultipartToFile(file),file.getOriginalFilename());
				
				NbxtAttachment attach = new NbxtAttachment();
				attach.setStid(stid);
				attach.setStmc(stmc);
				attach.setFjmc(file.getOriginalFilename());
				attach.setFjUrl(resourceVO.getFilepath());
				attach = this.add(attach);
				if(attach != null && attach.getId() != null){
					AttachmentBean attachBean = new AttachmentBean();
					attachBean.setId(attach.getId());
					attachBean.setName(attach.getFjmc());
					attachBean.setSaveName(attach.getFjUrl());
//					System.out.println(JSONObject.fromObject(attachBean).toString());
//					this.writeText(response, JSONObject.fromObject(attachBean).toString());
//				}else{
//					this.writeText(response, "");
				}
			}
		}

		/**
		 * 通过实体名称（即类名）和实体id 将其所属的附件全删除
		 * @author ChenBochao
		 * @param stmc
		 * @param stid
		 */
		@Override
		public void deleteByStidmc(String stmc, Integer stid) {
			List<NbxtAttachment> attachList = this.getList(stmc, stid);
			if(attachList!=null&&attachList.size()>0){
				for(NbxtAttachment attach:attachList){
					File file = new File(ContextLoader.getCurrentWebApplicationContext()
							.getServletContext().getRealPath(attach.getFjUrl()));
					ResourceManager.deleteFile(file); 		// 删除文件
					this.deleteById(attach.getId());// 删除记录
				}
			}
			
		}


		/* (non-Javadoc)
		 * @see com.jx.sso.service.webservice.UpdateLoadService#getPageList(java.lang.Class, com.jtframework.websupport.pagination.Pagination, java.lang.String, java.lang.String, java.util.List)
		 */
//		@SuppressWarnings("unchecked")
//		@Override
//		public PageList<?> getPageList(Class<?> classType, Pagination p,
//				String ljFetch, String condition, List params) {
//			// TODO Auto-generated method stub
//			return commonToolDao.getPageList(classType, p, "", "", params);
//		}


}
