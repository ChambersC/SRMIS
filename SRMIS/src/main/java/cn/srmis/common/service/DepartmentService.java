package cn.srmis.common.service;

import java.util.List;

import cn.srmis.common.po.Department;

/**
 * ����ҵ����
 * @author Chambers
 * 
 */
public interface DepartmentService {

	List<Department> findAll();

	Department findById(Integer dep_id);
}
