package cn.srmis.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.common.dao.DepartmentMapper;
import cn.srmis.common.po.Department;

/**
 * 科室业务类实现类
 * @author Chambers
 * 
 */

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentMapper departmentMapper;

	@Override
	public List<Department> findAll() {
		// TODO Auto-generated method stub
		return departmentMapper.findAll();
	}

	@Override
	public Department findById(Integer dep_id) {
		// TODO Auto-generated method stub
		return departmentMapper.selectByPrimaryKey(dep_id);
	}
}
