package cn.srmis.user.service;

import java.util.List;

import cn.srmis.user.po.User;
import cn.srmis.user.po.UserVo;


/**
 * 用户业务类
 * @author Chambers
 * 
 */
public interface UserService {

	User loginValidate(String zh, String password);
	
	User findById(Integer id);

	/**
	 * 获取所有本科室的专家，除去项目发起人
	 * @author Chambers
	 * @param dep_id
	 * @param user_id
	 * @return
	 */
	List<User> findExpert(Integer dep_id, Integer user_id);

	int getCountByCondition(UserVo vo);

	List<User> getPageList(UserVo vo);

	void update(User user);

	void save(User user);

	void delete(Integer id);

}
