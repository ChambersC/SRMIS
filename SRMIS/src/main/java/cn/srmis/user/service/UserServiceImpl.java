package cn.srmis.user.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.srmis.user.dao.UserMapper;
import cn.srmis.user.po.User;
import cn.srmis.user.po.UserVo;

/**
 * 用户业务类实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

	@Override
	public User loginValidate(String account, String password) {
		return userMapper.findByZhMm(account, password);
	}

	@Override
	public User findById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<User> findExpert(Integer dep_id, Integer user_id) {
		// TODO Auto-generated method stub
		return userMapper.findExpert(dep_id, user_id);
	}

	@Override
	public int getCountByCondition(UserVo vo) {
		// TODO Auto-generated method stub
		return userMapper.getCountByCondition(vo);
	}

	@Override
	public List<User> getPageList(UserVo vo) {
		// TODO Auto-generated method stub
		return userMapper.getPageList(vo);
	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		userMapper.updateByPrimaryKey(user);
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		userMapper.insert(user);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		userMapper.deleteByPrimaryKey(id);
	}

}
