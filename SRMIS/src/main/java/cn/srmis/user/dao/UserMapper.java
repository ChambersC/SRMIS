package cn.srmis.user.dao;

import java.util.List;

import cn.srmis.user.po.User;
import cn.srmis.user.po.UserVo;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);


    User findByZhMm(String account, String password);

	List<User> findExpert(Integer dep_id, Integer user_id);

	int getCountByCondition(UserVo vo);

	List<User> getPageList(UserVo vo);

}