package cn.srmis.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.srmis.notice.po.AwardWinningNews;
import cn.srmis.notice.po.NoticeBulletin;
import cn.srmis.notice.service.AwardWinningNewsService;
import cn.srmis.notice.service.NoticeBulletinService;
import cn.srmis.user.po.User;
import cn.srmis.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * Sign in、Return password、Main interface
 * @author Chambers
 *
 */

@Controller
@Slf4j
@RequestMapping(value = "/")
public class HomePageController {
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private NoticeBulletinService noticeBulletinService;
	
	@Autowired
	private AwardWinningNewsService awardWinningNewsService;
	
    @RequestMapping(value = "index")	//跳转到登录页面
    public ModelAndView index(){
    	return new ModelAndView("content/login");
    }
    
    @RequestMapping(value = "home")	//登录验证	进入主页面or错误提示
    public String home(HttpServletRequest request, Model model, User user){
    	User u = userservice.loginValidate(user.getAccount(), user.getPassword());
    	if(u!=null){
    		request.getSession().setAttribute("UserInformation", u);	//保存当前登录用户信息
    		return "content/home";
    	}
    	else{
    		model.addAttribute("user", user);
    		model.addAttribute("errorMessage", "账号或密码错误！");
    		return "content/login";
    	}
    }
    
    @RequestMapping(value = "password_return")	//跳转到寻回密码页面
    public ModelAndView password_return(){
    	return new ModelAndView("content/login-password_return");
    }
    
    @RequestMapping(value = "password_reset")	//重置密码页面
    public ModelAndView password_reset(){
    	
    	//安全密码或邮箱验证...
    	
    	return new ModelAndView("content/login-password_reset");
    }
    
    @RequestMapping(value = "reset")	//密码重置结果	成功后跳转至登录页面
    public ModelAndView reset(){
    	
    	//重置密码两次输入是否一样...
    	
    	return new ModelAndView("content/login");
    }

	
    /*	布局	*/
    @RequestMapping(value = "head")
    public String head(HttpServletRequest request, Model model){
    	User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
    	model.addAttribute("loginName", user.getName());
    	return "content/head";
    }

    @RequestMapping(value = "homeMenuPage")
    public String homeMenuPage(HttpServletRequest request, Model model){
    	User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
    	model.addAttribute("type", user.getType());
    	return "content/left";
    }
    
    @RequestMapping(value = "homeMainPage")
    public ModelAndView homeMainPage(){
    	Map<String, Object> model = new HashMap<String, Object>();
    	List<NoticeBulletin> noticeList = noticeBulletinService.findFbAll();
    	model.put("noticeList", noticeList);
    	List<AwardWinningNews> newsList = awardWinningNewsService.findFbAll();
    	model.put("newsList", newsList);
    	return new ModelAndView("content/homeMainPage", model);
    }

    @RequestMapping(value = "homeFooterPage")
    public ModelAndView homeFooterPage(){
    	return new ModelAndView("content/footer");
    }
      
}
