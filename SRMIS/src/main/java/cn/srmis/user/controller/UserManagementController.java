package cn.srmis.user.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.srmis.common.controller.BaseContorller;
import cn.srmis.common.po.Department;
import cn.srmis.common.service.DepartmentService;
import cn.srmis.user.po.User;
import cn.srmis.user.po.UserVo;
import cn.srmis.user.service.UserService;
import cn.srmis.util.DateUtil;

/**
 * Information Maintenance - User Management
 * @author Chambers
 *
 */

@RestController
@RequestMapping(value = "/maintenance")
@Slf4j
public class UserManagementController extends BaseContorller {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping(value = "information")	//用户信息页面
    public ModelAndView information(HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		User u = userService.findById(user.getId());
		mv.addObject("user", u);
		mv.setViewName("content/maintenance/information-list");
		return mv;
    }

	@RequestMapping(value = "update")		//用户信息修改保存
    public void update(HttpServletResponse response, Integer id, String name, 
    		String email, String telephone, String password) throws IOException{
		JSONObject result = new JSONObject();
		try{
			User user = userService.findById(id);
			if(name!=null||email!=null||telephone!=null){
				user.setName(name);
				user.setEmail(email);
				user.setTelephone(telephone);
			}else{
				user.setPassword(password);
			}
			userService.update(user);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "modify")
    public ModelAndView modify(HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		User user = (User) request.getSession().getAttribute("UserInformation");	//获取当前登录用户信息
		User u = userService.findById(user.getId());
		mv.addObject("user", u);
		mv.setViewName("content/maintenance/modify-list");
		return mv;
    }
	
	@RequestMapping(value = "manage")
    public ModelAndView manage(HttpServletRequest request, UserVo vo) {
		ModelAndView modelAndView = new ModelAndView();
		List<User> userList = new ArrayList<User>();
		// 当前页-1
        int pageIndex = StringUtils.isEmpty(request.getParameter(pageIndexName)) ? 0 : (Integer.parseInt(request.getParameter(pageIndexName)) - 1);
        vo.setIndex(pageIndex*pageSize);
        vo.setNumber(pageSize);
        //总记录数
        int resultSize = userService.getCountByCondition(vo);  
        //显示所有  
        List<User> pageList = userService.getPageList(vo); 
        modelAndView.addObject("userList", userList);
		modelAndView.addObject("vo", vo);
        modelAndView.addObject("pageList", pageList);
        modelAndView.addObject("resultSize", resultSize);
        modelAndView.addObject("pageSize", pageSize);
        List<Department> depList = departmentService.findAll();
        modelAndView.addObject("depList", depList);
        modelAndView.setViewName("content/maintenance/manage-list");
		return modelAndView;
	}
	
	@RequestMapping(value = "edit")
    public ModelAndView edit(Integer id, HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		if(id!=null){
			User user = userService.findById(id);
			mv.addObject("user", user);
		}
		List<Department> depList = departmentService.findAll();
		mv.addObject("depList", depList);
		mv.setViewName("content/maintenance/manage-edit");
    	return mv;
    }
	
	/**
	 * 验证账号是否存在
	 * @throws IOException 
	 */
	@RequestMapping(value = "accountValidation")
    public void accountValidation(String account, HttpServletResponse response) throws IOException{
		JSONObject result = new JSONObject();
		String isNotUsed = "Y";
		try {
			User user = userService.loginValidate(account, null);
			if(user!=null)
				isNotUsed = "N";
			result.put("isNotUsed", isNotUsed);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.write(result, response);
		}
	}
	
	/**
	 * 用户类型带出科室
	 * @throws Exception 
	 */
	@RequestMapping(value = "dcks")
	public void dcks(String type, HttpServletResponse response) throws Exception{
		JSONObject result=new JSONObject();
		try {
			if(type.equals("0")||type.equals("1")){
				List<Department> depList = departmentService.findAll();
				List<Object[]> depObjList = new ArrayList<Object[]>();
				if(depList!=null&&depList.size()>0){
					for(Department dep:depList){
						Object[] depObj=new Object[2];
						depObj[0]=dep.getId();
						depObj[1]=dep.getName();
						depObjList.add(depObj);
					}
				}
				result.put("depObjList", depObjList);
			}
		} catch (Exception e) {
			String exceptionMsg = new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(),
			"yyyy-MM-dd HH:mm:sssss")).append("),请联系技术支持!")
			.toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		}finally{
			this.write(result, response);
		}
	}
	
	@RequestMapping(value = "save")
    public void save(HttpServletResponse response, User user) throws IOException{
		JSONObject result = new JSONObject();
		try{
			if(user.getDep_id()!=null){
				Department dep = departmentService.findById(user.getDep_id());
				user.setDname(dep.getName());
			}
			if(user.getId()!=null){
				userService.update(user);
				result.put("msg", "操作成功！");
			}
			else{
				user.setPassword("123456");
				userService.save(user);
				result.put("msg", "操作成功,初始密码：123456");
			}
			result.put("flag", "1");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "reset")
    public void reset(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			User user = userService.findById(id);
			user.setPassword("123456");
			userService.update(user);
			result.put("flag", "1");
			result.put("msg", "操作成功,密码：123456");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
	@RequestMapping(value = "delete")
    public void delete(Integer id, HttpServletResponse response) throws IOException{
		JSONObject result=new JSONObject();
		try {
			userService.delete(id);
			result.put("flag", "1");
			result.put("msg", "操作成功");
		} catch (Exception e) {
			String exceptionMsg=new StringBuffer("程序出错(")
			.append(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:sssss"))
			.append("),请联系技术支持!").toString();
			System.out.println(exceptionMsg);
			e.printStackTrace();
			result.put("flag", "0");
			result.put("msg", exceptionMsg);
		} finally {
			this.write(result, response);
		}
    }
	
}
