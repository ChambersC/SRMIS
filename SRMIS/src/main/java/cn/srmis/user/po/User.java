package cn.srmis.user.po;

/**
 * 用户
 * @author Chambers
 * 2018
 */
public class User {
    private Integer id;

    private String name;

    private String account;

    private String password;

    private String securitypassword;

    private String email;

    private String telephone;

    private String type;	//用户类型 0-普通教师 1-专家评审 2-科研管理人员 3-系统管理员

    private Integer dep_id;	//科室id

    private String dname;	//对应科室名

    private String duty;	//职务

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSecuritypassword() {
        return securitypassword;
    }

    public void setSecuritypassword(String securitypassword) {
        this.securitypassword = securitypassword == null ? null : securitypassword.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getDep_id() {
		return dep_id;
	}

	public void setDep_id(Integer dep_id) {
		this.dep_id = dep_id;
	}

	public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname == null ? null : dname.trim();
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty == null ? null : duty.trim();
    }
}