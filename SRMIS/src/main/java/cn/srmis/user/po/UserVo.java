package cn.srmis.user.po;

/**
 * 用户 查询条件Vo
 * @author Chambers
 * 2018
 */
public class UserVo {

	private Integer index;
	
	private Integer number;

    private String name;

    private String type;	//用户类型 0-普通教师 1-专家评审 2-科研管理人员 3-系统管理员

    private Integer depId;	//科室id

    public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

	public Integer getDepId() {
		return depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}
}