var operateJs = {};

/**
 * 发布一条记录
 * cfg.id			发布id
 * cfg.url			发布地址
 * cfg.callback		发布后回调
 * **/
operateJs.fb = function(cfg){
	var loading=null;
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: {id:cfg.id},
		async: true,
		beforeSend: function(){
			loading=layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				layer.close(loading);
		}
	});
}


/**
 * 下架一条记录
 * cfg.id			下架id
 * cfg.url			下架地址
 * cfg.callback		下架后回调
 * **/
operateJs.xj = function(cfg){
	var loading=null;
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: {id:cfg.id},
		async: true,
		beforeSend: function(){
			loading=layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				layer.close(loading);
		}
	});
}


/**
 * 置顶一条记录
 * cfg.id			置顶id
 * cfg.url			置顶地址
 * cfg.callback		置顶后回调
 * **/
operateJs.zd = function(cfg){
	var loading=null;
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: {id:cfg.id},
		async: true,
		beforeSend: function(){
			loading=layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				layer.close(loading);
		}
	});
}


/**
 * 取消置顶一条记录
 * cfg.id			取消置顶id
 * cfg.url			取消置顶地址
 * cfg.callback		取消置顶后回调
 * **/
operateJs.qxzd = function(cfg){
	var loading=null;
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: {id:cfg.id},
		async: true,
		beforeSend: function(){
			loading=layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				layer.close(loading);
		}
	});
}



/**
 * 保存表单
 * cfg				表单id
 * cfg.url			表单提交地址
 * cfg.callback		保存后回调
 * **/
operateJs.saveForm = function(cfg){
	var loading=null;
	var param = $(cfg.id).serialize();
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: param,
		async: true,
		beforeSend: function(){
			loading=layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				layer.close(loading);
		}
	});
}


/**
 * 删除一条记录
 * cfg.id			记录id
 * cfg.url			删除地址
 * cfg.callback		删除后回调
 * **/
operateJs.resetPwd = function(cfg){
	if(confirm("确定重置密码？")){
		var loading=null;
		$.ajax({
			type: "POST",
			url: cfg.url, 
			dataType: "json",
			data: {id:cfg.id},
			async: true,
			beforeSend: function(){
				loading=layer.load(0, {shade: [0.5,'#fff']});
			},
			success:function(result){
				var flag=result.flag;
				var msg=result.msg;
				if(flag=="1"){
					layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
				}
				else{
					layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
				}
			},
			error:function(){
				layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			},
			complete:function(){
				if(loading!=null)
					layer.close(loading);
			}
		});
	}
}

