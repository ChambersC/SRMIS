var commonJs = {};
//检查数字录入type=1，默认为空，type=2:默认为1,type=3,支持输入0
commonJs.checkDigital = function(objInput,type){
   var value= $(objInput).val();
   if(value==null || value=='' || value==0){
	   if(type=='1'){
	   		$(objInput).val("");
	   }else if(type=='2'){
	   		$(objInput).val("1");
	   }
	   
   }else{
	   	if(type=='1' || type=='3'){
   			$(objInput).val(value.replace(/[^\d]/g,''));
		}else if(type=='2'){
			$(objInput).val(value.replace(/[^\d]/g,'1'));
		}
   }
}


/**
*全选表单中的复选框
*form 表单对象
*/
commonJs.checkAll = function(form){
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.name != 'chkall'&&e.name!='chkall1'&&form.elements[i].type.toLowerCase()=='checkbox'&&form.elements[i].style.display!='none')
       //e.checked = form.chkall.checked;
	   if(e.disabled==false){   
       		e.checked = document.getElementById("chkall").checked;
	   }
	   //if(document.getElementById("chkall1")!=null)
	      //document.getElementById("chkall1").checked=document.getElementById("chkall").checked;
    }
  }
  
/**
*全选表单中的复选框
*form 表单对象
*clickCheck 用于点击全选的复选框
*/
commonJs.checkAllOut = function(form,clickCheck){
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.name != 'chkall'&&e.name!='chkall1'&&form.elements[i].type.toLowerCase()=='checkbox'&&form.elements[i].style.display!='none')
       e.checked = clickCheck.checked;
    }
  }  

  
//清除表单内的所有checkBox
commonJs.unCheckAll = function(form){
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
       e.checked = false;
    }
  }

  //使用正则表达式匹配
commonJs.getCheckBox = function(theform,boxName){
  	var i=0;
	var checkValue="";
	var reg = new RegExp(boxName);

    for(i=0;i<theform.length;i++){
	  if(reg.exec(theform.elements[i].name) && theform.elements[i].checked==true){
	    if(checkValue==""){
		   if(theform.elements[i].value!="") checkValue=theform.elements[i].value;
		}else{
		   checkValue=checkValue+","+theform.elements[i].value;
		}
      }
	}
	return checkValue;
  }
  
  // 校验身份证
commonJs.validateIdCard = function(sIdCard) {
	if (sIdCard == null || sIdCard.length != 15 && sIdCard.length != 18) {
		return false;
	}
	if (sIdCard.length == 15) {
		var exp = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])(\d{3}|\d{2}[xX])$/;
		return exp.test(sIdCard);
	} else {
		var exp = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])(\d{4}|\d{3}[xX])$/;
		return exp.test(sIdCard);
	}
}

//校验手机格式
commonJs.validateMobilePhone = function(sMobileNumber) {
	var exp =/(^[1][3][0-9]{9}$)|(^[1][4][0-9]{9}$)|(^[1][5][^4]{1}\d{8}$)|(^[1][8][^4]{1}\d{8}$)/;
	//var exp =/"^(([1][3][0-9]{1})|([1][5][^4]{1})|([1][8][^4]{1}))\d{8}$"/; 
  	return exp.test(sMobileNumber);  // 手机验证 13x 14x 15x 18x 以此类推
}

commonJs.toJson = function(sJsonString) {
	if (sJsonString != null && sJsonString.indexOf('{') > -1) {
		return eval('(' + sJsonString + ')');
	} else if (sJsonString != null && sJsonString !='' ) {
		return eval('({' + sJsonString + '})');
	}
	return sJsonString;
};



//公用提示方法
var layerIconStyle = 'layer-ext-moon';


commonJs.openConfirm = function(sMessage, funDo,location) {
	var parameter = "btn: ['确定','取消'],shade: false,icon: 3,skin: layerIconStyle";
	if(location!=""){
		if(location=="top"){
			parameter += ",offset: 3";
		}else{
			parameter += ",offset: "+location;
		}
	}
	
	layer.confirm(sMessage,commonJs.toJson(parameter),function(index){
		//alert(index);
		funDo();
		// 提交表单的代码，需要你自己写，然后用 layer.close 关闭就可以了，取消可以省略
		layer.close(index);
	}, function(index){
		layer.closeAll('loading');//关闭进度条
		// 提交表单的代码，需要你自己写，然后用 layer.close 关闭就可以了，取消可以省略
		layer.close(index);
	});
}


/*执行方法
isWindowDo:是否弹出窗口里执行，值true/flase;默认true
*/
commonJs.openAjax = function(sUrl, vParams, funSuccess,isWindowDo){
	var funDo = function(){};
	//if(isWindowDo){
		//funDo = function(){};//commonJs.closeWindow();
	//}
	$.ajax({
		url:		sUrl,
		data:		vParams,
		type:		'POST',
		dataType:	'json',// xml,html,script,json,jsonp
		timeout:	30 * 60 * 1000,
		success:	funSuccess,
		error:		function(){   
			commonJs.closeLoadTip();
			layer.alert('程序出错，请稍后再试！',{icon: 2,skin: layerIconStyle,closeBtn: 0});
		}   
	});
}


/*
sMessage=信息
funDo
location=位置
*/
commonJs.openSuccessTip = function(sMessage,funDo,location){
	var parameter = "icon: 1,skin: layerIconStyle";
	layer.alert(sMessage,commonJs.toJson(parameter),funDo);
}

commonJs.openErrorTip = function(sMessage, funDo,location) {

	var parameter = "icon: 2,skin: layerIconStyle";
	if(location!=""){
		if(location=="top"){
			parameter += ",offset: 3";
		}else{
			parameter += ",offset:"+location;
		}
	}
	
	layer.alert(sMessage,commonJs.toJson(parameter),funDo);
}


commonJs.openErrorTip = function(sMessage) {
	layer.alert(sMessage,{icon: 2,skin: layerIconStyle});
}


commonJs.closeLoadTip = function() {
	layer.closeAll('loading');//关闭进度条
}

commonJs.openLoadTip = function(){
	layer.load(0, {shade: [0.1,'#fff']},true);//打开进度条0.1透明度的白色背景
}

/*通用保存操作方法,有特殊需求，可参照调整
url:执行地址
formId:提交form名称，不设置默认是myform;
isWindowDo:是否弹出窗口里执行，值true/flase;默认true
location:top/center提示显示位置
*/
commonJs.doSaveOrUpdate = function(obj){
	var url = obj.url;
	var formId = ""
	if(obj.formId==undefined){
		formId = "myform";
	}
	if(obj.isWindowDo==undefined){
		obj.isWindowDo = true;
	}
	/*if(obj.location==undefined){
		obj.location = "center";
	}*/
	commonJs.openLoadTip();
	var param = $("#"+formId).serialize();
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){if(obj.isWindowDo==true){parent.location.reload();}else{window.location.reload();};commonJs.closeWindow();},obj.location);
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow()},obj.location);;
		}
	};

	commonJs.openConfirm("是否确定保存数据？",function(){commonJs.openAjax(url,param,funSuccess,obj.isWindowDo);});
}

/*通用打开编辑窗口方法,有特殊需求，可参照调整
url:访问地址
maximize:是否最大化显示，默认false，true/false
windowType:窗口模式:false(默认，不显示标题和最大最小框);true:显示最大最新化
*/

commonJs.openWindow = function(obj){
		var url = obj.url;
		if(obj.id!=undefined && obj.id!=null){
			url += "?id="+obj.id;
		}
		var title = "";
		if(obj.title!=undefined){
			title = obj.title;
		}
		var width = 700;
		var height = 500;
		if(obj.width!=undefined){
			width = obj.width;
		}
		if(obj.height!=undefined){
			height = obj.height;
		}
		
		var windowType = false;//默认非弹窗口形式
		if(obj.windowType!=undefined){
			windowType = obj.windowType;
		}
	
	//if(windowType==true){//窗口形式显示
		var index =layer.open({
			  type: 2,
			  title: title,
			  shadeClose: false,
			  shade: [0.5, '#000'],//开启遮罩层，设置透明底
			  maxmin: true, //开启最大化最小化按钮
			  area: [width+'px', height+'px'],
			  offset: 5,
			  content: url
		});
		if(obj.maximize==true){
			layer.full(index);
		}
	/*}else{
		var index =layer.open({
			  type: 2,
			  title: false,
			  shadeClose: false,//控制点击弹层外区域不关闭
			  closeBtn: false,//不显示
			  shade: [0.5, '#000'],//开启遮罩层，设置透明底
			  maxmin: false, //开启最大化最小化按钮
			  //area: [width+'px', height+'px'],
			  area:	'auto',
			  offset: 5,
			  content: url
		});
		layer.full(index);
	}*/
	

}
//关闭窗口
commonJs.closeWindow = function(){
	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭   
}



/*通用删除操作方法,有特殊需求，可参照调整
url:访问地址
data:参数列表:例子：{'ywkGgxx.id': id};
isWindowDo:是否弹出窗口里执行，值true/flase;默认flase
*/
commonJs.doDelete = function(obj){
	var url = obj.url;
	
	if(obj.isWindowDo==undefined){
		obj.isWindowDo = false;
	}
	var batchDelete = false;//批量删除
	if(obj.batchDelete!=undefined){
		batchDelete = obj.batchDelete 
	}

	commonJs.openLoadTip();
	var param = obj.data;
	if(batchDelete==true){//批量删除
		if(obj.formId==undefined){
			formId = "myform";
		}
		param = $("#"+formId).serialize();
	}
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){window.location.reload();commonJs.closeWindow();});
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow();});
		}
	};
	commonJs.openConfirm("是否确定删除数据？",function(){commonJs.openAjax(url,param,funSuccess,obj.isWindowDo);});
}
//发布
commonJs.doPublish = function(obj){
	var url = obj.url;
	if(obj.isWindowDo==undefined){
		obj.isWindowDo = false;
	}
	var param = obj.data;
	commonJs.openLoadTip();
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){window.location.reload();commonJs.closeWindow();});
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow();});
		}
	};
	commonJs.openConfirm("是否确定发布？",function(){commonJs.openAjax(url,param,funSuccess,obj.isWindowDo);});
}

//取消发布
commonJs.doCancelPublish = function(obj){
	var url = obj.url;
	if(obj.isWindowDo==undefined){
		obj.isWindowDo = false;
	}
	var param = obj.data;
	commonJs.openLoadTip();
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){window.location.reload();commonJs.closeWindow();});
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow();});
		}
	};
	commonJs.openConfirm("是否确定下架？",function(){commonJs.openAjax(url,param,funSuccess,obj.isWindowDo);});
}

//重新提交
commonJs.doRollBackSubmit = function(obj){
	var url = obj.url;
	if(obj.isWindowDo==undefined){
		obj.isWindowDo = false;
	}
	var param = obj.data;
	commonJs.openLoadTip();
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){window.location.reload();commonJs.closeWindow();});
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow();});
		}
	};
	commonJs.openConfirm("是否确定提交审批？",function(){commonJs.openAjax(url,param,funSuccess,obj.isWindowDo);});
}

/*执行指定操作，自定义操作
url:执行地址
isWindowDo:是否在弹出窗口执行,默认flase否
hint:提示信息
formId:提交form,可不传
location:弹出提示显示位置
isShowConfirm:是否提示确认信息
*/
commonJs.doOperate = function(obj){
	var url = obj.url; //执行地址
	var formId = "";
	var isWindowDo = false;//是否在窗口执行,默认flase
	var location = "";//弹出提示窗口位置
	if(obj.isWindowDo!=undefined){
		isWindowDo = obj.isWindowDo;
	}
	if(obj.formId!=undefined){
		formId = obj.formId;
	}
	if(obj.location!=undefined){
		location = obj.location;
	}

	var param = obj.data;
	if(formId!=""){
		param = $("#"+formId).serialize();
	}
	commonJs.openLoadTip();
	var funSuccess = function(data) {
		commonJs.closeLoadTip();
		if(data.flag=="1"){//操作成功
			commonJs.openSuccessTip(data.msg,function(){
			if(isWindowDo==true){													  
				parent.location.reload();
			}else{
				window.location.reload();
			}
			commonJs.closeWindow();},location);
		}else{//操作失败
			commonJs.openErrorTip(data.msg,function(){commonJs.closeWindow();},location);
		}
	};
	commonJs.openConfirm(obj.hint,function(){commonJs.openAjax(url,param,funSuccess,isWindowDo);},location);
}

//提交审批方法
commonJs.doSubmitApprove = function(){
	if(checkApproveSubmit()==true){
		var rollBackText = $("#rollBackAim").find("option:selected").text();
		if(rollBackText=="申请人"){
			$("#isRollBackApplicant").val("true");
		}
		var url = $('#approveForm').attr('action');
		commonJs.doOperate({hint:"是否确定提交审批结果!",url:url,formId:"approveForm",isWindowDo:true});
	}
}

function checkApproveSubmit() {
	var flag = false;
	var allRadio = $("input[type='radio']","#approveForm");var checkedNum = 0 ;
	for(var i = 0;i < allRadio.length;i++){ 
		if(allRadio[i].checked){ 
			checkedNum = checkedNum + 1 ; 
			if(checkedNum > 1){ 
				commonJs.openErrorTip('一次只能操作一个节点！',null,200); 
				return false; 
			}  
			flag = true;   
		}
	}
	
	if (!flag){
		commonJs.openErrorTip('请选择审批结果',null,200); 
		return false;
	} 
			
	return flag;
}

/*
Ckeditor初始化
editorId:编辑器id
saveFolder:上传图片保存二级目录


*/
commonJs.initCkeditor  = function(editorId,saveFolder){
	var url = ctx + "/manager/common/upload-image/upload.jhtml?media=Editor";
	CKEDITOR.replace(editorId,{filebrowserImageUploadUrl:url});
}

//
commonJs.setRegionChange = function(curObj,fatherId,selValue,funDo) {
	var data = {
		'fatherId': fatherId
	}
	$.ajax({
		url:		ctx+"/manager/system/zxbz-qy/showNextAreaList.jhtml",
		data:		data,
		type:		'POST',
		dataType:	'json',// xml,html,script,json,jsonp
		timeout:	30 * 60 * 1000,
		success:	function(data){   
			commonJs.setRegionOption(curObj,data,selValue);
			if (curObj.val() != null && curObj.val() != '' && funDo!=null) {
				try{ funDo();} catch(e){}// 加载县数据
			}
		},
		error:	function(){
			layer.alert('程序出错，请稍后再试！',{icon: 2,skin: layerIconStyle,closeBtn: 0});
		} 
	});
}
commonJs.setRegionOption = function(curObj,data,selValue){
	curObj.empty();//清空旧数据
	curObj.append("<option value=''>--请选择--</option>");
	if(data!=null){
		for(var i=0;i<data.length;i++){
			if(data[i].id==selValue){
				curObj.append("<option value='" + data[i].id + "' selected>" + data[i].qymc + "</option>");
			}else{
				curObj.append("<option value='" + data[i].id + "'>" + data[i].qymc + "</option>");
			}
		}
	}
}


commonJs.setRegionSelect = function(parameter){
	var selProvince = $('#' + parameter.provinceId);
	if(parameter.disabled==true){
		selProvince.attr("disabled","disabled");
	}
	var funCountyLoad = function() {
		var fatherId = $('#' + parameter.cityId).val();
		var selCounty = $('#' + parameter.countyId);
		selCounty.empty();
		if (fatherId == null || fatherId == '') { return true; }
		
		commonJs.setRegionChange(selCounty,fatherId,parameter.countyValue,null);
		if(parameter.disabled==true){
			selCounty.attr("disabled","disabled");
		}
	};
	var funCityLoad = function() {
		var fatherId = $('#' + parameter.provinceId).val();
		var selCity = $('#' + parameter.cityId);
		selCity.empty();
		if (parameter.countyId != null && parameter.countyId != '') {
			$('#' + parameter.countyId).empty();
		}
		if (fatherId == null || fatherId == '') { return true; }
		if (parameter.countyId == null || parameter.countyId == '') { 
			commonJs.setRegionChange(selCity,fatherId,parameter.cityValue,null);
		}else{
			commonJs.setRegionChange(selCity,fatherId,parameter.cityValue,funCountyLoad);
		}
		if(parameter.disabled==true){
			selCity.attr("disabled","disabled");
		}
	};
	//执行省查询
	commonJs.setRegionChange(selProvince,0,parameter.provinceValue,funCityLoad);
	if (parameter.cityId == null || parameter.cityId == '') { return true; }
	$('#' + parameter.provinceId).unbind();
	$('#' + parameter.provinceId).change(funCityLoad);	
	
	if (parameter.countyId == null || parameter.countyId == '') { return true; }
	$('#' + parameter.cityId).unbind();
	$('#' + parameter.cityId).change(funCountyLoad);
}

//跳转到下一页面
commonJs.openPage = function(obj) {
	var url = obj.url;
	if(obj.id!=undefined && obj.id!=null){
		url += "?id="+obj.id;
	}
	window.location.href = url;
}

//返回上一页面
commonJs.returnPage= function(obj) {
	window.history.back(-1);
}

/**
 * 保存表单
 * cfg.id			表单id
 * cfg.url			表单提交地址
 * cfg.callback		保存后回调
 * **/
commonJs.saveForm = function(cfg){
	var loading=null;
//	var param = $("#"+cfg.id).serialize();
	var param = $(cfg.id).serialize();
	$.ajax({
		type: "POST",
		url: cfg.url, 
		dataType: "json",
		data: param,
		async: true,
		beforeSend: function(){
			loading=parent.layer.load(0, {shade: [0.5,'#fff']});
		},
		success:function(result){
			var flag=result.flag;
			var msg=result.msg;
			if(flag=="1"){
				parent.layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
			}
			else{
				parent.layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			}
		},
		error:function(){
			parent.layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
		},
		complete:function(){
			if(loading!=null)
				parent.layer.close(loading);
		}
	});
}


/**
 * 删除一条记录
 * cfg.id			记录id
 * cfg.url			删除地址
 * cfg.callback		删除后回调
 * **/
commonJs.deleteRow = function(cfg){
	if(confirm("确定删除？")){
		var loading=null;
		$.ajax({
			type: "POST",
			url: cfg.url, 
			dataType: "json",
			data: {id:cfg.id},
			async: true,
			beforeSend: function(){
				loading=layer.load(0, {shade: [0.5,'#fff']});
			},
			success:function(result){
				var flag=result.flag;
				var msg=result.msg;
				if(flag=="1"){
					layer.msg(msg,{icon: 1,time: 2000,btn: ['确　定'],shade: [0.5,'#fff']},cfg.callback);
				}
				else{
					layer.msg(msg,{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
				}
			},
			error:function(){
				layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
			},
			complete:function(){
				if(loading!=null)
					layer.close(loading);
			}
		});
	}
}
