$(function(){
	
	//表格选取第一列
	$(".listTable tr").each(function(){
		$(this).find("td:first,th:first").addClass("firstTd");
	});
	
	//表格隔行颜色
	$(".listTable tr:even").addClass("even");
	
	//表格经过颜色
	$(".listTable tr:gt(0)").hover(
		function(){
			$(this).addClass("trOver");
		},
		function(){
			$(this).removeClass("trOver");
		}
	);
	//审核表格
	//$(".shenhe-table tr:odd").addClass("bg-gray");
	$(".shenhe-table textarea").each(function(){
		$(this).parents("tr").prev().find("td,th").css("border-bottom",0).css("padding-bottom",0);
	})
	
	//表单按钮经过样式
	$(".btn01").hover(
		function(){
			$(this).addClass("btnOver01");
		},
		function(){
			$(this).removeClass("btnOver01");
		}
	);
	
	//表格文本框得到失去焦点
	 $(".input").each(function (){
		 $(this).hover(
			function(){
				$(this).addClass("tableTextOver");
		 },
			function(){
				$(this).removeClass("tableTextOver");
		});
		 $(this).focus(function(){
			 $(this).addClass("inputOver");
		 });
		 $(this).blur(function(){
			 $(this).removeClass("inputOver");
		 });
	 });
	 
	 	
	$("#showAllMenu div").height($("#showAllMenu").height());

	/*$("#showAllMenu").hover(
		function(){
			$(this).show();
		},
		function(){
			$(this).hide();
		}
	)*/

	//工具栏最后一个
	$(".editBar li:last").css({background:"none",margin:"0"});
	
})