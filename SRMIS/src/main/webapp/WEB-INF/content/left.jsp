<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'left.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<%@ include file="/WEB-INF/common/introduce.jsp"%>
<link href="<c:url value='/css/base.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/sider.css'/>" rel="stylesheet" type="text/css" />
<script src="<c:url value='/js/jquery.treeview.pack.js'/>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	//初始树
	$("#siderNav").treeview({
		persist: "location",
		collapsed: true,
		unique: true
	});
	//当前树点击的样式
	//$("#siderNav>li").addClass("cat");
	$("#siderNav >li").click(function(){
		$("#siderNav >li").removeClass("open");
		$(this).addClass("open");
	});
	$("#siderNav a").click(function(){
		$("#siderNav a").removeClass("curr");
		$(this).addClass("curr");
	});
});
</script>
  </head>

<body>
 <div class="sider">
<%-- <div><strong><img src="<c:url value='/images/sider-title-*.png'/>">一级目录</strong></div> --%>
<%-- <div><strong><div style='white-space:nowrap;width:120px;overflow:hidden;'>一级目录</div></strong></div> --%>
<ul id="siderNav" class="treeview">
	<li>
		<span style="padding-left: 0;"><a href="<c:url value='/homeMainPage'/>" target="mainFrame" >
		<div style="white-space:nowrap;padding-left: 35px;overflow:hidden;">
		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		首页</div></a></span><ul style="display:none;padding-top: 0;border-bottom-width: 0;"></ul>
	</li>
	<li>
		<span><div style="white-space:nowrap;width:120px;overflow:hidden;">
		<span class="glyphicon glyphicon-book" aria-hidden="true"></span>&nbsp;科研资讯</div></span>
		<ul style="display:none;">
			<li><a href="<c:url value='/notice/nblist'/>" target="mainFrame" >
			<div style="white-space:nowrap;width:120px;overflow:hidden;">通知公告</div></a></li>
		<c:if test="${type==2 }">
			<li><a href="<c:url value='/notice/nbrlist'/>" target="mainFrame" >
			<div style="white-space:nowrap;width:120px;overflow:hidden;">通知公告发布</div></a></li>
		</c:if>		
			<li><a href="<c:url value='/notice/awnlist'/>" target="mainFrame" >
			<div style="white-space:nowrap;width:120px;overflow:hidden;">获奖喜讯</div></a></li>
		<c:if test="${type==2 }">	
			<li><a href="<c:url value='/notice/awnrlist'/>" target="mainFrame" >
			<div style="white-space:nowrap;width:120px;overflow:hidden;">获奖喜讯发布</div></a></li>
		</c:if>	
		</ul>
	</li>
	<li>
		<span><div style="white-space:nowrap;width:120px;overflow:hidden;">
		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>&nbsp;项目申请管理</div></span>
		<ul style="display:none;">
		<c:if test="${type==1||type==0 }">	
			<li><a href="<c:url value='/apply/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">项目申请</div></a></li>
		</c:if>
		<c:if test="${type==2 }">
			<li><a href="<c:url value='/apply/batch/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">批次设置</div></a></li>
			<li><a href="<c:url value='/apply/auditing/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">项目申请审核</div></a></li>
			<li><a href="<c:url value='/apply/organize/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">组织专家评审</div></a></li>
			<li><a href="<c:url value='/apply/establish/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">项目立项</div></a></li>
		</c:if>
		<c:if test="${type==1 }">			
			<li><a href="<c:url value='/apply/review/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">项目评审</div></a></li>		
		</c:if>
		</ul>
	</li>
	
	<li>
		<span style="padding-left: 0;"><a href="<c:url value='/inspect/list'/>" target="mainFrame" >
		<div style="white-space:nowrap;padding-left: 35px;overflow:hidden;">
		<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
		中期检查</div></a></span><ul style="display:none;padding-top: 0;border-bottom-width: 0;"></ul>
	</li>
	
	<li>
		<span><div style="white-space:nowrap;width:120px;overflow:hidden;">
		<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;项目验收管理</div></span>
		<ul style="display:none;">
		<c:if test="${type==1||type==0 }">		
			<li><a href="<c:url value='/accept/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">验收申请</div></a></li>
		</c:if>
		<c:if test="${type==2 }">		
			<li><a href="<c:url value='/accept/auditing/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">验收申请审核</div></a></li>
			<li><a href="<c:url value='/accept/conclude/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">验收结题</div></a></li>
		</c:if>	
		<c:if test="${type==1 }">			
			<li><a href="<c:url value='/accept/review/list'/>" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">结项评审</div></a></li>		
		</c:if>
		</ul>
	</li>
	
	<li>
		<span style="padding-left: 0;"><a href="<c:url value='/achieve/list'/>" target="mainFrame" >
		<div style="white-space:nowrap;padding-left: 35px;overflow:hidden;">
		<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
		科研成果管理</div></a></span><ul style="display:none;padding-top: 0;border-bottom-width: 0;"></ul>
	</li>
	
	<!-- <li class="collapsable open"><div class="hitarea collapsable-hitarea"></div> -->
	<li class="">
		<span class=""><div style="white-space:nowrap;width:120px;overflow:hidden;">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;信息维护</div></span>
		<ul style="display:none;">
		<!-- <ul style="display: block;"> -->
			<li><a href="${ctx }/maintenance/information" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">个人信息</div></a></li>
		<!-- </ul>
		<ul style="display: block;"> -->
			<li><a href="${ctx }/maintenance/modify" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">密码修改</div></a></li>
		<!-- </ul>
		<ul style="display: block;">
			<li><a href="${ctx }/im/um/edit" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">安全设置</div></a></li>
		 --><c:if test="${type==3 }">		
			<li><a href="${ctx }/maintenance/manage" target="mainFrame" ><div style="white-space:nowrap;width:120px;overflow:hidden;">用户管理</div></a></li>
		</c:if>
		</ul>
	</li>
</ul>
</div>
</body>
</html>

