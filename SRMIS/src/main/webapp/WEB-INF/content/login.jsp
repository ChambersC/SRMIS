<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>高校科研管理信息系统登录页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  	
    <%@ include file="/WEB-INF/common/introduce.jsp"%>
	<link rel="stylesheet" type="text/css" href="css/homePage.css">

</head>
<body>
    <div class="container">

      <form class="form-signin" action="${ctx}/home" method="post">
        <h2 class="form-signin-heading">
        <!-- 显示错误信息 -->
	    <c:if test="${errorMessage!=null}">${errorMessage }</c:if>
        <c:if test="${errorMessage==null}">请登录..</c:if></h2>
        <div class="main">
        <label for="inputText">账号</label>
        <input type="text" id="inputText" class="form-control" name="account" value="${user.account }"
        placeholder="Account" required autofocus>
        
        <label for="inputPassword" id="pwd">密码</label>
        <input type="password" id="inputPassword" class="form-control" name="password" value="${user.password }"
        placeholder="Password" required>
        
        <%-- <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> 记住密码
          </label>&nbsp;&nbsp;&nbsp;
          <a class="label-link" href="${ctx}/password_return">忘记密码</a>
        </div> --%>
        
        <button class="btn btn-lg btn-info btn-sm" type="submit">登录</button>
      	</div>
      </form>

    </div>
  </body>
</html>

