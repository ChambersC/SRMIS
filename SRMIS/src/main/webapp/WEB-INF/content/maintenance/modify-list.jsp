﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Password modify</title>
  <%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#form").validate({
		rules:{
			"oldpwd":"required",
			"password":{
				rangelength:[6,16],
				required:true
			},
			"password2":{
				equalTo:"#newpwd",
				required:true
			}
		},
		messages:{
			"oldpwd":"请输入旧密码",
			"password":{
				rangelength:"请输入 6 到 16 位的密码",
				required:"请输入新密码"
			},
			"password2":{
				equalTo:"两次密码不一致",
				required:"请再次输入新密码"
			}
		},
		submitHandler:function(form) { //通过之后回调
			if($("#oldpwd").val()=='${user.password}'){
				operateJs.saveForm({
					id : form,
					url : "${ctx}/maintenance/update",
					callback : function() {
						location.reload();
					}
				});
			}else {
				layer.msg("旧密码不正确",{icon: 0,time: 3000,btn: ['确　定']});
			}
		}
	});
});
</script>
<style type="text/css">
input[type="text"] { width:200px; }
</style>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>密码修改</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id" id="id" value="${user.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nt_table">
					<col width="30%;" />
					<col width="70%;" />
					<tr>
						<th class="gray">旧密码：</th>
						<td>
							<input type="password" name="oldpwd" id="oldpwd" class="input" />
						</td>
					</tr><tr>
						<th class="gray">新密码：</th>
						<td>
							<input type="password" name="password" id="newpwd" class="input" />
						</td>
					</tr><tr>
						<th class="gray">再次输入新密码：</th>
						<td>
							<input type="password" name="password2" id="newpwd2" class="input" />
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="btn01" value="保存" />
			</div>
		</form>
	</div>

</body>
</html>