﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Personal information</title>
  <%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#form").validate({
		rules:{
			"name":"required",
			"email":{
				email:true,
				required:true
			},
			"telephone":"required"
		},
		messages:{
			"name":"请输入姓名",
			"email":{
				email:"邮箱格式错误",
				required:"请输入邮箱"
			},
			"telephone":"请输入联系电话"
		},
		submitHandler:function(form) { //通过之后回调
			operateJs.saveForm({
				id : form,
				url : "${ctx}/maintenance/update",
				callback : function() {
					location.reload();
				}
			});
		}
	});
});
</script>
<style type="text/css">
input[type="text"] { width:200px; }
</style>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>个人信息</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id" id="id" value="${user.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nt_table">
					<col width="30%;" />
					<col width="70%;" />
					<tr>
						<th class="gray">姓名：</th>
						<td>
							<input type="text" name="name" id="name" value="${user.name}" class="input" />
						</td>
					</tr><tr>
						<th class="gray">登录账号：</th>
						<td>
							<input type="text" name="account" id="account" value="${user.account}" class="input" readonly="readonly" />
						</td>
					</tr>
					
					<c:if test="${not empty user.dep_id }">
						<tr>
							<th class="gray">科室：</th>
							<td>
								<input type="text" name="dname" id="dname" value="${user.dname}" class="input" readonly="readonly" />
							</td>
						</tr><tr>
							<th class="gray">职务：</th>
							<td>
								<input type="text" name="duty" id="duty" value="${user.duty}" class="input" readonly="readonly" />
							</td>
						</tr>
					</c:if>
					<tr>
						<th class="gray">联系电话：</th>
						<td>
							<input type="text" name="telephone" id="telephone" value="${user.telephone}" class="input" />
						</td>
					</tr><tr>
						<th class="gray">邮箱：</th>
						<td>
							<input type="text" name="email" id="email" value="${user.email}" class="input" />
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="btn01" value="保存" />
			</div>
		</form>
	</div>

</body>
</html>