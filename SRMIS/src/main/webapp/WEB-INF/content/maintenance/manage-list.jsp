<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>user management list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$(".selectClass").select2({
		placeholder : "-请选择-"
	});
});

	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/maintenance/manage";
	}
	
	//添加或者修改
	function openEdit(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/maintenance/edit";
		if (id != "") {
			url += "?id=" + id ;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);
	}
	
	function reset(id){
		operateJs.resetPwd({
			id : id,
			url : "${ctx}/maintenance/reset",
			callback : function() {
				location.reload();
			}
		});
	}
	
	function deleteRow(id) {
		commonJs.deleteRow({
			id : id,
			url : "${ctx}/maintenance/delete",
			callback : function() {
				location.reload();
			}
		});
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
</head>
  <body>
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>用户管理栏目</span></strong>
		</h2>
		<form action="<c:url value='/maintenance/manage'/>" id="form1" method="POST">
		  <div class="searchBar mT10">
			用户类型：<select name="type" id="type" class="selectClass" style="width: 90px">
         		    <option value='' <c:if test="${vo.type==null}">selected="selected"</c:if>>请选择</option>
				<option value='0' <c:if test="${vo.type==0}">selected="selected"</c:if>>普通教师</option>
				<option value='1' <c:if test="${vo.type==1}">selected="selected"</c:if>>评审专家</option>
				<option value='2' <c:if test="${vo.type==2}">selected="selected"</c:if>>科研管理人员</option>
				<option value='3' <c:if test="${vo.type==3}">selected="selected"</c:if>>系统管理员</option>
			</select>
			&nbsp;所属科室：<select name="depId" id="depId" class="selectClass" style="width: 90px">
         		<option value='' <c:if test="${vo.depId==null}">selected="selected"</c:if>>请选择</option>
				<c:choose>
					<c:when test="${depList!=null && fn:length(depList)>0 }">
						<c:forEach var="dep" items="${depList}" varStatus="status">
							<c:set var="depIndex" value="${status.index+1 }" />
							<option value='${dep.id}' <c:if test="${dep.id==vo.depId}">selected="selected"</c:if>>${dep.name}</option>
						</c:forEach>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			</select>
			&nbsp;姓名：<input name="name" type="text" class="input" value="${vo.name}" />  
			<input type="submit" onClick="return onceSubmit(this);" name="button22" class="btn01" value="查询" />
			<input type="button" onClick="openEdit('');" name="button22" class="btn01" value="新增" />
		  </div>
		</form>
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }" 
			requestURI="/maintenance/manage" style="width: 100%;"  partialList="true" size="resultSize" >
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="用户类型">
				<c:if test="${bean.type==0}">普通教师</c:if>
				<c:if test="${bean.type==1}">评审专家</c:if>
				<c:if test="${bean.type==2}">科研管理人员</c:if>
				<c:if test="${bean.type==3}">系统管理员</c:if>
			</display:column>
			<display:column title="账号">${bean.account}</display:column>
			<display:column title="姓名">${bean.name}</display:column>
			<display:column title="所属科室">${bean.dname}</display:column>
			<display:column title="职务">${bean.duty}</display:column>
			<display:column title="联系电话">${bean.telephone}</display:column>
			<display:column title="邮箱">${bean.email}</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:openEdit('${bean.id}');">修改</a>
				<a class="editBtn012" href="javascript:reset('${bean.id}');">重置密码</a>
				<a class="editBtn01" href="javascript:deleteRow('${bean.id}');">删除</a>
			</display:column>
		</display:table>
	</div>
  </body>
</html>
