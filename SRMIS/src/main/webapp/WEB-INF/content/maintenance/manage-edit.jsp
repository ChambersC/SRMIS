﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<%@ page import="cn.srmis.user.po.User" %>
<%
	List<String> dutyList = new ArrayList<String>();
	User u = (User)request.getAttribute("user");
	if(u!=null){
		if(u.getType().equals("0")){
			String a = "助教";	dutyList.add(a);
			String b = "副讲师";	dutyList.add(b);
			String c = "讲师";	dutyList.add(c);
		}else if(u.getType().equals("1")){
			String a = "副教授";	dutyList.add(a);
			String b = "教授";	dutyList.add(b);
			String c = "副主任";	dutyList.add(c);
			String d = "主任";	dutyList.add(d);
			String e = "副院长";	dutyList.add(e);
			String f = "院长";	dutyList.add(f);
		}
	}
 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Personal information</title>
  <%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#form").validate({
		rules:{
			"name":"required",
			"account":"required"
		},
		messages:{
			"name":"请输入姓名",
			"account":"请填写登录账号"
		},
		submitHandler:function(form) { //通过之后回调
			if($("#type").val()==""){
				layer.msg("请选择用户类型",{icon: 0,time: 2000,btn: ['确　定']});
				return;
			}else if($("#type").val()=="0"||$("#type").val()=="1"){
				if($("#depId").val()==""){
					layer.msg("请选择科室",{icon: 0,time: 2000,btn: ['确　定']});
					return;
				}
				if($("#duty").val()==""){
					layer.msg("请选择职务",{icon: 0,time: 2000,btn: ['确　定']});
					return;
				}
			}
			var account = $("#account").val();
			var listUrl = "${ctx}/maintenance/manage";
			var url = "${ctx}/maintenance/accountValidation";
			if('${user.id}'!=""){
				if (confirm("是否要保存？")) {
		    		commonJs.saveForm({
						id:form,
						url:"${ctx}/maintenance/save",
						callback:function(){
							parent.sx();
						}
					});
		    	}
			}else {
				$.ajax({
					type: "POST",
					url: url,
					data:{"account":account},
					async: false,
					success:function(result){
					    var data = eval('(' + result + ')');
					    var isNotUsed = data.isNotUsed;
					    if(isNotUsed == "Y"){//未使用
					    	if (confirm("是否要保存？")) {
					    		commonJs.saveForm({
									id:form,
									url:"${ctx}/maintenance/save",
									callback:function(){
										parent.window.location.href = listUrl;
									}
								});
					    	}
					    }else if(isNotUsed == "N"){//已使用
					    	alert("该账号已被使用！");
					    	return false;
					    }
					}
				});
			}
		}
	});
	
	$(".selectClass").select2({
		placeholder : "-请选择-"
	});
	
	//单位带出科室、附加职务
	$("#type").on("change", function(e) {
		var type=$("#type").val();
		$.ajax({
			type: "POST",
			url: "${ctx}/maintenance/dcks",
			data:{"type":type},
			async: false,
			success:function(result){
			    var data = eval('(' + result + ')');
			    var depObjList=data.depObjList;
			    var ks = document.getElementById('depId');
			    var duty = document.getElementById('duty');
			    if(depObjList!=null&&depObjList.length>0){
			    	ks.length=depObjList.length+1;
			    	for(var i=0;i<depObjList.length;i++){
			    		ks.options[i+1] = new Option(depObjList[i][1],depObjList[i][0]); 
				    }
				    if(type==0){
				    	duty.length=4;
				    	duty.options[1] = new Option("助教"); 
				    	duty.options[2] = new Option("副讲师"); 
				    	duty.options[3] = new Option("讲师"); 
				    }else{
				   	 	duty.length=7;
				   	 	duty.options[1] = new Option("副教授"); 
				    	duty.options[2] = new Option("教授"); 
				    	duty.options[3] = new Option("副主任");
				    	duty.options[4] = new Option("主任"); 
				    	duty.options[5] = new Option("副院长"); 
				    	duty.options[6] = new Option("院长");
				    }
			    }else{
			    	ks.length=1;
			    	duty.length=1;
			    }
			}
		});
	});
	
});

</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>用户编辑</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id" id="id" value="${user.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nt_table">
					<col width="15%;" />
					<col width="35%;" />
					<col width="15%;" />
					<col width="35%;" />
					<tr>
						<th class="gray">姓名：</th>
						<td>
							<input type="text" name="name" id="name" value="${user.name}" class="input" />
							<span class="star">*</span>
						</td>
						
						<th class="gray">用户类型：</th>
						<td>
							<select name="type" id="type" class="selectClass" style="width: 100px">
								<option value="" >--请选择--</option>
								<option value="0" <c:if test="${user.type=='0' }">selected='selected'</c:if>>普通教师</option>
								<option value="1" <c:if test="${user.type=='1' }">selected='selected'</c:if>>评审专家</option>
								<option value="2" <c:if test="${user.type=='2' }">selected='selected'</c:if>>科研管理人员</option>
								<option value="3" <c:if test="${user.type=='3' }">selected='selected'</c:if>>系统管理员</option>
							</select>
							<span class="star">*</span>
						</td>
					</tr>
					<tr>
						<th class="gray">登录账号：</th>
						<td>
							<input type="text" name="account" id="account" value="${user.account}" class="input" <c:if test="${user.id != null}">readonly='readonly'</c:if> />
							<span class="star">*</span>
						</td>
						
						<th class="gray">科室：</th>
						<td>
							<select id="depId" name="dep_id" class="selectClass" style="width: 100px">
								<option value="">-请选择-</option>
								<c:if test="${not empty user.dep_id }">
								<c:forEach var="ks" items="${requestScope.depList }">
									<option value="${ks.id}" <c:if test="${ks.id==user.dep_id}">selected="selected"</c:if>>${ks.name}</option>
								</c:forEach></c:if>
							</select>
						</td>
					</tr>
					<tr>
						<th class="gray"></th>
						<td>
						</td>
						<th class="gray">职务：</th>
						<td>
							<select id="duty" name="duty" class="selectClass" style="width: 100px">
								<option value="">-请选择-</option>
								<c:if test="${not empty user.duty }">
								<c:forEach var="duty" items="<%=dutyList %>">
									<option value="${duty}" <c:if test="${duty==user.duty}">selected="selected"</c:if>>${duty}</option>
								</c:forEach></c:if>
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="btn01" value="保存" />
				<input type="button" name="button2" class="btn01" value="返回" onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>

</body>
</html>