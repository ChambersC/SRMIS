<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>notice bulletin list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<link href="<c:url value='/css/style.css'/>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//通知公告查看详细
function open(id){
	commonJs.openWindow({url:"${ctx}/notice/nbview",id:id,title:"通知公告",maximize:true});
}
</script>
  </head>
 
  <body>
  	<div class="main">
		<h2 class="subTitle mT10" >
			<strong><span>通知公告</span></strong>
		</h2>
	</div>
    <div class="bg3">
		<div class="container column c1" style="padding-left: 0px; margin-top: 0px;">
			<div class="tab_LR">
				<%-- <div class="tab_nav">
					<ul class="clear">
						<c:forEach var="nav" items="${requestScope.navList }" varStatus="navStatus">
							<li <c:if test="${navStatus.index==0 }">class="on"</c:if>>${nav }</li>
						</c:forEach>
					</ul>
				</div> --%>
				<div class="tab_main" style="display:block">
					<div class="list_div">
						<ul>
						
						<li style="border-bottom-style:solid; border-bottom-width: 1px;"><strong>
							<span style="color: #666;font-size: 16px;">发布时间</span>
							<i style="width: 35px; margin-right: 7px;">序号</i>
        					标题    
						</strong></li>
						
							<c:forEach var="data" items="${list }" varStatus="dataStatus">
								<li>
									<span><fmt:formatDate pattern="yyyy-MM-dd" value="${data.fbsj }" /></span>
									<i>${dataStatus.count }</i>
          							<a href="javascript:open('${data.id}');" >${data.btmc }</a>              
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
  </body>
</html>
