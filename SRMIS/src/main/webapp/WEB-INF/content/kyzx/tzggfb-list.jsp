<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>notice bulletin release list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	
<script type="text/javascript" language="javascript">
	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/notice/nbrlist";
	}

	//查看
	function view(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/notice/nbrview";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);  
	}

	function deleteRow(id) {
		commonJs.deleteRow({
			id : id,
			url : "${ctx}/notice/nbrdelete",
			callback : function() {
				location.reload();
			}
		});
	}

	//添加或者修改
	function openEdit(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/notice/nbredit";
		if (id != "") {
			url += "?id=" + id ;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);
	}
	
	/**
     * 发布
     */
    function fb(id){
   	   operateJs.fb({
			id : id,
			url : "${ctx}/notice/nbrfb",
			callback : function() {
				location.reload();
			}
		});
    }
	
	/**
     * 下架
     */
    function xj(id){
   	   operateJs.xj({
			id : id,
			url : "${ctx}/notice/nbrxj",
			callback : function() {
				location.reload();
			}
		});
    }
	
    /**
     * 置顶
     */
    function zd(id){
	   operateJs.zd({
			id : id,
			url : "${ctx}/notice/nbrzd",
			callback : function() {
				location.reload();
			}
		});
     }
      
	/**
	 * 取消置顶
     */
	function qxzd(id){
     	operateJs.qxzd({
			id : id,
			url : "${ctx}/notice/nbrqxzd",
			callback : function() {
				location.reload();
			}
		});
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
  </head>
 
  <body>
    <!-- <div class="pos noprint">
		<strong>当前位置：</strong>通知公告
	</div> -->
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>通知公告发布栏</span></strong>
		</h2>
		
			<form action="<c:url value='/notice/nbrlist'/>" id="form1" method="POST">
				<div class="searchBar mT10">
				标题名称：<input name="btmc" type="text" class="input" value="${vo.btmc}" /> 
				发布时间：<input class="input"  id="fbkssj" name="fbkssj"
					value="<fmt:formatDate value="${vo.fbkssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'fbjssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				至 <input class="input" id="fbjssj" name="fbjssj"
					value="<fmt:formatDate value="${vo.fbjssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'fbkssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				<input type="submit" onClick="return onceSubmit(this);" name="button22" class="btn01" value="查询" />
				<input type="button" onClick="openEdit('');" name="button22" class="btn01" value="添加" />
			</div>
		</form>
		
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }" 
			requestURI="/notice/nbrlist" style="width: 100%;"  partialList="true" size="resultSize" >
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="编号"  sortable="true">${bean.bh}</display:column>
			<display:column title="标题名称">
				<c:set var="testTitle" value="${bean.btmc}"></c:set> 
    				<c:choose>  
    					<c:when test="${fn:length(testTitle) > 26}">  
            				<c:out value="${fn:substring(testTitle, 0, 26)}......" />  
        				</c:when>  
       					<c:otherwise>  
          					<c:out value="${testTitle}" />  
        				</c:otherwise>  
    			  </c:choose> 
			</display:column>
			<display:column title="发布人姓名">${bean.fbrxm}</display:column>
			<display:column title="发布时间">
				<c:if test="${bean.fbzt!='0'}"><fmt:formatDate value="${bean.fbsj}" pattern="yyyy-MM-dd" /></c:if>
			</display:column>
			<display:column title="发布状态">
				<c:if test="${bean.fbzt=='0'}">未发布</c:if>
				<c:if test="${bean.fbzt=='1'}">已发布</c:if>
				<c:if test="${bean.fbzt=='2'}">已置顶</c:if>
				<c:if test="${bean.fbzt=='3'}">已下架</c:if>
			</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:view('${bean.id}');">查看</a>
				<c:if test="${bean.fbzt=='0'}">
					<a class="editBtn01" href="javascript:fb('${bean.id}');">发布</a>
				</c:if>
				<c:if test="${bean.fbzt=='1'}">
					<a class="editBtn01" href="javascript:zd('${bean.id}');">置顶</a>
					<a class="editBtn01" href="javascript:xj('${bean.id}');">下架</a>
				</c:if>
				<c:if test="${bean.fbzt=='2'}">
					<a class="editBtn01" href="javascript:xj('${bean.id}');">下架</a>
					<a class="editBtn012" href="javascript:qxzd('${bean.id}');">取消置顶</a>
				</c:if>
				<c:if test="${bean.fbzt=='3'}">
					<a class="editBtn012" href="javascript:fb('${bean.id}');">重新发布</a>
				</c:if>
				<c:if test="${bean.fbzt==0 || bean.fbzt==3 }">
					<a class="editBtn01" href="javascript:openEdit('${bean.id}');">编辑</a>
					<a class="editBtn01" href="javascript:deleteRow('${bean.id}');">删除</a>
				</c:if>
			</display:column>
		</display:table>
	</div>
  </body>
</html>
