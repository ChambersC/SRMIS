﻿﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>notice bulletin view</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>

<link href="<c:url value='/css/style.css'/>" rel="stylesheet" type="text/css" />
<style type="text/css">
h1,h3 {text-align: center;}
</style>
</head>
<body>
	<div class="main">
		<h2 class="subTitle mT10" >
			<strong><span>通知公告查看</span></strong>
		</h2>
	</div>
	
	<div class="bg3">
        <div class="container column c1">
            <div class="details2">
                <h1>${noticeBulletin.btmc}</h1>
                <h3>
                	<c:if test="${not empty noticeBulletin.zgr}">
                		<span>撰稿：${noticeBulletin.zgr}</span>
                	</c:if>
                	<c:if test="${not empty noticeBulletin.fbsj }">
                		<span>发布时间：<fmt:formatDate pattern="yyyy-MM-dd" value="${noticeBulletin.fbsj }" /></span>
                	</c:if>
                </h3>
                <div class="de_box">
                	<div class="de_con">
                       ${noticeBulletin.zynr }
                    </div>
                    <c:if test="${not empty fileList }">
	                    <div class="de_file">
	                    	<ul>
	                    		<c:forEach var="file" items="${fileList }">
	                    			<li>•<a href="${ctx }/attachment/download?id=${file.id }" target="">${file.fjmc }</a></li>
	                    		</c:forEach>
	                    	</ul>
	                    </div>
                    </c:if>	
                </div>
            </div>
        </div>
        <div class="nbot_control">
			<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
		</div>
    </div>
</body>
</html>