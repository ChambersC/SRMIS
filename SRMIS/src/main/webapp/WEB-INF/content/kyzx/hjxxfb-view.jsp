﻿﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>award-winning news release view</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<!-- 引入上传组件 -->
<link href="<c:url value='/js/uploadify/uploadify.css'/>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value='/js/uploadify/jquery.uploadify.js'/>"></script>
<!--引入ueditor编辑器begin-->
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/ueditor.config.js'/>"></script>
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/ueditor.all.js'/>"> </script>
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/lang/zh-cn/zh-cn.js'/>"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	UE.getEditor("zynr",{readonly:true});
});
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>获奖喜讯内容查看</span></strong>
		</h2>
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray">编号：</th>
						<td>${awardWinningNews.bh}</td>
					</tr>
					<tr>
						<th class="gray">标题名称：</th>
						<td>${awardWinningNews.btmc}</td>
					</tr>
					<tr>
						<th class="gray">主要内容：</th>
						<td ><textarea name="awardWinningNews.zynr" id="zynr" style="width:98%;" rows="5">${awardWinningNews.zynr}</textarea></td>
					</tr>
					<tr>
						<th class="gray">撰稿人：</th>
						<td>${awardWinningNews.zgr}</td>
					</tr>
					<tr>
						<th class="gray">发布人：</th>
						<td>${awardWinningNews.fbrxm}</td>
					</tr>
					<tr>
						<th class="gray">附件：</th>
						<td><jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value=".gif;*.jpg;*.jpeg;*.png;*.bmp;*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						<jsp:param name="isRead" value="true" />
						</jsp:include>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input
					type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
	</div>
</body>
</html>