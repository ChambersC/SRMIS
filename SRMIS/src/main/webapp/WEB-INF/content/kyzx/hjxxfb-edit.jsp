﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>award-winning news release edit</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<!--引入ueditor编辑器begin-->
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/ueditor.config.js'/>"></script>
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/ueditor.all.js'/>"> </script>
<script type="text/javascript" charset="utf-8" src="<c:url value='/js/ueditor/lang/zh-cn/zh-cn.js'/>"></script>
<script src="${ctx}/js/commonJs.js" type="text/javascript"></script><script type="text/javascript" language="javascript">
	$(document).ready(function() {
		//实例化编辑器
		UE.getEditor("zynr");
		$("#form").validate({
			rules : {
				"bh" : {
					required:true,
					sfcz:true
				},
				"btmc" : {
					required:true,
					maxlength:255 
				}/* ,
				"zynr" : "required" */
			},
			messages : {
				"bh" : {
					required:"请输入编号" ,
					sfcz:"编号已存在" 
				},
				"btmc" : {
					required:"请输入标题名称",
					maxlength:$.validator.format("标题已超出{0}字")
				}/* ,
				"zynr" : "请填写主要内容" */
			},
			submitHandler : function(form) { //通过之后回调
				//document.getElementById('fj').contentWindow.getMultiFileIds();
				commonJs.saveForm({
					id : form,
					url : "${ctx}/notice/awnrsave",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});
	});
	
	jQuery.validator.addMethod("sfcz", function(value, element) {  
		var bh=value;
		if(bh!=null&&bh!=""){
			var deferred = $.Deferred();//创建一个延迟对象
			var id=$("#Id").val();
			$.ajax({
				type: "POST",
				url: "${ctx}/notice/bhsfcz2", 
				dataType: "json",
				data: {id:id,bh:bh},
				async: false,
				beforeSend: function(){
					loading=layer.load(0, {shade: [0.5,'#fff']});
				},
				success:function(result){
					var flag=result.flag;
					if(flag=="1"){
						deferred.reject();
					}else{
						 deferred.resolve();   
					}
				},
				error:function(){
					layer.msg("程序出错，请联系技术支持!",{icon: 2,btn: ['确　定'],shade: [0.5,'#fff']});
				},
				complete:function(){
					if(loading!=null)
						layer.close(loading);
				}
			});
			return deferred.state() == "resolved" ? true : false;
		}else{
			return true;
		}
	}, "编号已存在"); 
	
	//iframe上传文件后回调函数
	function setFjxx(multiFileIds){
		$("#multiFileIds").val(multiFileIds);
	}
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>获奖喜讯编辑</span></strong>
		</h2>
		<form id="form" method="post" action="#" enctype="multipart/form-data" >
			<input type="hidden" name="id"	id="Id" value="${awardWinningNews.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray"><span class="star">*</span>编号：</th>
						<td><input type="text" name="bh"
							value="${awardWinningNews.bh}" class="input" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>标题名称：</th>
						<td><input type="text" name="btmc" 
							value="${awardWinningNews.btmc}" class="input" size="95"/></td>
					</tr>
					<tr>
						<th class="gray">主要内容：</th>
						<td ><textarea name="zynr" id="zynr" style="width:98%;" rows="5">${awardWinningNews.zynr}</textarea></td>
					</tr>
					<tr>
						<th class="gray">附件：</th>
						<td><jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value="*.gif;*.jpg;*.jpeg;*.png;*.bmp;*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						</jsp:include>
						<label style="color:red;">支持格式：*.gif;*.jpg;*.jpeg;*.png;*.bmp;*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf，上传限制20M</label>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="确定" /> 
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>