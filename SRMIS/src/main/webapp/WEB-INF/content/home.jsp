<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>高校科研管理信息系统</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  	
<link href="<c:url value='/css/base.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/index.css'/>" rel="stylesheet" type="text/css" />
<script src="${ctx}/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<c:url value='/js/common.js'/>" type="text/javascript"></script>
<script>
function auto(){
	var midHeight = document.body.clientHeight - 111;
	var mainWidth = document.body.clientWidth - 202;
	document.getElementById("mid").style.height=midHeight+"px";
	document.getElementById("main").style.width=mainWidth+"px";
}
window.onload=auto;
window.onresize=auto;
$(function(){
	/* $(".showAllMenu").hover(
		function(){
			$(this).addClass("showAllMenuOver");
			$("#showAllMenu").show();
		},
		function(){
			$(this).removeClass("showAllMenuOver");
			$("#showAllMenu").hide();
		}
	); */
	
	$(".swich").toggle(
		function(){
			$(this).addClass("swich01");
			$("#left").hide();
			$("#main").css("width",document.body.clientWidth-10+"px");
			
		},
		function(){
			$(this).removeClass("swich01");
			$("#left").show();
			$("#main").css("width",document.body.clientWidth-202+"px");
			
		}
	);
});

</script>
<style type="text/css">
@media print {.noprint {display: none;}.PageNext{page-break-after: always;}}
</style>
</head>
<body>
	<%-- <div class="showAllMenu noprint">
			<span>显示全部菜单</span>
			<div id="showAllMenu">
				<div></div>
				<ul class="clearFix">
				   ${headMenuStr}
				</ul>
			</div>
		</div> --%>
<!-- <div style="width: 100%;height: 109px; overflow: hidden;" class="noprint"> -->
<div style="width: 100%;height: 92px; overflow: hidden;" class="noprint">
    <iframe src="<c:url value='/head'/>" width="100%" scrolling="no" frameborder="0" name="headFrame"></iframe>
</div>
<div id="mid" style="height: 100%;">
    <div id="left" style="float:left; width:192px; height:100%" class="noprint">
        <iframe name="leftFrame" src="<c:url value='/homeMenuPage'/>" width="192" height="100%" frameborder="0"></iframe>
    </div>
    <div class="swich" style="float:left; width:10px; height:100%" class="noprint">隐藏左侧菜单</div>
    <div id="main" style="float:left; width:100%; height:100%">
        <iframe name="mainFrame" src="<c:url value='/homeMainPage'/>" width="100%" height="100%" frameborder="0"></iframe>
    </div>
</div>
<div style="width: 100%;height: 22px;" class="noprint">
    <iframe src="<c:url value='/homeFooterPage'/>" width="100%" height="22px" scrolling="no" frameborder="0"></iframe>
</div>
</body>
</html>