<%@ page language="java" import="java.util.*"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>head</title>
 <%@ include file="/WEB-INF/common/introduce.jsp"%>
<link href="<c:url value='/css/base.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/index.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/common.css'/>" rel="stylesheet" type="text/css" />
<script language="javascript">
	function doLoginOut(){
		if(confirm('确定退出系统？')){
			window.location.href = "${ctx}/index";
		}else{
			return false;
		}
	}
	function menuStyle(menuId){
		$("#navMenu ul li").find("a").each(function () {
			$(this).removeClass("curr"); 
		});
		$("#"+menuId).addClass("curr");
	}
	$(function(){
		$(".showAllMenu").hover(
				function(){
					$(this).addClass("showAllMenuOver");
					$("#showAllMenu").show();
				},
				function(){
					$(this).removeClass("showAllMenuOver");
					$("#showAllMenu").hide();
				}
		);
	});	
</script>
</head>
<body>
<div class="head">
    <div class="top clearFix">
        <h1 class="logo"><img src="<c:url value='/images/logo.png'/>" alt="高校科研管理信息系统" style="height:84px" /></h1>
        <dl class="clearFix" style="margin-top: 25px;">
            <dt style="line-height: 33px;font-weight: normal;"><strong>${loginName}</strong>，您好！</dt>
            <dd class="clearFix" style="line-height: 33px; ">
            	<a href="<c:url value='/homeMainPage'/>" target="mainFrame">站内邮件&nbsp;<span class="badge">112</span></a>
            	<a href="" target="_parent"></a>
            	<a href="" target="mainFrame">个人设置</a>
            	<a href="" target="_parent" onClick="return doLoginOut();">退出系统</a>
            </dd>
        </dl>
    </div>
	 <div id="navMenu" class="nav">
        <ul class="clearFix">
        	&nbsp;<%-- ${headMenuStr} --%>
        </ul>
    </div>

</div>
</body>
</html>
