﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>project application edit</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				"name" : {
					required:true,
					maxlength:30 
				},
				"content":"required",
				"funds":{
					required:true,
					number:true 
				},
				"wcsj":"required"
			},
			messages : {
				"pcmc" : {
					required:"请输入项目名称",
					maxlength:$.validator.format("标题已超出{0}字")
				},
				"content":"请输入项目内容介绍",
				"funds":{
					required:"请输入所需经费"/* ,
					number:"请输入有效的数字（<-默认的）" */
				},
				"wcsj":"请选择预计完成时间"
			},
			submitHandler : function(form) { //通过之后回调
				if($("#batch_id").val()==""){
					layer.msg("请选择所属批次",{icon: 0,time: 2000,btn: ['确　定']});
					return;
				}
				if($("#dep_id").val()==""){
					layer.msg("请选择所属学科",{icon: 0,time: 2000,btn: ['确　定']});
					return;
				}
				commonJs.saveForm({
					id : form,
					url : "${ctx}/apply/save",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});
		
		$(".selectClass").select2({
			placeholder : "--请选择--"
		});
	});
	
	//iframe上传文件后回调函数
	function setFjxx(multiFileIds){
		$("#multiFileIds").val(multiFileIds);
	}
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>项目申请编辑</span></strong>
		</h2>
		<form id="form" method="post" action="#" enctype="multipart/form-data">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<input type="hidden" name="status" value="${project.status}"/>
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray"><span class="star">*</span>所属批次：</th>
						<td><select name="batch_id" id="batch_id" class="selectClass" style="width: 100px">
							<option value='' <c:if test="${project.batch_id==null}">selected="selected"</c:if>>请选择</option>
							<c:choose>
								<c:when test="${batchList!=null && fn:length(batchList)>0 }">
									<c:forEach var="batch" items="${batchList}" varStatus="status">
										<c:set var="batchIndex" value="${status.index+1 }" />
										<option value='${batch.id}' <c:if test="${batch.id==project.batch_id}">selected="selected"</c:if>>${batch.pcmc}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</select></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>所属学科：</th>
						<td><select name="dep_id" id="dep_id" class="selectClass" style="width: 100px">
							<option value='' <c:if test="${project.dep_id==null}">selected="selected"</c:if>>请选择</option>
							<c:choose>
								<c:when test="${depList!=null && fn:length(depList)>0 }">
									<c:forEach var="dep" items="${depList}" varStatus="status">
										<c:set var="depIndex" value="${status.index+1 }" />
										<option value='${dep.id}' <c:if test="${dep.id==project.dep_id}">selected="selected"</c:if>>${dep.subject}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</select></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>项目名称：</th>
						<td><input type="text" name="name" value="${project.name}" class="input" size="50" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>项目内容介绍：</th>
						<td><textarea name="content" id="content" style="width:98%;" rows="15">${project.content}</textarea></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>所需经费：</th>
						<td><input type="text" name="funds" value="${project.funds}" class="input" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>预计完成时间：</th>
						<td><input class="Wdate" name="wcsj" value="<fmt:formatDate value="${project.wcsj}" pattern="yyyy-MM-dd" />" type="text" onfocus="WdatePicker({minDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"/></td>
					</tr>
					<tr>
						<th class="gray">附件：</th>
						<td>
						<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value="*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						</jsp:include>
						<label style="color:red;">支持格式：*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf，上传限制20M</label>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="确定" /> 
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>