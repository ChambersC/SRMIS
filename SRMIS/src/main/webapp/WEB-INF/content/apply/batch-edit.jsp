﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>batch setting edit</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				"pcmc" : {
					required:true,
					maxlength:50 
				},
				"kssj":"required",
				"jssj":"required"
			},
			messages : {
				"pcmc" : {
					required:"请输入批次名称",
					maxlength:$.validator.format("标题已超出{0}字")
				},
				"kssj":"请选择开始时间",
				"jssj":"请选择结束时间"
			},
			submitHandler : function(form) { //通过之后回调
				commonJs.saveForm({
					id : form,
					url : "${ctx}/apply/batch/save",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});
	});
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>批次设置编辑</span></strong>
		</h2>
		<form id="form" method="post" action="#" >
			<input type="hidden" name="id"	id="Id" value="${batchSetting.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="30%" />
					<col width="70%" />
					<tr>
						<th class="gray"><span class="star">*</span>批次名称：</th>
						<td><input type="text" name="pcmc"
							value="${batchSetting.pcmc}" class="input" size="30" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>申请开放开始时间：</th>
						<td colspan="3"><input class="Wdate" name="kssj" value="<fmt:formatDate value="${batchSetting.kssj}" pattern="yyyy-MM-dd" />" type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jssj\')}',dateFmt:'yyyy-MM-dd'})" id="kssj" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>申请开放结束时间：</th>
						<td colspan="3"><input class="Wdate" name="jssj" value="<fmt:formatDate value="${batchSetting.jssj}" pattern="yyyy-MM-dd" />" type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'kssj\')}',dateFmt:'yyyy-MM-dd'})" id="jssj" /></td>
					</tr>
					<tr>
						<th class="gray">备注：</th>
						<td><input type="text" name="bz" value="${batchSetting.bz}" class="input" size="30" /></td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="确定" /> 
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>