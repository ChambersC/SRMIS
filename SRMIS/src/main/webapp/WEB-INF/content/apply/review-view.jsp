﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>project review view</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>项目评审</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray">项目所属批次：</th>
						<td>${project.batchSetting.pcmc}</td>
					</tr>
					<tr>
						<th class="gray">项目所属学科：</th>
						<td>${project.subject}</td>
					</tr>
					<tr>
						<th class="gray">项目名称：</th>
						<td>${project.name}</td>
					</tr>
					<tr>
						<th class="gray">项目内容介绍：</th>
						<td><textarea name="content" id="content" style="width:98%;" rows="15" readonly="readonly">${project.content}</textarea></td>
					</tr>
					<tr>
						<th class="gray">所需经费：</th>
						<td>${project.funds}</td>
					</tr>
					<tr>
						<th class="gray">预计完成时间：</th>
						<td><fmt:formatDate value="${project.wcsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">附件：</th>
						<td>
						<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value="*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						<jsp:param name="isRead" value="true" />
						</jsp:include>
						</td>
					</tr>
					<tr>
						<th class="gray">申请人：</th>
						<td>${project.user.dname} ${project.user.name} ${project.user.duty}</td>
					</tr>
					<tr>
						<th class="gray">申请时间：</th>
						<td><fmt:formatDate value="${project.application.sqsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">审核人：</th>
						<td>${project.application.shr }</td>
					</tr>
					<tr>
						<th class="gray">审核时间：</th>
						<td><fmt:formatDate value="${project.application.shsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<c:if test="${review.opinion!=null }">
						<tr>
							<th class="gray">评审意见：</th>
							<td><c:if test="${review.opinion==1 }">通过</c:if>
								<c:if test="${review.opinion==2 }">不通过</c:if>
								<c:if test="${review.opinion==3 }">退回</c:if></td>
						</tr>
						<tr>
							<th class="gray">评语：</th>
							<td>${review.comment }</td>
						</tr>
						<tr>
							<th class="gray">评审时间：</th>
							<td><fmt:formatDate value="${review.pssj }" pattern="yyyy-MM-dd"/></td>
						</tr>
					</c:if>
				</table>
			</div>
			<div class="nbot_control">
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>