<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>batch setting list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	
<script type="text/javascript" language="javascript">
	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/apply/batch/list";
	}

	function deleteRow(id) {
		commonJs.deleteRow({
			id : id,
			url : "${ctx}/apply/batch/delete",
			callback : function() {
				location.reload();
			}
		});
	}

	//添加或者修改
	function openEdit(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/apply/batch/edit";
		if (id != "") {
			url += "?id=" + id ;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);  
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
</head>
  <body>
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>批次设置</span></strong>
		</h2>
		<form action="<c:url value='/apply/batch/list'/>" id="form1" method="POST">
			<div class="searchBar mT10">
				批次名称：<input name="mc" type="text" class="input" value="${vo.mc}" />  
				开放时间：<input class="input"  id="kssj" name="kssj"
					value="<fmt:formatDate value="${vo.kssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				至 <input class="input" id="jssj" name="jssj"
					value="<fmt:formatDate value="${vo.jssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'kssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				<input type="submit" onClick="return onceSubmit(this);" name="button22" class="btn01" value="查询" />
				<input type="button" onClick="openEdit('');" name="button22" class="btn01" value="新增" />
			</div>
		</form>
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }"
			requestURI="/apply/batch/list" style="width: 100%;" partialList="true" size="resultSize" >
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="批次名称" >
				<c:set var="testTitle" value="${bean.pcmc}"></c:set> 
    				<c:choose>  
    					<c:when test="${fn:length(testTitle) > 26}">  
            				<c:out value="${fn:substring(testTitle, 0, 26)}......" />  
        				</c:when>  
       					<c:otherwise>  
          					<c:out value="${testTitle}" />  
        				</c:otherwise>  
    			  </c:choose> 
			</display:column>
			<display:column title="申请开放开始时间"><fmt:formatDate value="${bean.kssj}" pattern="yyyy-MM-dd" /></display:column>
			<display:column title="结束时间"><fmt:formatDate value="${bean.jssj}" pattern="yyyy-MM-dd" /></display:column>
			<display:column title="备注">${bean.bz}</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:openEdit('${bean.id}');">编辑</a>
				<a class="editBtn01" href="javascript:deleteRow('${bean.id}');">删除</a>
			</display:column>
		</display:table>
	</div>
  </body>
</html>
