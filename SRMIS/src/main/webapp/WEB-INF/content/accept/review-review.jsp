﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>accept review review</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				
			},
			messages : {
				
			},
			submitHandler : function(form) { //通过之后回调
				if($("input:radio:checked").val()==null){
					layer.msg("请选择评审意见",{icon: 0,time: 2000,btn: ['确　定']});
					return;
				}
				commonJs.saveForm({
					id : form,
					url : "${ctx}/accept/review/save",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});	
	});
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>验收评审</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray">项目所属批次：</th>
						<td>${project.batchSetting.pcmc}</td>
					</tr>
					<tr>
						<th class="gray">项目所属学科：</th>
						<td>${project.subject}</td>
					</tr>
					<tr>
						<th class="gray">项目名称：</th>
						<td>${project.name}</td>
					</tr>
					<tr>
						<th class="gray">项目内容介绍：</th>
						<td><textarea name="content" id="content" style="width:98%;" rows="15" readonly="readonly">${project.content}</textarea></td>
					</tr>
					<tr>
						<th class="gray">所需经费：</th>
						<td>${project.funds}</td>
					</tr>
					<tr>
						<th class="gray">项目负责人：</th>
						<td>${project.user.dname} ${project.user.name} ${project.user.duty}</td>
					</tr>
					<tr>
						<th class="gray">预计完成时间：</th>
						<td><fmt:formatDate value="${project.wcsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">项目立项时间：</th>
						<td><fmt:formatDate value="${project.lxsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">申请验收成果：</th>
						<td>
						<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value="*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						<jsp:param name="isRead" value="true" />
						</jsp:include>
						</td>
					</tr>
					<tr>
						<th class="gray">申请时间：</th>
						<td><fmt:formatDate value="${project.application.sqsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">审核人：</th>
						<td>${project.application.shr }</td>
					</tr>
					<tr>
						<th class="gray">审核时间：</th>
						<td><fmt:formatDate value="${project.application.shsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray"><span class="star">*</span>评审意见：</th>
						<td><input type="radio" name="opinion" value="1" />&nbsp;通过&nbsp;&nbsp;
						<input type="radio" name="opinion" value="2" />&nbsp;不通过</td>
					</tr>
					<tr>
						<th class="gray">评语：</th>
						<td><input type="text" name="comment" class="input" size="100" /></td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="确定"/>
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>