<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>accept conclusion list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$(".selectClass").select2({
		placeholder : "-请选择-"
	});
});

	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/accept/conclude/list";
	}

	//查看
	function view(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/accept/conclude/view";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);  
	}

	//审核
	function establish(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/accept/conclude/edit";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
</head>
  <body>
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>验收结题</span></strong>
		</h2>
		<form action="<c:url value='/accept/conclude/list'/>" id="form1" method="POST">
			<div class="searchBar mT10">
				项目名称：<input name="mc" type="text" class="input" value="${vo.mc}" />  
				申请时间：<input class="input"  id="kssj" name="kssj"
					value="<fmt:formatDate value="${vo.kssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				至 <input class="input" id="jssj" name="jssj"
					value="<fmt:formatDate value="${vo.jssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'kssj\')}',dateFmt:'yyyy-MM-dd'})" />
				&nbsp;批次：<select name="batchId" id="batchId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.batchId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${batchList!=null && fn:length(batchList)>0 }">
							<c:forEach var="batch" items="${batchList}" varStatus="status">
								<c:set var="batchIndex" value="${status.index+1 }" />
								<option value='${batch.id}' <c:if test="${batch.id==vo.batchId}">selected="selected"</c:if>>${batch.pcmc}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;学科：<select name="depId" id="depId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.depId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${depList!=null && fn:length(depList)>0 }">
							<c:forEach var="dep" items="${depList}" varStatus="status">
								<c:set var="depIndex" value="${status.index+1 }" />
								<option value='${dep.id}' <c:if test="${dep.id==vo.depId}">selected="selected"</c:if>>${dep.subject}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;<input type="submit" onClick="return onceSubmit(this);" name="button22" class="btn01" value="查询" />
			</div>
		</form>
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }"
			requestURI="/accept/conclude/list" style="width: 100%;" partialList="true" size="resultSize" >
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="项目名称" >
				<c:set var="testTitle" value="${bean.name}"></c:set> 
    				<c:choose>  
    					<c:when test="${fn:length(testTitle) > 26}">  
            				<c:out value="${fn:substring(testTitle, 0, 26)}......" />  
        				</c:when>  
       					<c:otherwise>  
          					<c:out value="${testTitle}" />  
        				</c:otherwise>  
    			  </c:choose> 
			</display:column>
			<display:column title="对应批次">${bean.batchSetting.pcmc}</display:column>
			<display:column title="对应学科">${bean.subject}</display:column>
			<display:column title="申请时间">
				<fmt:formatDate value="${bean.application.sqsj}" pattern="yyyy-MM-dd" />
			</display:column>
			<display:column title="审核时间">
				<fmt:formatDate value="${bean.application.shsj}" pattern="yyyy-MM-dd" />
			</display:column>
			<display:column title="状态">
				<c:if test="${bean.status == 10 }">未处理</c:if>
				<c:if test="${bean.status == 12 }">已验收</c:if>
				<c:if test="${bean.status == 13 }">验收不通过</c:if>
			</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:view('${bean.id}');">查看</a>
				<c:if test="${bean.status == 10 }">
					<a class="editBtn01" href="javascript:establish('${bean.id}');">验收</a>
				</c:if>
			</display:column>
		</display:table>
	</div>
  </body>
</html>
