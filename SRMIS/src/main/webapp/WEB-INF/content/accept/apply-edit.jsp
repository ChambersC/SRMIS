﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>acceptance application edit</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				
			},
			messages : {
				
			},
			submitHandler : function(form) { //通过之后回调
				if($("#multiFileIds").val()==""){
					layer.msg("请提交验收成果",{icon: 0,time: 3000,btn: ['确　定']});
					return;
				}
				commonJs.saveForm({
					id : form,
					url : "${ctx}/accept/save",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});
	});
	
	//iframe上传文件后回调函数
	function setFjxx(multiFileIds){
		$("#multiFileIds").val(multiFileIds);
	}
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>验收申请编辑</span></strong>
		</h2>
		<form id="form" method="post" action="#" enctype="multipart/form-data">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray">项目名称：</th>
						<td>${project.name}</td>
					</tr>
					<tr>
						<th class="gray">验收成果：</th>
						<td>
							<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
							<jsp:param name="stmc" value="${stmc }" />
							<jsp:param name="stid" value="${stid }" />
							<jsp:param name="fileExts"
								value="*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
							</jsp:include>
							<label style="color:red;">文件命名请以“项目名+相应分类名陈”，支持格式：*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf，上传限制20M</label>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="确定" /> 
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>