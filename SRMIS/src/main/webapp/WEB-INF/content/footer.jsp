<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>底部</title>
<link href="<c:url value='/css/base.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/common.css'/>" rel="stylesheet" type="text/css" />
<style type="text/css">
@media print {.noprint {display: none;}.PageNext{page-break-after: always;}}
</style>
</head>
<body >
<div class="footer clearFix noprint">
  <p>Copyright © 2017-2018. All Rights Reserved(v1.0正式版)<span>Chen Bochao</span></p>
</div>
</body>
</html>
