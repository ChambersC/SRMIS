<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>寻回密码页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  	
    <%@ include file="/WEB-INF/common/introduce.jsp"%>
	<link rel="stylesheet" type="text/css" href="css/homePage.css">

</head>
<body>
    <div class="container">

      <form class="form-signin" action="${ctx}/password_reset">
        <h2 class="form-signin-heading">请输入账号和安全密码..</h2>
        <div class="main">
        <label for="inputText">账号</label>
        <input type="text" id="inputText" class="form-control" placeholder="Account" required autofocus>
        
        <label for="inputPassword" id="pwd">安全密码</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="SecurityPassword" required>
        
        <button class="btn btn-lg btn-info btn-sm" type="submit">确定</button>&nbsp;&nbsp;&nbsp;
        <button class="btn btn-lg btn-info btn-sm" type="button" onclick="javascript:history.go(-1)">返回</button>
      	</div>
      </form>

    </div>
  </body>
</html>

