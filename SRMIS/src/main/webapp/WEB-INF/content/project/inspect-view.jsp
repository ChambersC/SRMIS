﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>midterm inspection view</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>中期检查查看</span></strong>
		</h2>
		<form id="form" method="post" action="#">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					<tr>
						<th class="gray">项目所属批次：</th>
						<td>${project.batchSetting.pcmc}</td>
					</tr>
					<tr>
						<th class="gray">项目所属学科：</th>
						<td>${project.subject}</td>
					</tr>
					<tr>
						<th class="gray">项目名称：</th>
						<td>${project.name}</td>
					</tr>
					<tr>
						<th class="gray">项目内容介绍：</th>
						<td><textarea name="content" id="content" style="width:98%;" rows="15" readonly="readonly">${project.content}</textarea></td>
					</tr>
					<tr>
						<th class="gray">所需经费：</th>
						<td>${project.funds}</td>
					</tr>
					<tr>
						<th class="gray">预计完成时间：</th>
						<td><fmt:formatDate value="${project.wcsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th class="gray">附件：</th>
						<td>
						<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
						<jsp:param name="stmc" value="${stmc }" />
						<jsp:param name="stid" value="${stid }" />
						<jsp:param name="fileExts"
							value="*.txt;*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
						<jsp:param name="isRead" value="true" />
						</jsp:include>
						</td>
					</tr>
					<c:if test="${project.user!=null}">
						<tr>
							<th class="gray">项目负责人：</th>
							<td>${project.user.dname} ${project.user.name} ${project.user.duty}</td>
					</tr></c:if>
					<tr>
						<th class="gray">立项时间：</th>
						<td><fmt:formatDate value="${project.lxsj}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<c:if test="${not empty nbxtList}">
						<tr>
							<th class="gray">中期报告：</th>
							<td><div id="custom-queue" style="border: 1px solid #E5E5E5; min-height: 50px; margin-bottom: 10px; width: 670px; overflow:auto; float:left;">
								<c:forEach var="file" items="${nbxtList }">
	                    			<a href="${ctx }/attachment/download?id=${file.id }" target="">${file.fjmc }</a><br/>
	                    		</c:forEach></div>
							</td>
					</tr></c:if>
				</table>
			</div>
			<div class="nbot_control">
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>