<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>midterm inspection list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$(".selectClass").select2({
		placeholder : "-请选择-"
	});
});

	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/inspect/list";
	}

	//查看
	function view(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/inspect/view";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);  
	}

	//提交中期检查
	function submit(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/inspect/submit";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);
	}
	
	//查阅
	function refer(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/inspect/refer";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
</head>
  <body>
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>中期检查</span></strong>
		</h2>
		<form action="<c:url value='/inspect/list'/>" id="form1" method="POST">
			<div class="searchBar mT10">
				批次：<select name="batchId" id="batchId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.batchId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${batchList!=null && fn:length(batchList)>0 }">
							<c:forEach var="batch" items="${batchList}" varStatus="status">
								<c:set var="batchIndex" value="${status.index+1 }" />
								<option value='${batch.id}' <c:if test="${batch.id==vo.batchId}">selected="selected"</c:if>>${batch.pcmc}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;学科：<select name="depId" id="depId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.depId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${depList!=null && fn:length(depList)>0 }">
							<c:forEach var="dep" items="${depList}" varStatus="status">
								<c:set var="depIndex" value="${status.index+1 }" />
								<option value='${dep.id}' <c:if test="${dep.id==vo.depId}">selected="selected"</c:if>>${dep.subject}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;项目名称：<input name="mc" type="text" class="input" value="${vo.mc}" />  
				<input type="submit" onClick="return onceSubmit(this);" name="button22" class="btn01" value="查询" />
			</div>
		</form>
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }"
			requestURI="/inspect/list" style="width: 100%;" partialList="true" size="resultSize" >
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="项目名称" >
				<c:set var="testTitle" value="${bean.name}"></c:set> 
    				<c:choose>  
    					<c:when test="${fn:length(testTitle) > 26}">  
            				<c:out value="${fn:substring(testTitle, 0, 26)}......" />  
        				</c:when>  
       					<c:otherwise>  
          					<c:out value="${testTitle}" />  
        				</c:otherwise>  
    			  </c:choose> 
			</display:column>
			<display:column title="中期报告">
				<c:if test="${bean.attachment==null }">未提交</c:if>
				<c:if test="${bean.attachment!=null }">
					<a href="${ctx }/attachment/download?id=${bean.attachment.id }" target="">
					${bean.attachment.fjmc }</a>
				</c:if>
			</display:column>
			<display:column title="状态">
				<c:if test="${bean.status == 5 && bean.attachment!=null }">未查阅</c:if>
				<c:if test="${bean.status >= 8 && bean.attachment!=null }">已查阅</c:if>
			</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:view('${bean.id}');">详细</a>
				<c:if test="${bean.status == 5 && Type != 2 && bean.attachment==null }">
					<a class="editBtn01" href="javascript:submit('${bean.id}');">提交</a>
				</c:if>
				<c:if test="${bean.status == 5 && Type == 2 && bean.attachment!=null }">
					<a class="editBtn01" href="javascript:refer('${bean.id}');">查阅</a>
				</c:if>
			</display:column>
		</display:table>
	</div>
  </body>
</html>
