<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>management of science research achievement list</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
	<!-- 引入 ECharts 文件 -->
    <script src="${ctx}/js/echarts.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$(".selectClass").select2({
		placeholder : "-请选择-"
	});
});

	/**
	 * 刷新
	 */
	function sx() {
		window.location.href = "${ctx}/achieve/list";
	}

	//查看
	function view(id) {
		var width = $(document).width() * 0.8;
		var height = $(document).height() * 0.8;
		var url = "${ctx}/achieve/view";
		if (id != "") {
			url += "?id=" + id;
		}
		var index=layer.open({
			type : 2,
			area : [ width + "px", height + "px" ],
			content : url
		});
		layer.full(index);  
	}
	
	/**
	 * 全选，全不选
	 */
	function selectCheckbox(){
		var chk_ss = document.getElementsByName('cid');
		for(var i=0; i<chk_ss.length; i++){
			if(chk_ss[i].type == 'checkbox' ){
				chk_ss[i].checked = document.getElementById('selectAll').checked;
				
				if (chk_ss[i].onclick != null)
					chk_ss[i].onclick();
			}
		}
	}
	
	function onceSubmit(){
		var url = "${ctx}/achieve/list";
		$("#form1").attr("action", url).submit(); 
	}
	
	/**
	 * 导出Excel
	 */
	function exportExcel(){
		var chk_value =[]; 
		$('input[name="cid"]:checked').each(function(){ 
			chk_value.push($(this).val()); 
		});
		if(chk_value==""){
			alert("请勾选想要导出信息的项目");
		}else{
			if (confirm("是否要导出当前选择的项目信息？")) {
				var url = "${ctx}/achieve/exportExcel?cid="+chk_value;
				$("#form1").attr("action", url).submit(); 
			}
		}
	}
</script>
<style type="text/css">
.sortable a{	color:#000;}
.pagebanner{margin-left: 46%}
</style>
</head>
  <body>
	<div class="main">
		<h2 class="subTitle mT10">
			<strong><span>科研成果管理</span></strong>
		</h2>
		<form action="<c:url value='/achieve/list'/>" id="form1" method="POST">
			<div class="searchBar mT10">
				批次：<select name="batchId" id="batchId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.batchId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${batchList!=null && fn:length(batchList)>0 }">
							<c:forEach var="batch" items="${batchList}" varStatus="status">
								<c:set var="batchIndex" value="${status.index+1 }" />
								<option value='${batch.id}' <c:if test="${batch.id==vo.batchId}">selected="selected"</c:if>>${batch.pcmc}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;学科：<select name="depId" id="depId" class="selectClass" style="width: 90px">
          		    <option value='' <c:if test="${vo.depId==null}">selected="selected"</c:if>>请选择</option>
					<c:choose>
						<c:when test="${depList!=null && fn:length(depList)>0 }">
							<c:forEach var="dep" items="${depList}" varStatus="status">
								<c:set var="depIndex" value="${status.index+1 }" />
								<option value='${dep.id}' <c:if test="${dep.id==vo.depId}">selected="selected"</c:if>>${dep.subject}</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</select>
				&nbsp;项目名称：<input name="mc" type="text" class="input" value="${vo.mc}" />  
				结题时间：<input class="input"  id="kssj" name="jtkssj"
					value="<fmt:formatDate value="${vo.jtkssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'jssj\')}',dateFmt:'yyyy-MM-dd'})" /> 
				至 <input class="input" id="jssj" name="jtjssj"
					value="<fmt:formatDate value="${vo.jtjssj}" pattern="yyyy-MM-dd" />"
					type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'kssj\')}',dateFmt:'yyyy-MM-dd'})" />
				<input type="button" onClick="onceSubmit('');" name="button22" class="btn01" value="查询" />
				<input type="button" onClick="exportExcel('');" name="button22" class="btn01" value="导出" />
			</div>
		</form>
		<display:table name="pageList" id="bean" class="listTable mT5 tCenter"	pagesize="${pageSize }"
			requestURI="/achieve/list" style="width: 100%;" partialList="true" size="resultSize" >
			<display:column title="<input type='checkbox' name='selectAll' id='selectAll'  onclick='selectCheckbox()'/>">
				<input type="checkbox" name="cid" id="cid_${bean.id}" value="${bean.id}" class="cid" /> 
			</display:column>
			<display:column title="序号">&nbsp;${bean_rowNum}</display:column>
			<display:column title="项目名称" >
				<c:set var="testTitle" value="${bean.name}"></c:set> 
    				<c:choose>  
    					<c:when test="${fn:length(testTitle) > 26}">  
            				<c:out value="${fn:substring(testTitle, 0, 26)}......" />  
        				</c:when>  
       					<c:otherwise>  
          					<c:out value="${testTitle}" />  
        				</c:otherwise>  
    			  </c:choose> 
			</display:column>
			<display:column title="对应批次">${bean.batchSetting.pcmc}</display:column>
			<display:column title="对应学科">${bean.subject}</display:column>
			<display:column title="结题时间">
				<fmt:formatDate value="${bean.jtsj}" pattern="yyyy-MM-dd" />
			</display:column>
			<display:column title="操作">
				<a class="editBtn01" href="javascript:view('${bean.id}');">详细</a>
			</display:column>
		</display:table>
	</div><br><br><br>
	<c:if test="${Type==2 }">
		<div id="barContainer" style="width: 50%;height: 50%;float: left;"></div>
	</c:if>
	<div id="container" style="width: 50%;height: 50%;float: left;"></div>
	<script type="text/javascript">
	var lineContext =eval('${strLineContext}');  
	//名称数组  
	var nameArray = new Array();
	//数据数组  
	var dataArray= new Array();
	//将数据进行处理  
    for(var i=0;i<lineContext.length;i++){     
        nameArray.push(lineContext[i].name);  
        dataArray.push(lineContext[i].number);  
    } 


var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
option = {
	title: {
		text: '批次-项目数线形图'
	},
    xAxis: {
    	name: '批次',
        type: 'category',
        data: nameArray
    },
    yAxis: {
    	name: '项目数量',
        type: 'value'
    },
    series: [{
        data: dataArray,
        type: 'line'
    }]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}


if('${Type}'==2){
	var barContext =eval('${strBarContext}');
	//名称数组  
	var nameArray2 = new Array();
	//数据数组  
	var dataArray2 = new Array();
	//将数据进行处理  
    for(var j=0;j<barContext.length;j++){     
        nameArray2.push(barContext[j].name);  
        dataArray2.push(barContext[j].number);  
    } 


	var dom2 = document.getElementById("barContainer");
	var myChart2 = echarts.init(dom2);
	var app2 = {};
	option2 = null;
	option2 = {
		title: {
			text: '${name}学科-项目数柱状图'
		},
	    xAxis: {
	    	name: '学科',
	        type: 'category',
	        data: nameArray2,
	        axisLabel :{  
			    interval:0,
			    rotate:30 
			}  
	    },
	    yAxis: {
	   		name: '项目数量',
	        type: 'value'
	    },
	    series: [{
	        data: dataArray2,
	        type: 'bar'
	    }]
	};
	;
	if (option2 && typeof option2 === "object") {
	    myChart2.setOption(option2, true);
	}
}
       </script>
  </body>
</html>
