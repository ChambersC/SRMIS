﻿<%@ page language="java" errorPage="/WEB-INF/common/exception.jsp"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>midterm inspection submit</title>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				
			},
			messages : {
				
			},
			submitHandler : function(form) { //通过之后回调
				if($("#multiFileIds").val()==""){
					layer.msg("请上传中期报告",{icon: 0,time: 3000,btn: ['确　定']});
					return;
				}
				commonJs.saveForm({
					id : form,
					url : "${ctx}/inspect/save",
					callback : function() {
						parent.sx();
					}
				}); 
			}
		});	
	});
</script>
</head>
<body>
	<div id="formbody">
		<h2 class="subTitle mT10">
			<strong><span>中期检查提交</span></strong>
		</h2>
		<form id="form" method="post" action="#" enctype="multipart/form-data">
			<input type="hidden" name="id"	id="Id" value="${project.id}" />
			<div class="ntable_box mT5">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="nt_table">
					<col width="20%" />
					<col width="80%" />
					
					<tr>
						<th class="gray">项目名称：</th>
						<td>${project.name}</td>
					</tr>
					
					<tr>
						<th class="gray">中期报告：</th>
						<td>
							<jsp:include page="/WEB-INF/common/multiFileUp.jsp">
							<jsp:param name="stmc" value="${stmc2 }" />
							<jsp:param name="stid" value="${stid2 }" />
							<jsp:param name="fileExts"
								value="*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf;" />
							<jsp:param name="fileCount" value="1" />
							</jsp:include>
							<label style="color:red;">文件命名请以“项目名+中期报告”，支持格式：*.doc;*.docx;*.xls;*.xlsx;*.zip;*.rar;*.pdf，上传限制20M</label>
						</td>
					</tr>
				</table>
			</div>
			<div class="nbot_control">
				<input type="submit" name="button1" class="formBtn" value="提交"/>
				<input type="button" name="button2" class="formBtn" value="返回"
					onclick="commonJs.closeWindow();" />
			</div>
		</form>
	</div>
</body>
</html>