<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'homeMainPage.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style>
	/*覆盖原来日历的高度*/
	.fc-event-inner{height:16px;}
</style>
<%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%>
<%@ page errorPage="/WEB-INF/common/exception.jsp"  %>
<link href="<c:url value='/js/fullcalendar/fullcalendar.css'/>" rel='stylesheet' />
<link href="<c:url value='/js/fullcalendar/fullcalendar.print.css'/>" rel='stylesheet' media='print' />
<script type="text/javascript" src="<c:url value='/js/fullcalendar/fullcalendar.min.js'/>"></script>

  </head>
  <script type="text/javascript">

$(document).ready(function(){  
    var date = new Date();  
    var d = date.getDate();  
    var m = date.getMonth();  
    var y = date.getFullYear();  
    // page is now ready, initialize the calendar...  
    $('#calendar').fullCalendar({
        header: {
    		left: 'prev,next',
    		center: 'title',
    		right: 'today'
        },
        titleFormat: {
			month: "yyyy-MM",
			week: "yyyy-MM-dd {'&#8212;' yyyy-MM-dd}",  //花括号里面用于动态的计算结束日期
			day : 'yyyy-MM-dd, dddd'
        },
        height:150,
        //firstHour : 8, //指定在视图显示时，默认跳转的第一时间
        //minTime : 8, //指定日视图日历的开始时间(8am)
        //maxTime : 23, //指定日视图日历的结束时间(11pm)
        //defaultView : 'month', //默认首先显示日视图
        //slotMinutes : 30,
        //editable: true,
        monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],  
        monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],  
        dayNames: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],  
        dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],  
        today: ["今天"],
        buttonText: {  
            today: '今天',  
        	month: '月',  
        	week: '周',  
        	day: '日'  
        },
        eventClick: function(calEvent, jsEvent, view) {
        	openwindow(calEvent.url,"chakan",800,600);
        	return false;
        }
        
    });  
});

//通知公告查看详细
function openTzgg(id){
		commonJs.openWindow({url:"${ctx}/notice/nbview",id:id,title:"通知公告",maximize:true});
}

//站内信查看详细
function openZnsx(id){
	commonJs.openWindow({url:"${ctx}/",id:id,title:"查看信件",maximize:true});
}
//查看审批
function openSp(id,title){
	commonJs.openWindow({url:"${ctx}/notice/awnview",id:id,title:title,maximize:true});
}

</script>



 <style>
 .weather{
 	padding-top: 10%;
 }
 .fc-header-left{
 	padding-left: 3px !important;
 }
 .fc-header-right{
 	padding-right: 9px !important;
 	width: 18%;
 }
 .fc-header-title h2{
 	font-size: 25px;
 }
 .fc-content{
 	font-size: 15px !important;
 	padding: 5px 1.5px 0 0.5px;
 }
 
 
.home { width: 95%; margin: 0 auto; padding: 20px 0px 10px 0px;}
.articleList { width: 95%; margin-left: -270px;}
.articleList dl { margin-left: 270px;}
.articleList ul { padding: 7px 5px 0 5px;}
.articleList li { overflow: hidden; height: 30px; line-height: 30px; padding: 0 10px; border-bottom: 1px dashed #9e9e9e;}
.articleList li a { display: block; overflow: hidden; padding-left: 15px; color: #000; background: url(<c:url value='/images/articleArrow.gif' />) no-repeat left 11px; white-space: nowrap; -o-text-overflow: ellipsis; text-overflow: ellipsis;}
.articleList li a:hover { color: blue;}
.articleList p { margin-top: 5px; padding-right: 10px; text-align: right;}
.articleList p a { display: inline-block; padding-left: 12px; background: url(<c:url value='/images/articleMore.gif' />) no-repeat left 5px; color: #f90;}
.articleList p a:hover { color: red;}
.siderBar { width: 270px;}
.siderBar dl { background: url(<c:url value='/images/siderBarBottom.gif'/>) no-repeat left bottom;}
.siderBar dt { height: 18px; line-height: 16px; padding-left: 20px; color: #3097d2; background: url(<c:url value='/images/siderBarTop.gif' />) no-repeat left top; font-size: 14px; font-weight: bold;}
.siderBar dd { padding: 5px 10px 32px 0px;}
.qLink dd { padding-left: 90px; background: url(<c:url value='/images/siderBarBg01.gif' />) no-repeat 10px 10px;}
.qLink ul { zoom: 1; overflow: hidden;}
.qLink li { float: left; width: 72px; height: 22px; line-height: 22px; display: inline; margin: 6px 5px 0 5px;}
.qLink li a { display: block; color: #666; background: url(<c:url value='/images/siderBarLink01.gif' />) no-repeat left top; text-align: center;}
.qLink li a:hover { color: #f60; background: url(<c:url value='/images/siderBarLink02.gif' />) no-repeat left top;}
.date { padding: 0 0 0 15px; text-align: left;}
</style>

<body>
 <div id="formbody">
        <div class="ntable_box mT5">
            <div class="desk_right">
                <div class="desk-div2">
                    <dl>
                        <dt class="clear mB10"><span>
                        <span class="glyphicon glyphicon-cloud" aria-hidden="true"></span>&nbsp;&nbsp;天气预报</span></dt>
                        <dd><div class="weather">
                        <iframe width="225" scrolling="no" height="80" frameborder="0" allowtransparency="true" src="http://i.tianqi.com/index.php?c=code&id=8&icon=1&py=zhongshan&num=3"></iframe>
                        </div>
                        </dd>
                    </dl>
                </div>

                <div class="desk-div2">
                    <dl><!-- <dl style=" height: auto;"> -->
                        <dt class="clear mB10"><span>
                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>&nbsp;&nbsp;日历</span></dt>
                        <dd><div id="calendar"></div>
                        </dd>
                    </dl>
                </div>
            </div>

            <div class="desk_left">
                <div class="desk-div">
                    <dl>
                        <dt class="clear mB10"><span>
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;&nbsp;通知公告</span>
	                    
	                        <a href="${ctx}/notice/nblist" class="more">
                        <img src="<c:url value='/images/more.gif'/>"></a>
                        
                        </dt>
                        
                        
                        <dd>
                        <ul>
                        
                        	<c:if test="${not empty noticeList}">
                        		<c:forEach items="${noticeList}" var="item" varStatus="status">
                        			<c:set var="index" value="${status.index+1 }" />
                        			<c:if test="${index<8 }"><li>
                        				<span class="time" id="tzgg">
                        				<fmt:formatDate value="${item.fbsj }" pattern="yyyy-MM-dd HH:mm"/>
                        				</span>
                        				<a href="javascript:openTzgg('${item.id}');">${item.btmc }</a>
                        			</li></c:if>
                        		</c:forEach>
                        	</c:if>
                        	<c:if test="${empty noticeList}">
                        		<li>
                        			没有通知公告
                        		</li>
                        	</c:if>
                       	
                        </ul>
                        </dd>
                        
                    </dl>
                </div>

                <div class="desk-div">
                    <dl>
                        <dt class="clear mB10"><span>
                        <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>&nbsp;&nbsp;获奖快讯</span>
                        <a href="${ctx}/notice/awnlist" class="more"><img src="<c:url value='/images/more.gif'/>"></a></dt>
                        <dd>
                        
                        <ul>
					<c:if test="${not empty newsList}">
						<c:forEach items="${newsList}" var="news" varStatus="status">
                    		<c:set var="index" value="${status.index+1 }" />
                    		<c:if test="${index<8 }">
							<li><span class="time" id="fbsj" >
									
									<%-- <c:if test="${news.zgr != null}" >
										${news.zgr}
									</c:if>&nbsp;&nbsp; --%>
								<fmt:formatDate value="${news.fbsj}" pattern="yyyy-MM-dd HH:mm"/>
								</span>
								<a href="javascript:openSp('${news.id}','获奖喜讯');">${news.btmc}
								</a> 
							</li></c:if>
						</c:forEach>
					</c:if>
					<c:if test="${empty newsList}">
						<li>没有获奖快讯
						</li>
					</c:if>
                </ul>
                        </dd>
                        
                    </dl>
                </div>

                <div class="desk-div">
                    <dl>
                        <dt class="clear mB10"><span>
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;&nbsp;站内信</span>
                        <a href="${ctx}/homeMainPage" class="more"><img src="<c:url value='/images/more.gif'/>"></a></dt>
                        <dd>
						<ul>
                        	<c:if test="${!empty YwkZnxSjjlList}">
                        		<c:forEach items="${YwkZnxSjjlList}" var="item">
                        			<li>
                        				<span class="time" id="tzgg">
                        				<fmt:formatDate value="${item.TYwkZnxFjjl.fbsj}" pattern="yyyy-MM-dd HH:mm"/>
                        				</span>
                        				<a href="javascript:openZnsx('${item.id}');">${item.TYwkZnxFjjl.bt }</a>
                        			</li>
                        		</c:forEach>
                        	</c:if>
                        	<c:if test="${empty YwkZnxSjjlList}">
                        		<li>
                        			没有收信
                        		</li>
                        	</c:if>
                        </ul>
                        </dd>
                        
                    </dl>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

