<%-- <%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%> --%>

	<!-- Bootstrap 不支持 IE 古老的兼容模式。为了让 IE 浏览器运行最新的渲染模式下，
	建议将此 <meta> 标签加入到你的页面中 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<!-- <link type="text/css" rel="stylesheet" href="css/base.css">
	<script src="js/common.js" type="text/javascript"></script> -->
	
	<link href="${ctx}/bootstrap-3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<!-- <script src="http://code.jquery.com/jquery-1.8/11.3.min.js" type="text/javascript"></script> -->
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>


<%-- 支持 HTML5 --%>
<script src="${ctx}/js/html5shiv.js"></script>
<%-- 支持 CSS3 --%>
<script src="${ctx}/js/respond.min.js"></script>
