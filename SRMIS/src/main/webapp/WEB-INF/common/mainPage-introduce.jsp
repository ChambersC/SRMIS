<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

	<!-- Bootstrap 不支持 IE 古老的兼容模式。为了让 IE 浏览器运行最新的渲染模式下，
	建议将此 <meta> 标签加入到你的页面中 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link href="${ctx}/bootstrap-3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

	<!-- 引入上传组件 -->
	<link href="<c:url value='/js/uploadify/uploadify.css'/>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<c:url value='/js/uploadify/jquery.uploadify.js'/>"></script>
	
	<script type="text/javascript" charset="utf-8" src="${ctx}/js/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${ctx}/js/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
	
	<link href="${ctx}/js/select2/css/select2.min.css" rel="stylesheet" />
	<script src="${ctx}/js/select2/js/select2.min.js"></script>
	<script src="${ctx}/js/select2/js/i18n/zh-CN.js"></script>
	
<link href="<c:url value='/css/base.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/css/newstyle.css'/>" rel="stylesheet" type="text/css" />

<script src="${ctx}/js/commonJs.js" type="text/javascript"></script>
<script src="${ctx}/js/operateJs.js" type="text/javascript"></script>
	
	<!-- Web弹窗/层 -->
	<script src="${ctx}/js/layer/layer.js" type="text/javascript"></script>
	<!-- 日期控件 -->
	<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>

<%-- 支持 HTML5 --%>
<script src="${ctx}/js/html5shiv.js"></script>
<%-- 支持 CSS3 --%>
<script src="${ctx}/js/respond.min.js"></script>
