<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	String stid	= request.getParameter("stid");	// 实体id
	String stmc	= request.getParameter("stmc");		// 实体类名
//实体id 和名称适用于识别文件的唯一id，可用于编辑下载 ， 如果为空，则表示新增的操作
	Boolean isRead = false;								// 是否只读
	if (StringUtils.isNotBlank(request.getParameter("isRead")) && request.getParameter("isRead").equalsIgnoreCase("true")){
		isRead = true;
	}
	if (StringUtils.isBlank(stid)) stid	= StringUtils.EMPTY;
	if (StringUtils.isBlank(stmc)) stmc	= StringUtils.EMPTY;
	//默认值
	String fileExts = "*.*";	
	if (StringUtils.isNotBlank(request.getParameter("fileExts"))){
		fileExts = request.getParameter("fileExts");
	}
	
	String fileSize 	= "209715200";		//  200*1024*1024   附件大小，默认200MB    1M= 1*1024*1024
	if (StringUtils.isNotBlank(request.getParameter("fileSize"))){
		fileSize = request.getParameter("fileSize");
	}
	
	String fileCount 	= "20";			// 附件数量，默认 20
	if (StringUtils.isNotBlank(request.getParameter("fileCount"))){
		fileCount = request.getParameter("fileCount");
	}
%>
<%-- <%@ include file="/WEB-INF/common/mainPage-introduce.jsp"%> --%>
<link href="<c:url value='/js/multiFileUp/uploadify.css'/>" rel="stylesheet" type="text/css" />
<script language="javascript" src="<c:url value='/js/multiFileUp/swfobject.js'/>"></script>
<script language="javascript" src="<c:url value='/js/multiFileUp/jquery.uploadify.v2.1.4.min.js'/>"></script>
<style type="text/css">
#custom-demo .uploadifyQueueItem {
	background-color: #FFFFFF;
	border: none;
	border-bottom: 1px solid #E5E5E5;
	font: 11px Verdana, Geneva, sans-serif;
	height: 26px;
	margin-top: 0;
	padding: 10px;
	width: 630px;
}
#custom-demo .uploadifyError {
	background-color: #FDE5DD !important;
	border: none !important;
	border-bottom: 1px solid #FBCBBC !important;
}
#custom-demo .uploadifyQueueItem .cancel {
	float: right;
}
#custom-demo .uploadifyQueue .completed {
text-align:left; 
	color: #C5C5C5;
}
#custom-demo .uploadifyProgress {
	background-color: #E5E5E5;
	margin-top: 10px;
	width: 100%;
}
#custom-demo .uploadifyProgressBar {
	background-color: #0099FF;
	height: 3px;
	width: 1px;
}
#custom-demo #custom-queue {
	border: 1px solid #E5E5E5;
	height: 50px;
	margin-bottom: 10px;
	width: 670px;
	overflow:auto;
	float:left;
}
</style>
<div id="custom-demo" class="demo">
<script type="text/javascript">
function delRes(id){
	//alert(id);
    var url =  urlPath+"attachment/delete?id=" + id;
    document.getElementById("downfile").src = url;
}
function getItemHTML(id,fileName,fileSaveName) {
   	var multiFileIds	= $('#multiFileIds');
   	multiFileIds.val(multiFileIds.val() + id + ",");
   	
	var str = '<div id="fileupload' + id + '" class="uploadifyQueueItem completed">';
	
	/* <c:if test="<%= !isRead %>"> */
	if(<%= !isRead %>){
		str += '<div class="cancel">';
		str += '<a  onclick="jQuery(\'#fileupload\').uploadifyCancel(\'' + id + '\')">';
		str += '<img border="0" src="<c:url value='/js/multiFileUp/cancel.png'/>">';
		str += '</a>';
		str += '</div>';
	}/* </c:if> */

	//urlPath  str += '<span class="fileName"><a target="downfile" href="<c:url value='/manager/nbxt/attachment/download.jhtml' />?id=' + id + '">' + fileName + '</a></span>';
	str += '<span class="fileName"><a target="downfile" href="'+urlPath+'attachment/download?id=' + id + '">' + fileName + '</a></span>';	
	str += '<span class="percentage"> - Completed</span>';
	str += '</div>';
	return str;
}
function getItemCount(){
	var count = $('#multiFileIds').val().split(',').length;
	return count == 0 ? 0 : count - 1;
}
function getMsg(err) {
	var count = getItemCount();
	var msg = "";
	if (count>0){
		msg = '已上传<font color="red"> ' +  getItemCount()  + ' </font>个文件 ';
	} else {
		msg = '请选择文件 ';
	}
	
	if (err !=null && err > 0) {
		msg += ', 发生  ' + err + ' 个 错误 ';
	}
	msg += '. ';
	
	return msg;
}

$(function() {	
	//路径配置
  	var configPath = "";	/* var configPath = "manager/"; */
  	var basePath="<c:url value='/'/>";
    urlPath=basePath+configPath;
  //  alert(urlPath);
	$('#fileupload').uploadify({
		'uploader'       : '<c:url value='/js/multiFileUp/uploadify.swf'/>',
		'script'         : urlPath+"attachment/upLoad", 
		'cancelImg'      : '<c:url value='/js/multiFileUp/cancel.png'/>',
		'multi'          : true,
		'auto'           : true,	/* 是否自动上传 */
		'fileDataName'   : 'file',	/* 要与表单 <input type="file"> 里的 name 相同 */
		'fileExt'        : '<%= fileExts %>',
		'fileDesc'       : '任意文件(*.*)',
		'sizeLimit'      : '<%= fileSize %>', 
        'fileQueueLimit' : '<%= fileCount %>',
        'queueSizeLimit' : '<%= fileCount %>',
        'uploadLimit'	 : '<%= fileCount %>',
		'queueID'        : 'custom-queue',
		'buttonText'     : '',
		'buttonImg'		 : '<c:url value='/js/multiFileUp/upload.png'/>',
		'width'			 : 80,
	//	'queueSizeLimit' : 2,
	//	'simUploadLimit' : 1,
	//	'sizeLimit'      : 2147483648,
		'removeCompleted': false,
		'onSelectOnce'  : function(event,data) {
		    $('#status-message').html('成功添加  ' + data.filesSelected + ' 个 文件到队列  .');
		},
		'onAllComplete'	: function(event,data) {
			$('#status-message').html(getMsg(data.errors));
		},
		'onComplete'	: onComplete,
		'onCancel'		: onCancel,
		'onInit'		: onInit
		});
});

function onInit(){
	setTimeout(function(){
		var stmc = '<%= stmc %>';
		var stid = '<%= stid %>';
		var obj = $('#custom-queue');
		if(stmc != "" && stid != ""){
			$.ajax({
				type: "POST",
				url: urlPath + "attachment/list", 
				data:{
					stmc:stmc,
					stid:stid
				},
				async: false,
				success:function(data){
					var ids = eval(data);
					//alert(ids.length);
					for (var i=0; i<ids.length; i++){
						obj.append(getItemHTML(ids[i].id, ids[i].name, ids[i].saveName));
					}
					//只读
					/* <c:if test="<%= isRead %>"> */
					if(<%= isRead %>){
						if(ids.length>=1){
							var h = 50*ids.length;
							obj.css("height", h+"px");
						};
					}/* </c:if> */
					
					/* <c:if test="<%= !isRead %>"> */
					if(<%= !isRead %>){
						obj.css("height", "216px");
					}/* </c:if> */
				},
				error:function(){
					$().toastmessage("showToast",{text:"对不起，操作失败!",type:"error",sticky:true});
				}
			});
		}else{
			/* <c:if test="<%= !isRead %>"> */
			if(<%= !isRead %>){
				obj.css("height", "216px");
			}/* </c:if> */
		};
		
	},20);
}
function onComplete (event, queueID, fileObj, response, data) {
	//alert(queueID);
	//alert(response);
	eval("var obj=" + response);
	//alert(obj);
	var item = document.getElementById("fileupload" + queueID); <%-- 列表新加项 --%>
	item.childNodes[1].innerHTML = 
		'<a target="downfile" href="<c:url value='' />" >' + item.childNodes[1].innerHTML + '</a>';
  	var saveId 			= response ;<%-- 服务器返回的信息 --%>
   	var multiFileIds	= $('#multiFileIds');
   	multiFileIds.val(multiFileIds.val() + obj.id + ",");
	//alert("onComplete");
}
function onCancel(event, queueId){
//alert("onCancel");
var lst = document.getElementById("custom-queue").childNodes;
for (var i=0; i<lst.length; i++){
	if (lst[i].id == "fileupload" + queueId){
		var result = "";
		var temp = document.getElementById('multiFileIds').value;
		//alert(temp);
		var multiFileIds	= document.getElementById('multiFileIds').value.split(",");
		for (var j=0; j<multiFileIds.length; j++){
			if (multiFileIds[j]=="") continue;
			if (i == j) {
				delRes(multiFileIds[j]);
				continue;
			}
			result += multiFileIds[j] + ",";
		}
		document.getElementById('multiFileIds').value = result;
		break;
	}
}
}
</script>
<div class="demo-box" >
<table cellpadding="0" cellspacing="0" border="0"  id="tb">
	<tr>
	<td style="border:0"><div id="custom-queue"></div></td>
	<td style="border:0">
       <table cellpadding="0" cellspacing="0" border="0" <c:if test="<%= isRead %>">style="display:none"</c:if>>
	      <tr >
				<td style="border:0px;"
					style="border:0px;background-repeat:no-repeat; background-image:url(<c:url value="/js/multiFileUp/upload.png"/>);background-position:8px 4px;">
					<input type="file" id="fileupload" name="file" />
					<div id="status-message"></div>
				</td>
          </tr>
        </table>
	</td>
	</tr>
</table>
</div>
<input type="hidden" id="multiFileIds" name="multiFileIds" /><%-- 已保存发附件id列表 --%>
</div>
<iframe id="downfile" name="downfile" style="display:none"></iframe>


