<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>高校科研管理信息系统</title>
    <%@ include file="/WEB-INF/common/introduce.jsp"%>
</head>
<body>
</body>
<script type="text/javascript">
	var baseUrl = "${ctx}/";
    $(document).ready(function () {
        window.location.href = baseUrl + "index";
    });
</script>
</html>
